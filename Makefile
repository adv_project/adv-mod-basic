SHELL := /bin/bash
ifndef PREFIX
  PREFIX = /usr
endif
MOD := adv-basic
VERSION=$(shell git describe | rev | cut -d- -f2- | rev | sed -e 's/-/.r/' -e 's/^v//')

ASSETSDIRS :=	assets

MDDIRS :=			.

EXECDIRS :=		hooks

PERLDIRS :=		lib \
              games/awari \
              games/bagels \
              games/basketball \
              games/battle \
              games/blackjack \
              games/bombardment \
              games/bombsaway \
              games/boxing \
              games/chemist \
              games/combat \
              games/depthcharge \
              games/golf \
              games/gunner \
              games/hammurabi \
              games/horserace \
              games/hurkle \
              games/kinema \
              games/king \
              games/lem \
              games/lunar \
              games/orbit \
              games/rocket \
              games/roulette \
              games/superstartrek \
              games/target \
							games/menu

JSONDIRS :=		. \
              games/awari \
              games/bagels \
              games/basketball \
              games/battle \
              games/blackjack \
              games/bombardment \
              games/bombsaway \
              games/boxing \
              games/chemist \
              games/combat \
              games/depthcharge \
              games/golf \
              games/gunner \
              games/hammurabi \
              games/horserace \
              games/hurkle \
              games/kinema \
              games/king \
              games/lem \
              games/lunar \
              games/orbit \
              games/rocket \
              games/roulette \
              games/superstartrek \
              games/target \
							games/menu

DOCDIRS :=		doc

default:

.PHONY: clean

.FORCE:

install:
	for assetsdir in $(ASSETSDIRS) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$assetsdir; \
		for assetsfile in $$assetsdir/* ; do \
			install -Dv -m 644 $$assetsfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$assetsfile ; \
		done; \
	done
	for mddir in $(MDDIRS) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$mddir; \
		for mdfile in $$mddir/*.md ; do \
			install -Dv -m 644 $$mdfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$mdfile ; \
		done; \
	done
	for execdir in $(EXECDIRS) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$execdir; \
		for execfile in $$execdir/* ; do \
			install -Dv -m 755 $$execfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$execfile ; \
		done; \
	done
	for perldir in $(PERLDIRS) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$perldir; \
		for perlfile in $$perldir/*.pl ; do \
			install -Dv -m 644 $$perlfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$perlfile ; \
		done; \
	done
	for jsondir in $(JSONDIRS) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$jsondir; \
		for jsonfile in $$jsondir/*.json ; do \
			install -Dv -m 644 $$jsonfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$jsonfile ; \
		done; \
	done
	for docdir in $(DOCDIRS) ; do \
		mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$docdir; \
		for docfile in $$docdir/*; do \
			install -Dv -m 644 $$docfile $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$docfile ; \
		done; \
	done
	@sed -i $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/properties.json -e "s/[\"]version[\"]: [\"].*[\"]/\"version\": \"$(VERSION)\"/"

uninstall:
	@echo "Uninstalling..."
	@rm -r $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)

clean:


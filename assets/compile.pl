#!/usr/bin/env perl

# REMEMBER! This should be used to port *short* games, easy to';
# debug and test. Parsing superstartrek with this will destroy';
# the Earth.';

use Storable qw(dclone);
use JSON;
use List::MoreUtils qw(uniq);

sub cout { print shift."\n"; }
sub forcedcomment {
  my $e = shift;
  my $l = shift;
  my $s = shift;
  if ($e eq 0) { cout '  # '.$l.'  '.$s; }
  else { cout '  #('.$l.') '.$s; }
}

sub comment {
  my $e = shift;
  my $l = shift;
  my $s = shift;
#  return unless ($comment);
  if ($e eq 0) { cout '  # '.$l.'  '.$s; }
  else { cout '  #('.$l.') '.$s; }
}

my $infile = $ARGV[0];

#local $/;
open my $fh, '<', $infile
or die "Could not open file: ".$infile;
my @lines = <$fh>;
close $fh;
chomp @lines;
@_lines = ();
@_goto = ();
@_gosub = ();
$code = {
  "lines" => dclone(\@_lines),
  "goto" => dclone(\@_goto),
  "gosub" => dclone(\@_gosub)
};

cout '#!/usr/bin/env perl';
cout '';
cout '# This file is part of the ADV Project';
cout '# Text-based adventure game, highly modular and customizable';
cout '# Written by Marcelo López Minnucci <coloconazo@gmail.com>';
cout '# and Mariano López Minnucci <mlopezviedma@gmail.com>';
cout '';

foreach my $l (0 .. (@lines-1)) {
  my $line = $lines[$l];
  my @tmp = split(" ", $line);
  my $lnum = $tmp[0];
  $line =~ s/$lnum //;
  my @tmp2 = split(":", $line);
  $code->{$lnum} = dclone(\@tmp2);
  push(@{$code->{"lines"}}, $lnum);
  foreach my $e (0 .. (@tmp2-1)) {
    my $expr = $tmp2[$e];
    next if ($expr =~ /^REM /);
    next if ($expr =~ /^PRINT/);
    next if ($expr =~ /^ PRINT/);
    my $ifgoto = $expr;
    if ($ifgoto =~ /GOTO/) {
      $ifgoto =~ s/GOTO//;
      $ifgoto =~ s/ //g;
      push(@{$code->{'goto'}}, $ifgoto);
      next;
    }
    my $ifgosub = $expr;
    if ($ifgosub =~ /GOSUB/) {
      $ifgosub =~ s/GOSUB//;
      $ifgosub =~ s/ //g;
      push(@{$code->{'gosub'}}, $ifgosub);
      next;
    }
    my $ifthengoto = $expr;
    if ($ifthengoto =~ /THEN.*[123456789]/) {
#      comment('', '<<IFTHENGOTO>> ', $ifthengoto);
      $ifthengoto =~ s/IF.*THEN//;
      $ifthengoto =~ s/ //;
      push(@{$code->{'goto'}}, $ifthengoto+0);
      next;
    }
    my $ifongoto = $expr;
    if ($ifongoto =~ /^ON.*GOTO/) {
      $ifongoto =~ s/ON.*GOTO//;
      $ifongoto =~ s/ //g;
      my @s = split(',', $ifongoto);
      for my $f (0 .. (@s-1)) {
        push(@{$code->{'goto'}}, $s[$f]);
      }
      next;
    }
  }
}

my @goto = uniq @{$code->{"goto"}};
my @gosub = uniq @{$code->{"gosub"}};
my @lnums = @{$code->{"lines"}};

comment('', '<<<GOTO>> ', join(', ', @goto));
comment('', '<<<GOSUB>> ', join(', ', @gosub));
cout '';
cout 'sub initialize {';
cout '  my $out = { "anchor" => "goto'.$lnums[0].'" }; return $out;';
cout '}';
cout '';
cout 'sub goto'.$lnums[0].' {';
$flag = 0;

for my $ll (0 .. (@{$code->{"lines"}}-1)) {
  my $l = $code->{"lines"}[$ll];
  my @ee = @{$code->{$l}};
  
  if ($l ~~ @gosub) {
    cout '  my $out = { "anchor" => "goto'.$l.'" }; return $out;' unless ($flag eq 2);
    cout '}';
    cout '';
    cout 'sub gosub'.$l.' {';
  }
  if ($l ~~ @goto) {
    cout '  my $out = { "anchor" => "goto'.$l.'" }; return $out;' unless ($flag eq 2);
    cout '}';
    cout '';
    cout 'sub goto'.$l.' {';
  }
  $flag = 0;
  for my $e (0 .. (@ee-1)) {
    my $expr = $ee[$e];
    if ($expr =~ /^REM /) {
      comment($e, $l, $expr);
    } elsif ($expr =~ /PRINT TAB.*"/) {
      comment($e, $l, $expr);
      my $msgt = $expr;
      $msgt =~ s/PRINT TAB\(//;
      $msgt =~ s/\).*//;
      my $p = $expr;
      $p =~ s/PRINT TAB\(.*\);//;
      cout('  gameMessageTab('.$msgt.', '.$p.');');
      $flag = 0;
#   } elsif (($expr =~ /PRINT.*"/) and ($expr !~ /"$/) and ($expr !~ /;$/)) {
#     comment($e, $l, $expr);
#     my $p = $expr;
#     $p =~ s/PRINT/  gameMessage(/;
#     print $p.":";
#     $flag = 1;
    } elsif ($expr =~ /PRINT.*"/) {
      comment($e, $l, $expr);
      my $p = $expr;
      $p =~ s/PRINT/  gameMessage(/;
      $p =~ s/$/");/;
      $p =~ s/";"\);/");/;
      $p =~ s/""\)/")/;
      $p =~ s/";(\w*|\w*\$);"/".\$state->{"$1"}."/g;
      $p =~ s/(\w*|\w*\$);"/\$state->{"$1"}."/;
      $p =~ s/";(\w*|\w*\$)/".\$state->{"$1"}/;
      $p =~ s/"(\w)\$"/"$1s"/g;
      cout $p;
      $flag = 0;
    } elsif ($expr =~ /PRINT/) {
      comment($e, $l, $expr);
      cout '  blank();';
      $flag = 0;
    } elsif ($expr =~ /GOSUB/) {
      comment($e, $l, $expr);
      my $g = $expr;
      $g =~ s/GOSUB//;
      $g = $g+0;
      cout '  gosub'.$g.'();';
      $flag = 0;
    } elsif ($expr =~ /^RETURN/) {
      comment($e, $l, $expr);
      cout '  return;';
      $flag = 0;
    } elsif ($expr =~ /FOR.*=.*/) {
      comment($e, $l, $expr);
      my $var_ = $expr;
      $var_ =~ s/FOR//;
      $var_ =~ s/=.*//;
      $var_ =~ s/ //g;
      my $i_ = $expr;
      $i_ =~ s/FOR.*=//;
      $i_ =~ s/TO.*//;
      $i_ =~ s/ //g;
      my $f_ = $expr;
      $f_ =~ s/FOR.*=.*TO//;
      $f_ =~ s/ //g;
      cout '  $state->{"'.$var_.'"} = '.($i_-1).';';
      cout '  my $out = { "anchor" => "next'.$var_.'" }; return $out;';
      cout '}';
      cout '';
      cout 'sub next'.$var_.' {';
      cout '  $state->{"'.$var_.'"} = $state->{"'.$var_.'"}+1;';
      cout '  if ($state->{"'.$var_.'"} > '.$f_.') {';
      cout '    $state->{"'.$var_.'"} = '.$f_.';';
      cout '    my $out = { "anchor" => "postnext'.$var_.'" }; return $out;';
      cout '  }';
      $flag = 0;
    } elsif ($expr =~ /NEXT.*/) {
      comment($e, $l, $expr);
      my $var_ = $expr;
      $var_ =~ s/NEXT//;
      $var_ =~ s/ //g;
      cout '  my $out = { "anchor" => "next'.$var_.'" }; return $out;';
      cout '}';
      cout '';
      cout 'sub postnext'.$var_.' {';
      $flag = 0;
    } elsif ($expr =~ /IF.*THEN.*/) {
      comment($e, $l, $expr);
      my $if_ = $expr;
      my $then_ = $expr;
      $if_ =~ s/IF//;
      $if_ =~ s/THEN.*//;
      $if_ =~ s/ //g;
      $if_ =~ s/INT/int/g;
      $if_ =~ s/RND\(1\)/rand()/g;
      $if_ =~ s/ABS/abs/g;
      $if_ =~ s/SQR/sqrt/g;
      $if_ =~ s/<=/ <= /g;
      $if_ =~ s/>=/ >= /g;
      $if_ =~ s/<>/ != /g;
      $if_ =~ s/(\w|\d|\))<(\w|\d|\()/$1 < $2/g;
      $if_ =~ s/(\w|\d|\))>(\w|\d|\()/$1 > $2/g;
      $then_ =~ s/IF.*THEN//;
      $then_ =~ s/ //;;
      cout '  if ( '.$if_.' ) { my $out = { "anchor" => "goto'.$then_.'" }; return $out; }';
      $flag = 2;
    } elsif ($expr =~ /INPUT/) {
      comment($e, $l, $expr);
      if ($expr =~ /INPUT.*"/) {
        my $s = $expr;
        $s =~ s/INPUT *"//;
        $s =~ s/";.*//;
        cout('  gameMessage("'.$s.'");');
      }
      cout('  my @argv = ("integer");');
      cout('  my $out = {');
      cout('    "anchor" => "goto'.$l.'postinput",');
      cout('    "echo" => " > ",');
      cout('    "argc" => 1,');
      cout('    "argv" => dclone(\@argv),');
      cout('    "hook" => "reply"');
      cout('  };');
      cout('  return $out;');
      cout('}');
      cout('');
      cout('sub goto'.$l.'postinput {');
      cout('  $state->{""} = $input->{"action"}[0];');
    } elsif ($expr =~ /ON.*GOTO/) {
      comment($e, $l, $expr);
      my $on_ = $expr;
      $on_ =~ s/ON//;
      $on_ =~ s/GOTO.*//;
      $on_ =~ s/ //g;
      my $to_ = $expr;
      $to_ =~ s/ON.*GOTO//;
      $to_ =~ s/ //g;
      my @g = split(',', $to_);
      for my $h (0 .. (@g-1)) {
        cout('  if ( $state->{"'.$on_.'"} == '.($h+1).' ) { my $out = { "anchor" => "goto'.$g[$h].'" }; return $out; }');
      }
    } elsif ($expr =~ /GOTO/) {
      comment($e, $l, $expr);
      my $g = $expr;
      $g =~ s/GOTO//;
      $g = $g+0;
      cout '  my $out = { "anchor" => "goto'.$g.'" }; return $out;';
      $flag = 2;
    } elsif (($expr =~ /END/) or ($expr =~ /STOP/)) {
      comment($e, $l, $expr);
      cout '  my $out = { "anchor" => "quit_game" }; return $out;';
      $flag = 2;
    } elsif ($expr =~ /=/) {
      comment($e, $l, $expr);
      my $v = $expr;
      my $a = $expr;
      $v =~ s/=.*//;
      $v =~ s/LET//;
      $v =~ s/ //g;
      $v =~ s/\$/s/;
      $a =~ s/.*=//;
      $a =~ s/INT/int/g;
      $a =~ s/RND\(1\)/rand()/g;
      $a =~ s/ABS/abs/g;
      $a =~ s/SQR/sqrt/g;
      if ($v =~ /.*\(.*\)/) {
        my $arr = $v;
        $arr =~ s/\(.*//;
        my $ind = $v;
        $ind =~ s/.*\(//;
        $ind =~ s/\)//;
        cout '  $state->{"'.$arr.'"}['.$ind.'] = '.$a.';';
      } else {
        cout '  $state->{"'.$v.'"} = '.$a.';';
      }
      $flag = 0;
    } elsif ($flag eq 1) {
      cout $expr.');';
      $flag = 0;
    } else {
      forcedcomment($e, $l, $expr);
      $flag = 0;
    }
  }
}

cout '}';
cout '';
cout '1;';


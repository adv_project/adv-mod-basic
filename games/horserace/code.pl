#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

  #(<<<GOTO>> ) 320, 670, 690, 630, 860, 880, 900, 920, 940, 960, 970, 1090, 1190, 1240, 720, 1370, 380
  #(<<<GOSUB>> ) 

$__horsies__ = '';

sub initialize {
  my $out = { "anchor" => "goto100" }; return $out;
  my @a = ();
  $state->{"Ws"} = dclone(\@a);
  $state->{"Sa"} = dclone(\@a);
  $state->{"Da"} = dclone(\@a);
  $state->{"Vs"} = dclone(\@a);
  $state->{"Oa"} = dclone(\@a);
  $state->{"Pa"} = dclone(\@a);
  $state->{"Ma"} = dclone(\@a);
  $state->{"Y"} = dclone(\@a);
  $state->{"Qa"} = dclone(\@a);
}

sub goto100 {
  # 100  PRINT TAB(31);"HORSERACE"
  gameMessageTab(31, "HORSERACE");
  # 110  PRINT TAB(15);"CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY"
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  # 120  PRINT
  blank();
  #(120) PRINT
  blank();
  #(120) PRINT
  blank();
  # 210  DIM S(8)
  # 220  PRINT "WELCOME TO SOUTH PORTLAND HIGH RACETRACK"
  gameMessage( "WELCOME TO SOUTH PORTLAND HIGH RACETRACK");
  # 230  PRINT "                      ...OWNED BY LAURIE CHEVALIER"
  gameMessage( "                      ...OWNED BY LAURIE CHEVALIER");
  # 240  PRINT "DO YOU WANT DIRECTIONS";
  gameMessage( "DO YOU WANT DIRECTIONS");
  # 250  INPUT X$
  my $out = {
    "anchor" => "goto250postinput",
    "echo" => " > ",
    "argc" => 1,
    "hook" => "reply"
  };
  return $out;
}

sub goto250postinput {
  $state->{"Xs"} = $input->{"action"}[0];
  # 260  IF X$="NO" THEN 320
  if ( $state->{"Xs"} eq "NO" ) { my $out = { "anchor" => "goto320" }; return $out; }
  # 270  PRINT"UP TO 10 MAY PLAY.  A TABLE OF ODDS WILL BE PRINTED.  YOU"
  gameMessage("UP TO 10 MAY PLAY.  A TABLE OF ODDS WILL BE PRINTED.  YOU");
  # 280  PRINT"MAY BET ANY + AMOUNT UNDER 100000 ON ONE HORSE."
  gameMessage("MAY BET ANY + AMOUNT UNDER 100000 ON ONE HORSE.");
  # 290  PRINT "DURING THE RACE, A HORSE WILL BE SHOWN BY ITS"
  gameMessage( "DURING THE RACE, A HORSE WILL BE SHOWN BY ITS");
  # 300  PRINT"NUMBER.  THE HORSES RACE DOWN THE PAPER!"
  gameMessage("NUMBER.  THE HORSES RACE DOWN THE PAPER!");
  # 310  PRINT
  blank();
  my $out = { "anchor" => "goto320" }; return $out;
}

sub goto320 {
  # 320  PRINT "HOW MANY WANT TO BET";
  gameMessage( "HOW MANY WANT TO BET");
  # 330  INPUT C
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto330postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto330postinput {
  $state->{"C"} = $input->{"action"}[0];
  # 340  PRINT "WHEN ? APPEARS,TYPE NAME"
  gameMessage( "WHEN ? APPEARS,TYPE NAME");
  # 350  FOR A=1 TO C
  $state->{"A"} = 0;
  my $out = { "anchor" => "nextA" }; return $out;
}

sub nextA {
  $state->{"A"} = $state->{"A"}+1;
  if ($state->{"A"} > $state->{"C"}) {
    $state->{"A"} = $state->{"C"};
    my $out = { "anchor" => "postnextA" }; return $out;
  }
  gameMessage(" ?");
  # 360  INPUT W$(A)
  my $out = {
    "anchor" => "goto360postinput",
    "echo" => " > ",
    "argc" => 1,
    "hook" => "reply"
  };
  return $out;
}

sub goto360postinput {
  $state->{"Ws"}[$state->{"A"}] = $input->{"action"}[0];
  # 370  NEXT A
  my $out = { "anchor" => "nextA" }; return $out;
}

sub postnextA {
  my $out = { "anchor" => "goto380" }; return $out;
}

sub goto380 {
  # 380  PRINT
  blank();
  # 390  PRINT"HORSE",,"NUMBER","ODDS"
  gameMessage(
    sprintf('%-32s',  "HORSE").
    sprintf('%-16s',  "NUMBER").
                      "ODDS");
  # 400  PRINT
  blank();
  # 410  FOR I=1 TO 8
  $state->{"I"} = 0;
  my $out = { "anchor" => "nextI" }; return $out;
}

sub nextI {
  $state->{"I"} = $state->{"I"}+1;
  if ($state->{"I"} > 8) {
    $state->{"I"} = 8;
    my $out = { "anchor" => "postnextI" }; return $out;
  }
  #(410)  S(I)=0
  $state->{"Sa"}[$state->{"I"}] = 0;
  #(410)  NEXT I
  my $out = { "anchor" => "nextI" }; return $out;
}

sub postnextI {
  # 420  LET R=0
  $state->{"R"} = 0;
  # 430  FOR A=1 TO 8
  $state->{"A"} = 0;
  my $out = { "anchor" => "nextA2" }; return $out;
}

sub nextA2 {
  $state->{"A"} = $state->{"A"}+1;
  if ($state->{"A"} > 8) {
    $state->{"A"} = 8;
    my $out = { "anchor" => "postnextA2" }; return $out;
  }
  # 440  LET D(A)=INT(10*RND(1)+1)
  $state->{"Da"}[$state->{"A"}] = int(10*rand()+1);
  # 450  NEXT A
  my $out = { "anchor" => "nextA2" }; return $out;
}

sub postnextA2 {
  # 460  FOR A=1 TO 8
  $state->{"A"} = 0;
  my $out = { "anchor" => "nextA3" }; return $out;
}

sub nextA3 {
  $state->{"A"} = $state->{"A"}+1;
  if ($state->{"A"} > 8) {
    $state->{"A"} = 8;
    my $out = { "anchor" => "postnextA3" }; return $out;
  }
  # 470  LET R=R+D(A)
  $state->{"R"} = $state->{"R"}+$state->{"Da"}[$state->{"A"}];
  # 480  NEXT A
  my $out = { "anchor" => "nextA3" }; return $out;
}

sub postnextA3 {
  # 490  LET V$(1)="JOE MAW"
  $state->{"Vs"}[1] = "JOE MAW";
  # 500  LET V$(2)="L.B.J."
  $state->{"Vs"}[2] = "L.B.J.";
  # 510  LET V$(3)="MR.WASHBURN"
  $state->{"Vs"}[3] = "MR.WASHBURN";
  # 520  LET V$(4)="MISS KAREN"
  $state->{"Vs"}[4] = "MISS KAREN";
  # 530  LET V$(5)="JOLLY"
  $state->{"Vs"}[5] = "JOLLY";
  # 540  LET V$(6)="HORSE"
  $state->{"Vs"}[6] = "HORSE";
  # 550  LET V$(7)="JELLY DO NOT"
  $state->{"Vs"}[7] = "JELLY DO NOT";
  # 560  LET V$(8)="MIDNIGHT"
  $state->{"Vs"}[8] = "MIDNIGHT";
  # 570  FOR N=1 TO 8
  $state->{"N"} = 0;
  my $out = { "anchor" => "nextN" }; return $out;
}

sub nextN {
  $state->{"N"} = $state->{"N"}+1;
  if ($state->{"N"} > 8) {
    $state->{"N"} = 8;
    my $out = { "anchor" => "postnextN" }; return $out;
  }
  # 580  PRINT V$(N),,N,R/D(N);"
  #(580) 1"
  my $f = sprintf('%.5f', ($state->{"R"}/$state->{"Da"}[$state->{"N"}]) );
  gameMessage(
    sprintf('%-32s',  $state->{"Vs"}[$state->{"N"}]).
    sprintf('%-16s',  $state->{"N"}).
    sprintf('%-9f',   $f) . " :1");
  # 590  NEXT N
  my $out = { "anchor" => "nextN" }; return $out;
}

sub postnextN {
  # 600  PRINT"--------------------------------------------------"
  gameMessage("--------------------------------------------------");
  # 610  PRINT "PLACE YOUR BETS...HORSE # THEN AMOUNT"
  gameMessage( "PLACE YOUR BETS...HORSE # THEN AMOUNT");
  # 620  FOR J=1 TO C
  $state->{"J"} = 0;
  my $out = { "anchor" => "nextJ" }; return $out;
}

sub nextJ {
  $state->{"J"} = $state->{"J"}+1;
  if ($state->{"J"} > $state->{"C"} ) {
    $state->{"J"} = $state->{"C"};
    my $out = { "anchor" => "postnextJ" }; return $out;
  }
  my $out = { "anchor" => "goto630" }; return $out;
}

sub goto630 {
  # 630  PRINT W$(J);
  gameMessage($state->{"Ws"}[$state->{"J"}]."?");
  # 640  INPUT Q(J),P(J)
  my @argv = ("integer", "integer");
  my $out = {
    "anchor" => "goto640postinput",
    "echo" => " > ",
    "argc" => 2,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto640postinput {
  $state->{"Qa"}[$state->{"J"}] = $input->{"action"}[0];
  $state->{"Pa"}[$state->{"J"}] = $input->{"action"}[0];
  # 650  IF P(J)<1 THEN 670
  if ( $state->{"Pa"}[$state->{"J"}] < 1 ) { my $out = { "anchor" => "goto670" }; return $out; }
  # 660  IF P(J)<100000 THEN 690
  if ( $state->{"Pa"}[$state->{"J"}] < 100000 ) { my $out = { "anchor" => "goto690" }; return $out; }
  my $out = { "anchor" => "goto670" }; return $out;
}

sub goto670 {
  # 670  PRINT"  YOU CAN'T DO THAT!"
  gameMessage("  YOU CAN'T DO THAT!");
  # 680  GOTO 630
  my $out = { "anchor" => "goto630" }; return $out;
}

sub goto690 {
  # 690  NEXT J
  my $out = { "anchor" => "nextJ" }; return $out;
}

sub postnextJ {
  # 700  PRINT
  blank();
  # 710  PRINT"1 2 3 4 5 6 7 8"
  gameMessage("1 2 3 4 5 6 7 8");
  my $out = { "anchor" => "goto720" }; return $out;
}

sub goto720 {
  # 720  PRINT"XXXXSTARTXXXX"
  gameMessage("XXXXSTARTXXXX");
  # 730  FOR I=1 TO 8
  $state->{"I"} = 0;
  my $out = { "anchor" => "nextI2" }; return $out;
}

sub nextI2 {
  $state->{"I"} = $state->{"I"}+1;
  if ($state->{"I"} > 8) {
    $state->{"I"} = 8;
    my $out = { "anchor" => "postnextI2" }; return $out;
  }
  # 740  LET M=I
  $state->{"M"} = $state->{"I"};
  # 750  LET M(I)=M
  $state->{"Ma"}[$state->{"I"}] = $state->{"M"};
  writeState();
  # 760  LET Y(M(I))=INT(100*RND(1)+1)
  $state->{"Ya"}[$state->{"Ma"}[$state->{"I"}]] = int(100*rand()+1);
  # 770  IF Y(M(I))<10 THEN 860
  if ( $state->{"Ya"}[$state->{"Ma"}[$state->{"I"}]] < 10 ) { my $out = { "anchor" => "goto860" }; return $out; }
  writeState();
  # 780  LET S=INT(R/D(I)+.5)
  $state->{"S"} = int($state->{"R"}/$state->{"Da"}[$state->{"I"}]+.5);
  # 790  IF Y(M(I))<S+17 THEN 880
  if ( $state->{"Ya"}[$state->{"Ma"}[$state->{"I"}]] < $state->{"S"}+17 ) { my $out = { "anchor" => "goto880" }; return $out; }
  # 800  IF Y(M(I))<S+37 THEN 900
  if ( $state->{"Ya"}[$state->{"Ma"}[$state->{"I"}]] <S+37 ) { my $out = { "anchor" => "goto900" }; return $out; }
  # 810  IF Y(M(I))<S+57 THEN 920
  if ( $state->{"Ya"}[$state->{"Ma"}[$state->{"I"}]] <S+57 ) { my $out = { "anchor" => "goto920" }; return $out; }
  # 820  IF Y(M(I))<77+S THEN 940
  if ( $state->{"Ya"}[$state->{"Ma"}[$state->{"I"}]] <77+S ) { my $out = { "anchor" => "goto940" }; return $out; }
  # 830  IF Y(M(I))<S+92 THEN 960
  if ( $state->{"Ya"}[$state->{"Ma"}[$state->{"I"}]] <S+92 ) { my $out = { "anchor" => "goto960" }; return $out; }
  # 840  LET Y(M(I))=7
  $state->{"Ya"}[$state->{"Ma"}[$state->{"I"}]] = 7;
  # 850  GOTO 970
  my $out = { "anchor" => "goto970" }; return $out;
}

sub goto860 {
  # 860  LET Y(M(I))=1
  $state->{"Ya"}[$state->{"Ma"}[$state->{"I"}]] = 1;
  # 870  GOTO 970
  my $out = { "anchor" => "goto970" }; return $out;
}

sub goto880 {
  # 880  LET Y(M(I))=2
  $state->{"Ya"}[$state->{"Ma"}[$state->{"I"}]] = 2;
  # 890  GOTO 970
  my $out = { "anchor" => "goto970" }; return $out;
}

sub goto900 {
  # 900  LET Y(M(I))=3
  $state->{"Ya"}[$state->{"Ma"}[$state->{"I"}]] = 3;
  # 910  GOTO 970
  my $out = { "anchor" => "goto970" }; return $out;
}

sub goto920 {
  # 920  LET Y(M(I))=4
  $state->{"Ya"}[$state->{"Ma"}[$state->{"I"}]] = 4;
  # 930  GOTO 970
  my $out = { "anchor" => "goto970" }; return $out;
}

sub goto940 {
  # 940  LET Y(M(I))=5
  $state->{"Ya"}[$state->{"Ma"}[$state->{"I"}]] = 5;
  # 950  GOTO 970
  my $out = { "anchor" => "goto970" }; return $out;
}

sub goto960 {
  # 960  LET Y(M(I))=6
  $state->{"Ya"}[$state->{"Ma"}[$state->{"I"}]] = 6;
  my $out = { "anchor" => "goto970" }; return $out;
}

sub goto970 {
  # 970  NEXT I
  my $out = { "anchor" => "nextI2" }; return $out;
}

sub postnextI2 {
  # 980  LET M=I
  $state->{"M"} = $state->{"I"};
  # 990  FOR I=1 TO 8
  $state->{"I"} = 0;
  my $out = { "anchor" => "nextI3" }; return $out;
}

sub nextI3 {
  $state->{"I"} = $state->{"I"}+1;
  if ($state->{"I"} > 8) {
    $state->{"I"} = 8;
    my $out = { "anchor" => "postnextI3" }; return $out;
  }
  # 1000  LET S(M(I))=S(M(I))+Y(M(I))
  $state->{"Sa"}[$state->{"Ma"}[$state->{"I"}]] = $state->{"Sa"}[$state->{"Ma"}[$state->{"I"}]]+$state->{"Ya"}[$state->{"Ma"}[$state->{"I"}]];
  # 1010  NEXT I
  my $out = { "anchor" => "nextI3" }; return $out;
}

sub postnextI3 {
  # 1020  LET I=1
  $state->{"I"} = 1;
  # 1030  FOR L=1 TO 8
  $state->{"L"} = 0;
  my $out = { "anchor" => "nextL" }; return $out;
}

sub nextL {
  $state->{"L"} = $state->{"L"}+1;
  if ($state->{"L"} > 8) {
    $state->{"L"} = 8;
    my $out = { "anchor" => "postnextL" }; return $out;
  }
  # 1040  FOR I=1 TO 8-L
  $state->{"I"} = 0;
  my $out = { "anchor" => "nextI4" }; return $out;
}

sub nextI4 {
  $state->{"I"} = $state->{"I"}+1;
  if ($state->{"I"} > 8-$state->{"L"}) {
    $state->{"I"} = 8-$state->{"L"};
    my $out = { "anchor" => "postnextI4" }; return $out;
  }
  # 1050  IF S(M(I))<S(M(I+1))THEN 1090
  if ( $state->{"Sa"}[$state->{"Ma"}[$state->{"I"}]] < $state->{"Sa"}[$state->{"Ma"}[$state->{"I"}+1]] ) { my $out = { "anchor" => "goto1090" }; return $out; }
  # 1060  LET H=M(I)
  $state->{"H"} = $state->{"Ma"}[$state->{"I"}];
  # 1070  LET M(I)=M(I+1)
  $state->{"Ma"}[$state->{"I"}] = $state->{"Ma"}[$state->{"I"}+1];
  # 1080  LET M(I+1)=H
  $state->{"Ma"}[$state->{"I"}+1] = $state->{"H"};
  my $out = { "anchor" => "goto1090" }; return $out;
}

sub goto1090 {
  # 1090  NEXT I
  my $out = { "anchor" => "nextI4" }; return $out;
}

sub postnextI4 {
  # 1100  NEXT L
  my $out = { "anchor" => "nextL" }; return $out;
}

sub postnextL {
  # 1110  LET T=S(M(8))
  $state->{"T"} = $state->{"Sa"}[$state->{"Ma"}[8]];
  # 1120  FOR I=1 TO 8
  $state->{"I"} = 0;
  my $out = { "anchor" => "nextI5" }; return $out;
}

sub nextI5 {
  $state->{"I"} = $state->{"I"}+1;
  if ($state->{"I"} > 8) {
    $state->{"I"} = 8;
    my $out = { "anchor" => "postnextI5" }; return $out;
  }
  # 1130  LET B=S(M(I))-S(M(I-1))
  $state->{"B"} = $state->{"Sa"}[$state->{"Ma"}[$state->{"I"}]]-$state->{"Sa"}[$state->{"Ma"}[$state->{"I"}-1]];
  # 1140  IF B=0 THEN 1190
  if ( $state->{"B"} == 0 ) { my $out = { "anchor" => "goto1190" }; return $out; }
  # 1150  FOR A=1 TO B
  $state->{"A"} = 0;
  my $out = { "anchor" => "nextA4" }; return $out;
}

sub nextA4 {
  $state->{"A"} = $state->{"A"}+1;
  if ($state->{"A"} > $state->{"B"}) {
    $state->{"A"} = $state->{"B"};
    my $out = { "anchor" => "postnextA4" }; return $out;
  }
  # 1160  PRINT
  #blank();
  gameMessage($__horsies__);
  $__horsies__ = '';
  # 1170  IF S(M(I))>27 THEN 1240
  if ( $state->{"Sa"}[$state->{"Ma"}[$state->{"I"}]] > 27 ) { my $out = { "anchor" => "goto1240" }; return $out; }
  # 1180  NEXT A
  my $out = { "anchor" => "nextA4" }; return $out;
}

sub postnextA4 {
  my $out = { "anchor" => "goto1190" }; return $out;
}

sub goto1190 {
  # 1190  PRINT M(I); ####
  $__horsies__ = $__horsies__.' '.$state->{"Ma"}[$state->{"I"}];
  # 1200  NEXT I
  my $out = { "anchor" => "nextI5" }; return $out;
}

sub postnextI5 {
  # 1210  FOR A=1 TO 28-T
  $state->{"A"} = 0;
  my $out = { "anchor" => "nextA5" }; return $out;
}

sub nextA5 {
  $state->{"A"} = $state->{"A"}+1;
  if ($state->{"A"} > 28-$state->{"T"}) {
    $state->{"A"} = 28-$state->{"T"};
    my $out = { "anchor" => "postnextA5" }; return $out;
  }
  # 1220  PRINT
  blank();
  # 1230  NEXT A
  my $out = { "anchor" => "nextA5" }; return $out;
}

sub postnextA5 {
  my $out = { "anchor" => "goto1240" }; return $out;
}

sub goto1240 {
  # 1240  PRINT "XXXXFINISHXXXX";
  gameMessage( "XXXXFINISHXXXX");
  # 1242  PRINT
  blank();
  # 1243  PRINT
  blank();
  # 1244  PRINT "---------------------------------------------"
  gameMessage( "---------------------------------------------");
  # 1245  PRINT
  #blank();
  gameMessage( "ENTER ANY VALUE TO CONTINUE...");
  my $out = {
    "anchor" => "goto1245postinput",
    "hook" => "press_any_key"
  };
  return $out;
}

sub goto1245postinput {
  # 1250  IF T<28 THEN 720
  if ( $state->{"T"} < 28 ) { my $out = { "anchor" => "goto720" }; return $out; }
  # 1270  PRINT "THE RACE RESULTS ARE
  gameMessage( "THE RACE RESULTS ARE:");
  # 1272  LET Z9=1
  $state->{"Z9"} = 1;
  # 1280  FOR I=8 TO 1 STEP-1
  $state->{"I"} = 9;
  my $out = { "anchor" => "nextI6" }; return $out;
}

sub nextI6 {
  $state->{"I"} = $state->{"I"}-1;
  if ($state->{"I"} < 1) {
    $state->{"I"} = 1;
    my $out = { "anchor" => "postnextI6" }; return $out;
  }
  # 1290  LET F=M(I)
  $state->{"F"} = $state->{"Ma"}[$state->{"I"}];
  # 1300  PRINT
  blank();
  # 1310  PRINT Z9;"PLACE HORSE NO.";F,"AT ";R/D(F);"
  gameMessage(
    sprintf('%-32s', $state->{"Z9"}." PLACE HORSE NO.".$state->{"F"})."AT ".$state->{"R"}/$state->{"Da"}[$state->{"F"}]." :1");
  # 1312  LET Z9=Z9+1
  $state->{"Z9"} = $state->{"Z9"}+1;
  # 1320   NEXT I
  my $out = { "anchor" => "nextI6" }; return $out;
}

sub postnextI6 {
  # 1330  FOR J=1 TO C
  $state->{"J"} = 0;
  my $out = { "anchor" => "nextJ2" }; return $out;
}

sub nextJ2 {
  $state->{"J"} = $state->{"J"}+1;
  if ($state->{"J"} > $state->{"C"}) {
    $state->{"J"} = $state->{"C"};
    my $out = { "anchor" => "postnextJ2" }; return $out;
  }
  # 1340  IF Q(J)<>M(8) THEN 1370
  if ( $state->{"Qa"}[$state->{"J"}] != $state->{"Ma"}[8] ) { my $out = { "anchor" => "goto1370" }; return $out; }
  # 1350  LET N=Q(J)
  $state->{"N"} = $state->{"Qa"}[$state->{"J"}];
  # 1355  PRINT
  blank();
  # 1360  PRINT W$(J);" WINS $";(R/D(N))*P(J)
  gameMessage( $state->{"Ws"}[$state->{"J"}]." WINS \$".(($state->{"R"}/$state->{"Da"}[$state->{"N"}])*$state->{"Pa"}[$state->{"J"}]));
  my $out = { "anchor" => "goto1370" }; return $out;
}

sub goto1370 {
  # 1370  NEXT J
  my $out = { "anchor" => "nextJ2" }; return $out;
}

sub postnextJ2 {
  # 1372  PRINT "DO YOU WANT TO BET ON THE NEXT RACE ?"
  gameMessage( "DO YOU WANT TO BET ON THE NEXT RACE ?");
  # 1374  INPUT "YES OR NO"; O$
  gameMessage( "YES OR NO");
  my $out = {
    "anchor" => "goto1374postinput",
    "echo" => " > ",
    "argc" => 1,
    "hook" => "reply"
  };
  return $out;
}

sub goto1374postinput {
  $state->{"Os"} = $input->{"action"}[0];
  # 1376  IF O$="YES" THEN 380
  if ( $state->{"Os"} eq "YES" ) { my $out = { "anchor" => "goto380" }; return $out; }
  # 1380  END
  my $out = { "anchor" => "quit_game" }; return $out;
}

1;

#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

  #(<<<GOTO>> ) 1550, 1630, 1680, 1780, 1830, 1920, 1900, 2020, 2000, 1980, 2710, ONT(C)-362090,2190,2220,2250,2300,2350,2400,2470,2500, ONT(C)-452530,2560,2630, 2150, 2180, 2810, 2110, 2430, 2760, 2780, 2880, 3190, 2920, 2960, 3210, 3420
  #(<<<GOSUB>> ) 3230

use Storable qw(dclone);

sub restore {
  $state->{"cur"} = 0;
}

sub initialize {
  # 2950  DATA 1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36
  my @_data = (1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36);
  $state->{"DATA"} = dclone(\@_data);
  restore();
  my $out = { "anchor" => "goto10" }; return $out;
}

sub goto10 {
  # 10  PRINT TAB(32);"ROULETTE"
  gameMessageTab(32, "ROULETTE");
  # 20  PRINT TAB(15);"CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY"
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  # 30  PRINT
  blank();
  #(30) PRINT
  blank();
  #(30) PRINT
  blank();
  # 40  PRINT "ENTER THE CURRENT DATE (AS IN 'JANUARY 23, 1979') -";
  gameMessage( "ENTER THE CURRENT DATE (AS IN 'JANUARY 23, 1979') -");
  # 50  INPUT D$,E$
  my $out = {
    "anchor" => "goto50postinput",
    "echo" => " > ",
    "argc" => 3,
    "hook" => "reply"
  };
  return $out;
}

sub goto50postinput {
  $state->{"Ds"} = $input->{"action"}[0].' '.$input->{"action"}[1];
  $state->{"Ds"} =~ s/,//;
  $state->{"Es"} = $input->{"action"}[2];
  # 1000  REM-ROULETTE
  # 1010  REM-DAVID JOSLIN
  # 1020  PRINT "WELCOME TO THE ROULETTE TABLE"
  gameMessage( "WELCOME TO THE ROULETTE TABLE");
  # 1030  PRINT 
  blank();
  # 1040  PRINT "DO YOU WANT INSTRUCTIONS";
  gameMessage( "DO YOU WANT INSTRUCTIONS");
  # 1050  INPUT Y$
  my $out = {
    "anchor" => "goto1050postinput",
    "echo" => " > ",
    "hook" => "reply"
  };
  return $out;
}

sub goto1050postinput {
  $state->{"Ys"} = $input->{"action"}[0];
  # 1060  IF LEFT$(Y$,1)="N" THEN 1550
  if ( $state->{"Ys"} =~ /^N/ ) { my $out = { "anchor" => "goto1550" }; return $out; }
  # 1070  PRINT
  blank();
  # 1080  PRINT "THIS IS THE BETTING LAYOUT"
  gameMessage( "THIS IS THE BETTING LAYOUT");
  # 1090  PRINT "  (*=RED)"
  gameMessage( "  (*=RED)");
  # 1100  PRINT 
  blank();
  # 1110  PRINT " 1*    2     3*"
  gameMessage( " 1*    2     3*");
  # 1120  PRINT " 4     5*    6 "
  gameMessage( " 4     5*    6 ");
  # 1130  PRINT " 7*    8     9*"
  gameMessage( " 7*    8     9*");
  # 1140  PRINT "10    11    12*"
  gameMessage( "10    11    12*");
  # 1150  PRINT "---------------"
  gameMessage( "---------------");
  # 1160  PRINT "13    14*   15 "
  gameMessage( "13    14*   15 ");
  # 1170  PRINT "16*   17    18*"
  gameMessage( "16*   17    18*");
  # 1180  PRINT "19*   20    21*"
  gameMessage( "19*   20    21*");
  # 1190  PRINT "22    23*   24 "
  gameMessage( "22    23*   24 ");
  # 1200  PRINT "---------------"
  gameMessage( "---------------");
  # 1210  PRINT "25*   26    27*"
  gameMessage( "25*   26    27*");
  # 1220  PRINT "28    29    30*"
  gameMessage( "28    29    30*");
  # 1230  PRINT "31    32*   33 "
  gameMessage( "31    32*   33 ");
  # 1240  PRINT "34*   35    36*"
  gameMessage( "34*   35    36*");
  # 1250  PRINT "---------------"
  gameMessage( "---------------");
  # 1260  PRINT "    00    0    "
  gameMessage( "    00    0    ");
  # 1270  PRINT
  blank();
  # 1280  PRINT "TYPES OF BETS"
  gameMessage( "TYPES OF BETS");
  # 1290  PRINT 
  blank();
  # 1300  PRINT "THE NUMBERS 1 TO 36 SIGNIFY A STRAIGHT BET"
  gameMessage( "THE NUMBERS 1 TO 36 SIGNIFY A STRAIGHT BET");
  # 1310  PRINT "ON THAT NUMBER."
  gameMessage( "ON THAT NUMBER.");
  # 1320  PRINT "THESE PAY OFF 35
  gameMessage( "THESE PAY OFF 35");
  #(1320) 1"
  # 1330  PRINT 
  blank();
  # 1340  PRINT "THE 2
  #(1340) 1 BETS ARE
  #(1340) "
  gameMessage( "THE 2:1 BETS ARE:");
  # 1350  PRINT " 37) 1-12     40) FIRST COLUMN"
  gameMessage( " 37) 1-12     40) FIRST COLUMN");
  # 1360  PRINT " 38) 13-24    41) SECOND COLUMN"
  gameMessage( " 38) 13-24    41) SECOND COLUMN");
  # 1370  PRINT " 39) 25-36    42) THIRD COLUMN"
  gameMessage( " 39) 25-36    42) THIRD COLUMN");
  # 1380  PRINT 
  blank();
  # 1390  PRINT "THE EVEN MONEY BETS ARE
  #(1390) "
  gameMessage( "THE EVEN MONEY BETS ARE:");
  # 1400  PRINT " 43) 1-18     46) ODD"
  gameMessage( " 43) 1-18     46) ODD");
  # 1410  PRINT " 44) 19-36    47) RED"
  gameMessage( " 44) 19-36    47) RED");
  # 1420  PRINT " 45) EVEN     48) BLACK"
  gameMessage( " 45) EVEN     48) BLACK");
  # 1430  PRINT 
  blank();
  # 1440  PRINT " 49)0 AND 50)00 PAY OFF 35
  gameMessage( " 49)0 AND 50)00 PAY OFF 35:1");
  #(1440) 1"
  # 1450  PRINT " NOTE
  #(1450)  0 AND 00 DO NOT COUNT UNDER ANY"
  gameMessage( " NOTE: 0 AND 00 DO NOT COUNT UNDER ANY");
  # 1460  PRINT "       BETS EXCEPT THEIR OWN."
  gameMessage( "       BETS EXCEPT THEIR OWN.");
  # 1470  PRINT
  blank();
  # 1480  PRINT "WHEN I ASK FOR EACH BET, TYPE THE NUMBER"
  gameMessage( "WHEN I ASK FOR EACH BET, TYPE THE NUMBER");
  # 1490  PRINT "AND THE AMOUNT, SEPARATED BY A COMMA."
  gameMessage( "AND THE AMOUNT, SEPARATED BY A SPACE.");
  # 1500  PRINT "FOR EXAMPLE
  #(1500)  TO BET $500 ON BLACK, TYPE 48,500"
  gameMessage( "FOR EXAMPLE: TO BET \$500 ON BLACK, TYPE \"48 500\"");
  # 1510  PRINT "WHEN I ASK FOR A BET."
  gameMessage( "WHEN I ASK FOR A BET.");
  # 1520  PRINT 
  blank();
  # 1530  PRINT "THE MINIMUM BET IS $5, THE MAXIMUM IS $500."
  gameMessage( "THE MINIMUM BET IS \$5, THE MAXIMUM IS \$500.");
  # 1540  PRINT 
  blank();
  my $out = { "anchor" => "goto1550" }; return $out;
}

sub goto1550 {
  # 1550  REM-PROGRAM BEGINS HERE
  # 1560  REM-TYPE OF BET(NUMBER) ODDS
  # 1570  REM  DON'T NEED TO DIMENSION STRINGS
  # 1580  DIM B(100),C(100),T(100),X(38)
  my $_a = ();
  $state->{"B"} = dclone(\@_a);
  $state->{"C"} = dclone(\@_a);
  $state->{"T"} = dclone(\@_a);
  $state->{"X"} = dclone(\@_a);
  # 1590  DIM A(50)
  $state->{"A"} = dclone(\@_a);
  # 1600  FOR I=1 TO 38
  #(1600)  X(I)=0
  #(1600)  NEXT I
  for my $_i (1 .. 38) { $state->{"X"}[$_i] = 0; }
  my $out = { "anchor" => "postnextI" }; return $out;
}

sub postnextI {
  #(1600)  REM  MAT X=ZER
  # 1610  P=1000
  $state->{"P"} = 1000;
  # 1620  D=100000.
  $state->{"D"} = 100000.0;
  my $out = { "anchor" => "goto1630" }; return $out;
}

sub goto1630 {
  # 1630  PRINT "HOW MANY BETS";
  gameMessage( "HOW MANY BETS");
  # 1640  INPUT Y
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto1640postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto1640postinput {
  $state->{"Y"} = $input->{"action"}[0];
  # 1650  IF Y<1 OR Y<>INT(Y) THEN 1630
  if ($state->{"Y"} < 1 or $state->{"Y"} != int($state->{"Y"})) {
    my $out = { "anchor" => "goto1630" }; return $out;
  }
  # 1660  FOR I=1 TO 50
  #(1660)  A(I)=0
  #(1660)  NEXT I
  for my $_i (1 .. 50) { $state->{"A"}[$_i] = 0; }
  #(1660)  REM  MAT A=ZER
  # 1670  FOR C=1 TO Y
  $state->{"C"} = 0;
  my $out = { "anchor" => "nextC" }; return $out;
}

sub nextC {
  $state->{"C"} = $state->{"C"}+1;
  if ($state->{"C"} > $state->{"Y"}) {
    $state->{"C"} = $state->{"Y"};
    my $out = { "anchor" => "postnextC" }; return $out;
  }
  my $out = { "anchor" => "goto1680" }; return $out;
}

sub goto1680 {
  # 1680  PRINT "NUMBER";C;
  gameMessage( "NUMBER ".$state->{"C"});
  # 1690  INPUT X,Z
  my @argv = ("integer", "integer");
  my $out = {
    "anchor" => "goto1690postinput",
    "echo" => " > ",
    "argc" => 2,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto1690postinput {
  $state->{"X"} = $input->{"action"}[0];
  $state->{"Z"} = $input->{"action"}[1];
  # 1700  B(C)=Z
  $state->{"B"}[$state->{"C"}] = $state->{"Z"};
  # 1710  T(C)=X
  $state->{"T"}[$state->{"C"}] = $state->{"X"};
  # 1720  IF X<1 OR X>50 OR X<>INT(X) THEN 1680
  if ( $state->{"X"} < 1
    or $state->{"X"} > 50
    or $state->{"X"} != int($state->{"X"})
  ) {
    my $out = { "anchor" => "goto1680" }; return $out;
  }
  # 1730  IF Z<1 OR Z<>INT(Z) THEN 1680
  if ($state->{"Z"} < 1 or $state->{"Z"} != int($state->{"Z"})) {
    my $out = { "anchor" => "goto1680" }; return $out;
  }
  # 1740  IF Z<5 OR Z>500 THEN 1680
  if ($state->{"Z"} < 5 or $state->{"Z"} > 500 ) {
    my $out = { "anchor" => "goto1680" }; return $out;
  }
  # 1750  IF A(X)=0 THEN 1780
  if ( $state->{"A"}[$state->{"X"}] == 0 ) {
    my $out = { "anchor" => "goto1780" }; return $out;
  }
  # 1760  PRINT "YOU MADE THAT BET ONCE ALREADY,DUM-DUM"
  gameMessage( "YOU MADE THAT BET ONCE ALREADY,DUM-DUM");
  # 1770  GOTO 1680
  my $out = { "anchor" => "goto1680" }; return $out;
}

sub goto1780 {
  # 1780  A(X)=1
  $state->{"A"}[$state->{"X"}] = 1;
  # 1790  NEXT C
  my $out = { "anchor" => "nextC" }; return $out;
}

sub postnextC {
  # 1800  PRINT "SPINNING"
  gameMessage( "SPINNING");
  # 1810  PRINT 
  blank();
  # 1820  PRINT 
  blank();
  my $out = { "anchor" => "goto1830" }; return $out;
}

sub goto1830 {
  # 1830  S=INT(RND(1)*100)
  $state->{"S"} = int(rand()*100);
  # 1840  IF S=0 OR S>38 THEN 1830
  if ( $state->{"S"} == 0 or $state->{"S"} > 38 ) {
    my $out = { "anchor" => "goto1830" }; return $out;
  }
  # 1850  X(S)=X(S)+1
  $state->{"X"}[$state->{"S"}] = $state->{"X"}[$state->{"S"}]+1;
  # 1860  IF S<37 THEN 1920
  if ( $state->{"S"} < 37 ) {
    my $out = { "anchor" => "goto1920" }; return $out;
  }
  # 1870  IF S=37 THEN 1900
  if ( $state->{"S"} == 37 ) {
    my $out = { "anchor" => "goto1900" }; return $out;
  }
  # 1880  PRINT "00"
  gameMessage( "00");
  # 1890  GOTO 2020
  my $out = { "anchor" => "goto2020" }; return $out;
}

sub goto1900 {
  # 1900  PRINT "0"
  gameMessage( "0");
  # 1910  GOTO 2020
  my $out = { "anchor" => "goto2020" }; return $out;
}

sub goto1920 {
  # 1920  RESTORE 
  restore();
  # 1930  FOR I1=1 TO 18
  $state->{"I1"} = 0;
  my $out = { "anchor" => "nextI1" }; return $out;
}

sub nextI1 {
  $state->{"I1"} = $state->{"I1"}+1;
  if ($state->{"I1"} > 18) {
    $state->{"I1"} = 18;
    my $out = { "anchor" => "postnextI1" }; return $out;
  }
  # 1940  READ R
  $state->{"R"} = $state->{"DATA"}[$state->{"cur"}];
  $state->{"cur"} = $state->{"cur"}+1;
  # 1950  IF R=S THEN 2000
  if ( $state->{"R"} == $state->{"S"} ) {
    my $out = { "anchor" => "goto2000" }; return $out;
  }
  # 1960  NEXT I1
  my $out = { "anchor" => "nextI1" }; return $out;
}

sub postnextI1 {
  # 1970  A$="BLACK"
  $state->{"As"} = "BLACK";
  my $out = { "anchor" => "goto1980" }; return $out;
}

sub goto1980 {
  # 1980  PRINT S;A$
  gameMessage( $state->{"S"}." ".$state->{"As"});
  # 1990  GOTO 2020
  my $out = { "anchor" => "goto2020" }; return $out;
}

sub goto2000 {
  # 2000  A$="RED"
  $state->{"As"} = "RED";
  # 2010  GOTO 1980
  my $out = { "anchor" => "goto1980" }; return $out;
}

sub goto2020 {
  # 2020  PRINT
  blank();
  # 2030  FOR C=1 TO Y
  $state->{"C"} = 0;
  my $out = { "anchor" => "nextCa" }; return $out;
}

sub nextCa {
  $state->{"C"} = $state->{"C"}+1;
  if ($state->{"C"} > $state->{"Y"}) {
    $state->{"C"} = $state->{"Y"};
    my $out = { "anchor" => "postnextCa" }; return $out;
  }
  # 2040  IF T(C)<37 THEN 2710
  if ( $state->{"T"}[$state->{"C"}] < 37 ) { my $out = { "anchor" => "goto2710" }; return $out; }
  # 2050  ON T(C)-36 GOTO 2090,2190,2220,2250,2300,2350,2400,2470,2500
  if ( $state->{"T"}[$state->{"C"}]-36 == 1 ) { my $out = { "anchor" => "goto2090" }; return $out; }
  if ( $state->{"T"}[$state->{"C"}]-36 == 2 ) { my $out = { "anchor" => "goto2190" }; return $out; }
  if ( $state->{"T"}[$state->{"C"}]-36 == 3 ) { my $out = { "anchor" => "goto2220" }; return $out; }
  if ( $state->{"T"}[$state->{"C"}]-36 == 4 ) { my $out = { "anchor" => "goto2250" }; return $out; }
  if ( $state->{"T"}[$state->{"C"}]-36 == 5 ) { my $out = { "anchor" => "goto2300" }; return $out; }
  if ( $state->{"T"}[$state->{"C"}]-36 == 6 ) { my $out = { "anchor" => "goto2350" }; return $out; }
  if ( $state->{"T"}[$state->{"C"}]-36 == 7 ) { my $out = { "anchor" => "goto2400" }; return $out; }
  if ( $state->{"T"}[$state->{"C"}]-36 == 8 ) { my $out = { "anchor" => "goto2470" }; return $out; }
  if ( $state->{"T"}[$state->{"C"}]-36 == 9 ) { my $out = { "anchor" => "goto2500" }; return $out; }
  # 2060  ON T(C)-45 GOTO 2530,2560,2630
  if ( $state->{"T"}[$state->{"C"}]-45 == 1 ) { my $out = { "anchor" => "goto2530" }; return $out; }
  if ( $state->{"T"}[$state->{"C"}]-45 == 2 ) { my $out = { "anchor" => "goto2560" }; return $out; }
  if ( $state->{"T"}[$state->{"C"}]-45 == 3 ) { my $out = { "anchor" => "goto2630" }; return $out; }
  # 2070  GOTO 2710
  my $out = { "anchor" => "goto2710" }; return $out;
  # 2080  STOP
  my $out = { "anchor" => "quit_game" }; return $out;
}

sub goto2090 {
  # 2090  REM  1-12(37) 2
  #(2090) 1
  # 2100  IF S <= 12 THEN 2150
  if ( $state->{"S"} <= 12 ) { my $out = { "anchor" => "goto2150" }; return $out; }
  my $out = { "anchor" => "goto2110" }; return $out;
}

sub goto2110 {
  # 2110  PRINT "YOU LOSE";B(C);"DOLLARS ON BET";C
  gameMessage( "YOU LOSE ".$state->{"B"}[$state->{"C"}]." DOLLARS ON BET ".$state->{"C"});
  # 2120  D=D+B(C)
  $state->{"D"} = $state->{"D"}+$state->{"B"}[$state->{"C"}];
  # 2130  P=P-B(C)
  $state->{"P"} = $state->{"P"}-$state->{"B"}[$state->{"C"}];
  # 2140  GOTO 2180
  my $out = { "anchor" => "goto2180" }; return $out;
}

sub goto2150 {
  # 2150  PRINT "YOU WIN";B(C)*2;"DOLLARS ON BET"C
  my $_w = $state->{"B"}[$state->{"C"}]*2;
  gameMessage( "YOU WIN ".$_w." DOLLARS ON BET ".$state->{"C"});
  # 2160  D=D-B(C)*2
  $state->{"D"} = $state->{"D"}-$_w;
  # 2170  P=P+B(C)*2
  $state->{"P"} = $state->{"P"}+$_w;
  my $out = { "anchor" => "goto2180" }; return $out;
}

sub goto2180 {
  # 2180  GOTO 2810
  my $out = { "anchor" => "goto2810" }; return $out;
}

sub goto2190 {
  # 2190  REM  13-24(38) 2
  #(2190) 1
  # 2200  IF S>12 AND S<25 THEN 2150
  if ( $state->{"S"} > 12 and $state->{"S"} < 25 ) {
    my $out = { "anchor" => "goto2150" }; return $out;
  }
  # 2210  GOTO 2110
  my $out = { "anchor" => "goto2110" }; return $out;
}

sub goto2220 {
  # 2220  REM  25-36(39) 2
  #(2220) 1
  # 2230  IF S>24 AND S<37 THEN 2150
  if ( $state->{"S"} > 24 and $state->{"S"} < 37 ) {
    my $out = { "anchor" => "goto2150" }; return $out;
  }
  # 2240  GOTO 2110
  my $out = { "anchor" => "goto2110" }; return $out;
}

sub goto2250 {
  # 2250  REM  FIRST COLUMN(40) 2
  #(2250) 1
  # 2260  FOR I=1 TO 34 STEP 3
  $state->{"I"} = -2;
  my $out = { "anchor" => "nextIa" }; return $out;
}

sub nextIa {
  $state->{"I"} = $state->{"I"}+3;
  if ($state->{"I"} > 34) {
    $state->{"I"} = 34;
    my $out = { "anchor" => "postnextIa" }; return $out;
  }
  # 2270  IF S=I THEN 2150
  if ( $state->{"S"} == $state->{"I"} ) {
    my $out = { "anchor" => "goto2150" }; return $out;
  }
  # 2280  NEXT I
  my $out = { "anchor" => "nextIa" }; return $out;
}

sub postnextIa {
  # 2290  GOTO 2110
  my $out = { "anchor" => "goto2110" }; return $out;
}

sub goto2300 {
  # 2300  REM  SECOND COLUMN(41) 2
  #(2300) 1
  # 2310  FOR I=2 TO 35 STEP 3
  $state->{"I"} = -1;
  my $out = { "anchor" => "nextIb" }; return $out;
}

sub nextIb {
  $state->{"I"} = $state->{"I"}+3;
  if ($state->{"I"} > 35) {
    $state->{"I"} = 35;
    my $out = { "anchor" => "postnextIb" }; return $out;
  }
  # 2320  IF S=I THEN 2150
  if ( $state->{"S"} == $state->{"I"} ) {
    my $out = { "anchor" => "goto2150" }; return $out;
  }
  # 2330  NEXT I
  my $out = { "anchor" => "nextIb" }; return $out;
}

sub postnextIb {
  # 2340  GOTO 2110
  my $out = { "anchor" => "goto2110" }; return $out;
}

sub goto2350 {
  # 2350  REM  THIRD COLUMN(42) 2
  #(2350) 1
  # 2360  FOR I=3 TO 36 STEP 3
  $state->{"I"} = 0;
  my $out = { "anchor" => "nextIc" }; return $out;
}

sub nextIc {
  $state->{"I"} = $state->{"I"}+3;
  if ($state->{"I"} > 36) {
    $state->{"I"} = 36;
    my $out = { "anchor" => "postnextIc" }; return $out;
  }
  # 2370  IF S=I THEN 2150
  if ( $state->{"S"} == $state->{"I"} ) {
    my $out = { "anchor" => "goto2150" }; return $out;
  }
  # 2380  NEXT I
  my $out = { "anchor" => "nextIc" }; return $out;
}

sub postnextIc {
  # 2390  GOTO 2110
  my $out = { "anchor" => "goto2110" }; return $out;
}

sub goto2400 {
  # 2400  REM  1-18(43) 1
  #(2400) 1
  # 2410  IF S<19 THEN 2430
  if ( $state->{"S"} < 19 ) {
    my $out = { "anchor" => "goto2430" }; return $out;
  }
  # 2420  GOTO 2110
  my $out = { "anchor" => "goto2110" }; return $out;
}

sub goto2430 {
  # 2430  PRINT "YOU WIN";B(C);"DOLLARS ON BET";C
  gameMessage( "YOU WIN ".$state->{"B"}[$state->{"C"}]." DOLLARS ON BET ".$state->{"C"});
  # 2440  D=D-B(C)
  $state->{"D"} = $state->{"D"}-$state->{"B"}[$state->{"C"}];
  # 2450  P=P+B(C)
  $state->{"P"} = $state->{"P"}+$state->{"B"}[$state->{"C"}];
  # 2460  GOTO 2810
  my $out = { "anchor" => "goto2810" }; return $out;
}

sub goto2470 {
  # 2470  REM  19-36(44) 1
  #(2470) 1
  # 2480  IF S<37 AND S>18 THEN 2430
  if ( $state->{"S"} < 37 and $state->{"S"} > 18 ) {
    my $out = { "anchor" => "goto2430" }; return $out;
  }
  # 2490  GOTO 2110
  my $out = { "anchor" => "goto2110" }; return $out;
}

sub goto2500 {
  # 2500  REM  EVEN(45) 1
  #(2500) 1
  # 2510  IF S/2=INT(S/2) AND S<37 THEN 2430
  if ( $state->{"S"}/2 == int($state->{"S"}/2) and $state->{"S"} < 37 ) {
    my $out = { "anchor" => "goto2430" }; return $out;
  }
  # 2520  GOTO 2110
  my $out = { "anchor" => "goto2110" }; return $out;
}

sub goto2530 {
  # 2530  REM  ODD(46) 1
  #(2530) 1
  # 2540  IF S/2<>INT(S/2) AND S<37 THEN 2430
  if ( $state->{"S"}/2 != int($state->{"S"}/2) and $state->{"S"} < 37 ) {
    my $out = { "anchor" => "goto2430" }; return $out;
  }
  # 2550  GOTO 2110
  my $out = { "anchor" => "goto2110" }; return $out;
}

sub goto2560 {
  # 2560  REM  RED(47) 1
  #(2560) 1
  # 2570  RESTORE 
  restore();
  # 2580  FOR I=1 TO 18
  $state->{"I"} = 0;
  my $out = { "anchor" => "nextId" }; return $out;
}

sub nextId {
  $state->{"I"} = $state->{"I"}+1;
  if ($state->{"I"} > 18) {
    $state->{"I"} = 18;
    my $out = { "anchor" => "postnextId" }; return $out;
  }
  # 2590  READ R
  $state->{"R"} = $state->{"DATA"}[$state->{"cur"}];
  $state->{"cur"} = $state->{"cur"}+1;
  # 2600  IF S=R THEN 2430
  if ( $state->{"S"} == $state->{"R"} ) {
    my $out = { "anchor" => "goto2430" }; return $out;
  }
  # 2610  NEXT I
  my $out = { "anchor" => "nextId" }; return $out;
}

sub postnextId {
  # 2620  GOTO 2110
  my $out = { "anchor" => "goto2110" }; return $out;
}

sub goto2630 {
  # 2630  REM  BLACK(48) 1
  #(2630) 1
  # 2640  RESTORE
  restore();
  # 2650  FOR I=1 TO 18
  $state->{"I"} = 0;
  my $out = { "anchor" => "nextIe" }; return $out;
}

sub nextIe {
  $state->{"I"} = $state->{"I"}+1;
  if ($state->{"I"} > 18) {
    $state->{"I"} = 18;
    my $out = { "anchor" => "postnextIe" }; return $out;
  }
  # 2660  READ R
  $state->{"R"} = $state->{"DATA"}[$state->{"cur"}];
  $state->{"cur"} = $state->{"cur"}+1;
  # 2670  IF S=R THEN 2110
  if ( $state->{"S"} == $state->{"R"} ) {
    my $out = { "anchor" => "goto2110" }; return $out;
  }
  # 2680  NEXT I
  my $out = { "anchor" => "nextIe" }; return $out;
}

sub postnextIe {
  # 2690  IF S>36 THEN 2110
  if ( $state->{"S"} > 36 ) {
    my $out = { "anchor" => "goto2110" }; return $out;
  }
  # 2700  GOTO 2430
  my $out = { "anchor" => "goto2430" }; return $out;
}

sub goto2710 {
  # 2710  REM--1TO36,0,00(1-36,49,50)35
  #(2710) 1
  # 2720  IF T(C)<49 THEN 2760
  if ( $state->{"T"}[$state->{"C"}] < 49 ) {
    my $out = { "anchor" => "goto2760" }; return $out;
  }
  # 2730  IF T(C)=49 AND S=37 THEN 2780
  if ( $state->{"T"}[$state->{"C"}] == 49 and $state->{"S"} == 37 ) {
    my $out = { "anchor" => "goto2780" }; return $out;
  }
  # 2740  IF T(C)=50 AND S=38 THEN 2780
  if ( $state->{"T"}[$state->{"C"}] == 50 and $state->{"S"} == 38 ) {
    my $out = { "anchor" => "goto2780" }; return $out;
  }
  # 2750  GOTO 2110
  my $out = { "anchor" => "goto2110" }; return $out;
}

sub goto2760 {
  # 2760  IF T(C)=S THEN 2780
  if ( $state->{"T"}[$state->{"C"}] == $state->{"S"} ) {
    my $out = { "anchor" => "goto2780" }; return $out;
  }
  # 2770  GOTO 2110
  my $out = { "anchor" => "goto2110" }; return $out;
}

sub goto2780 {
  # 2780  PRINT "YOU WIN";B(C)*35;"DOLLARS ON BET";C
  my $_w = $state->{"B"}[$state->{"C"}]*35;
  gameMessage( "YOU WIN ".$_w." DOLLARS ON BET ".$state->{"C"});
  # 2790  D=D-B(C)*35
  $state->{"D"} = $state->{"D"}-$_w;
  # 2800  P=P+B(C)*35
  $state->{"P"} = $state->{"P"}+$_w;
  my $out = { "anchor" => "goto2810" }; return $out;
}

sub goto2810 {
  # 2810  NEXT C
  my $out = { "anchor" => "nextCa" }; return $out;
}

##### HERE #####
sub postnextCa {
  # 2820  PRINT 
  blank();
  # 2830  PRINT "TOTALS
  gameMessage( "TOTALS");
  #(2830) ","ME","YOU"
  # 2840  PRINT " ",D,P
  gameMessage(
    sprintf('%-16s', "TOTALS").
    sprintf('%-16s', "ME").
    sprintf('%-16s', "YOU") );
  gameMessage(
    sprintf('%-16s', "").
    sprintf('%-16s', $state->{"D"}).
    sprintf('%-16s', $state->{"P"}) );
  # 2850  IF P>0 THEN 2880
  if ( $state->{"P"} > 0 ) { my $out = { "anchor" => "goto2880" }; return $out; }
  # 2860  PRINT "OOPS! YOU JUST SPENT YOUR LAST DOLLAR!"
  gameMessage( "OOPS! YOU JUST SPENT YOUR LAST DOLLAR!");
  # 2870  GOTO 3190
  my $out = { "anchor" => "goto3190" }; return $out;
}

sub goto2880 {
  # 2880  IF D>0 THEN 2920
  if ( $state->{"D"} > 0 ) { my $out = { "anchor" => "goto2920" }; return $out; }
  # 2890  PRINT "YOU BROKE THE HOUSE!"
  gameMessage( "YOU BROKE THE HOUSE!");
  # 2900  P=101000.
  $state->{"P"} = 101000.0;
  # 2910  GOTO 2960
  my $out = { "anchor" => "goto2960" }; return $out;
}

sub goto2920 {
  # 2920  PRINT "AGAIN";
  gameMessage( "AGAIN");
  # 2930  INPUT Y$
  my $out = {
    "anchor" => "goto2930postinput",
    "echo" => " > ",
    "hook" => "reply"
  };
  return $out;
}

sub goto2930postinput {
  $state->{"Ys"} = $input->{"action"}[0];
  # 2940  IF LEFT$(Y$,1)="Y" THEN 1630
  if ( $state->{"Ys"} =~ /^Y/ ) { my $out = { "anchor" => "goto1630" }; return $out; }
  # 2950  DATA 1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36
  my $out = { "anchor" => "goto2960" }; return $out;
}

sub goto2960 {
  # 2960  IF P<1 THEN 3190
  if ( $state->{"P"} < 1 ) { my $out = { "anchor" => "goto3190" }; return $out; }
  # 2970  PRINT "TO WHOM SHALL I MAKE THE CHECK";
  gameMessage( "TO WHOM SHALL I MAKE THE CHECK");
  # 2980  INPUT B$
  my $out = {
    "anchor" => "goto2980postinput",
    "echo" => " > ",
    "hook" => "reply"
  };
  return $out;
}

sub goto2980postinput {
  $state->{"Bs"} = join(" ", @{$input->{"action"}});
  # 2990  PRINT 
  blank();
  # 3000  FOR I=1 TO 72
  #(3000)  PRINT "-";
  #(3000)  NEXT I
  #(3000)  REM PRINT 72 DASHES
  gameMessage("------------------------------------------------------------------------");
  blank();
  # 3010  PRINT TAB(50);"CHECK NO. ";INT(RND(1)*100)
  gameMessageTab(50, "CHECK NO. ".int(rand()*100));
  # 3020  PRINT 
  blank();
  # 3030  GOSUB 3230
  gosub3230();
  # 3040  PRINT TAB(40);M$
  gameMessageTab(40, $state->{"Ms"});
  # 3050  PRINT 
  blank();
  # 3060  PRINT 
  blank();
  # 3070  PRINT "PAY TO THE ORDER OF-----";B$;"-----$ ";
  # 3080  PRINT P
  gameMessage( "PAY TO THE ORDER OF-----".$state->{"Bs"}."-----\$ ".$state->{"P"});
  # 3090  PRINT 
  blank();
  # 3100  PRINT 
  blank();
  # 3110  PRINT TAB(10),"THE MEMORY BANK OF NEW YORK"
  gameMessageTab(10, "THE MEMORY BANK OF NEW YORK");
  # 3120  PRINT 
  blank();
  # 3130  PRINT TAB(40),"THE COMPUTER"
  gameMessageTab(40, "THE COMPUTER");
  # 3140  PRINT TAB(40)"----------X-----"
  gameMessageTab(40, "----------X-----");
  # 3150  PRINT 
  blank();
  # 3160  FOR I=1 TO 62
  #(3160)  PRINT "-";
  gameMessage( "--------------------------------------------------------------");
  #(3160)  NEXT I
  # 3170  PRINT "COME BACK SOON!"
  gameMessage( "COME BACK SOON!");
  # 3180  GOTO 3210
  my $out = { "anchor" => "goto3210" }; return $out;
}

sub goto3190 {
  # 3190  PRINT "THANKS FOR YOUR MONEY."
  gameMessage( "THANKS FOR YOUR MONEY.");
  # 3200  PRINT "I'LL USE IT TO BUY A SOLID GOLD ROULETTE WHEEL"
  gameMessage( "I'LL USE IT TO BUY A SOLID GOLD ROULETTE WHEEL");
  my $out = { "anchor" => "goto3210" }; return $out;
}

sub goto3210 {
  # 3210  PRINT 
  blank();
  # 3220  GOTO 3420
  my $out = { "anchor" => "goto3420" }; return $out;
}

sub gosub3230 {
  # 3230  REM
  # 3240  REM     THIS ROUTINE RETURNS THE CURRENT DATE IN M$
  # 3250  REM     IF YOU HAVE SYSTEM FUNCTIONS TO HANDLE THIS
  # 3260  REM     THEY CAN BE USED HERE.  HOWEVER IN THIS
  # 3270  REM     PROGRAM, WE JUST INPUT THE DATE AT THE START
  # 3280  REM     THE GAME
  # 3290  REM
  # 3300  REM     THE DATE IS RETURNED IN VARIABLE M$
  # 3310  M$=D$+", "+E$
  $state->{"Ms"} = $state->{"Ds"}.", ".$state->{"Es"};
  #$state->{"Ms"} = localtime();
  # 3320  RETURN
  return;
}

sub goto3420 {
  # 3420  END
  my $out = { "anchor" => "quit_game" }; return $out;
}

1;

#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

sub initialize {
  my $out = { "anchor" => "goto10" }; return $out;
}

sub goto10 {
  gameMessageTab(33, "BOMBARDMENT");
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  blank(3);
  gameMessage( "YOU ARE ON A BATTLEFIELD WITH 4 PLATOONS AND YOU");
  gameMessage( "HAVE 25 OUTPOSTS AVAILABLE WHERE THEY MAY BE PLACED.");
  gameMessage( "YOU CAN ONLY PLACE ONE PLATOON AT ANY ONE OUTPOST.");
  gameMessage( "THE COMPUTER DOES THE SAME WITH ITS FOUR PLATOONS.");
  blank();
  gameMessage( "THE OBJECT OF THE GAME IS TO FIRE MISSILES AT THE");
  gameMessage( "OUTPOSTS OF THE COMPUTER.  IT WILL DO THE SAME TO YOU.");
  gameMessage( "THE ONE WHO DESTROYS ALL FOUR OF THE ENEMY'S PLATOONS");
  gameMessage( "FIRST IS THE WINNER.");
  blank();
  gameMessage( "GOOD LUCK... AND TELL US WHERE YOU WANT THE BODIES SENT!");
  blank();
  gameMessage( "TEAR OFF MATRIX AND USE IT TO CHECK OFF THE NUMBERS.");
  blank(5);
  # 260  DIM M(100)
  my @a = ();
  $state->{"Ma"} = dclone(\@a);
  # 270  FOR R=1 TO 5
  # 280  I=(R-1)*5+1
  # 290  PRINT I,I+1,I+2,I+3,I+4
  # 300  NEXT R
  for my $r (1 .. 5) {
    my $i = ($r-1)*5+1;
    my $j = $i+1;
    my $k = $i+2;
    my $l = $i+3;
    my $m = $i+4;
    my $ii = sprintf('%2s', $i);
    my $ij = sprintf('%2s', $j);
    my $ik = sprintf('%2s', $k);
    my $il = sprintf('%2s', $l);
    my $im = sprintf('%2s', $m);
    gameMessage($ii." ".$ij." ".$ik." ".$il." ".$im);
  }
  blank(10);
  # 380  C=INT(RND(1)*25)+1
  $state->{"C"} = int(rand()*25)+1;
  my $out = { "anchor" => "goto390" }; return $out;
}

sub goto390 {
  # 390  D=INT(RND(1)*25)+1
  $state->{"D"} = int(rand()*25)+1;
  my $out = { "anchor" => "goto400" }; return $out;
}

sub goto400 {
  # 400  E=INT(RND(1)*25)+1
  $state->{"E"} = int(rand()*25)+1;
  my $out = { "anchor" => "goto410" }; return $out;
}

sub goto410 {
  # 410  F=INT(RND(1)*25)+1
  $state->{"F"} = int(rand()*25)+1;
  # 420  IF C=D THEN 390
  if ( $state->{"C"} == $state->{"D"} ) { my $out = { "anchor" => "goto390" }; return $out; }
  # 430  IF C=E THEN 400
  if ( $state->{"C"} == $state->{"E"} ) { my $out = { "anchor" => "goto400" }; return $out; }
  # 440  IF C=F THEN 410
  if ( $state->{"C"} == $state->{"F"} ) { my $out = { "anchor" => "goto410" }; return $out; }
  # 450  IF D=E THEN 400
  if ( $state->{"D"} == $state->{"E"} ) { my $out = { "anchor" => "goto400" }; return $out; }
  # 460  IF D=F THEN 410
  if ( $state->{"D"} == $state->{"F"} ) { my $out = { "anchor" => "goto410" }; return $out; }
  # 470  IF E=F THEN 410
  if ( $state->{"E"} == $state->{"F"} ) { my $out = { "anchor" => "goto410" }; return $out; }
  gameMessage( "WHAT ARE YOUR FOUR POSITIONS");
  # 490  INPUT G,H,K,L
  my @argv = ("integer", "integer", "integer", "integer");
  my $out = {
    "anchor" => "goto490postinput",
    "echo" => " > ",
    "argc" => 4,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto490postinput {
  $state->{"G"} = $input->{"action"}[0];
  $state->{"H"} = $input->{"action"}[1];
  $state->{"K"} = $input->{"action"}[2];
  $state->{"L"} = $input->{"action"}[3];
  blank();
  my $out = { "anchor" => "goto500" }; return $out;
}

sub goto500 {
  gameMessage( "WHERE DO YOU WISH TO FIRE YOUR MISSILE");
  # 510  INPUT Y
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto510postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto510postinput {
  $state->{"Y"} = $input->{"action"}[0];
  # 520  IF Y=C THEN 710
  if ( $state->{"Y"} == $state->{"C"} ) { my $out = { "anchor" => "goto710" }; return $out; }
  # 530  IF Y=D THEN 710
  if ( $state->{"Y"} == $state->{"D"} ) { my $out = { "anchor" => "goto710" }; return $out; }
  # 540  IF Y=E THEN 710
  if ( $state->{"Y"} == $state->{"E"} ) { my $out = { "anchor" => "goto710" }; return $out; }
  # 550  IF Y=F THEN 710
  if ( $state->{"Y"} == $state->{"F"} ) { my $out = { "anchor" => "goto710" }; return $out; }
  my $out = { "anchor" => "goto630" }; return $out;
}

sub goto570 {
  # 570  M=INT(RND(1)*25)+1
  $state->{"M"} = int(rand()*25)+1;
  my $out = { "anchor" => "goto1160" }; return $out;
}

sub goto580 {
  # 580  IF X=G THEN 920
  if ( $state->{"X"} == $state->{"G"} ) { my $out = { "anchor" => "goto920" }; return $out; }
  # 590  IF X=H THEN 920
  if ( $state->{"X"} == $state->{"H"} ) { my $out = { "anchor" => "goto920" }; return $out; }
  # 600  IF X=L THEN 920
  if ( $state->{"X"} == $state->{"L"} ) { my $out = { "anchor" => "goto920" }; return $out; }
  # 610  IF X=K THEN 920
  if ( $state->{"X"} == $state->{"K"} ) { my $out = { "anchor" => "goto920" }; return $out; }
  my $out = { "anchor" => "goto670" }; return $out;
}

sub goto630 {
  gameMessage( "HA, HA YOU MISSED. MY TURN NOW:");
  blank(2);
  my $out = { "anchor" => "goto570" }; return $out;
}

sub goto670 {
  gameMessage( "I MISSED YOU, YOU DIRTY RAT. I PICKED ".$state->{"M"}.". YOUR TURN:");
  blank(2);
  #(680)  GOTO 500
  my $out = { "anchor" => "goto500" }; return $out;
}

sub goto710 {
  # 710  Q=Q+1
  $state->{"Q"} = $state->{"Q"}+1;
  # 720  IF Q=4 THEN 890
  if ( $state->{"Q"} == 4 ) { my $out = { "anchor" => "goto890" }; return $out; }
  # 730  PRINT "YOU GOT ONE OF MY OUTPOSTS!"
  gameMessage( "YOU GOT ONE OF MY OUTPOSTS!");
  # 740  IF Q=1 THEN 770
  if ( $state->{"Q"} == 1 ) { my $out = { "anchor" => "goto770" }; return $out; }
  # 750  IF Q=2 THEN 810
  if ( $state->{"Q"} == 2 ) { my $out = { "anchor" => "goto810" }; return $out; }
  # 760  IF Q=3 THEN 850
  if ( $state->{"Q"} == 3 ) { my $out = { "anchor" => "goto850" }; return $out; }
}

sub goto770 {
  gameMessage( "ONE DOWN, THREE TO GO.");
  blank(2);
  my $out = { "anchor" => "goto570" }; return $out;
}

sub goto810 {
  gameMessage( "TWO DOWN, TWO TO GO.");
  blank();
  blank();
  my $out = { "anchor" => "goto570" }; return $out;
}

sub goto850 {
  gameMessage( "THREE DOWN, ONE TO GO.");
  blank();
  blank();
  my $out = { "anchor" => "goto570" }; return $out;
}

sub goto890 {
  gameMessage( "YOU GOT ME, I'M GOING FAST. BUT I'LL GET YOU WHEN");
  gameMessage( "MY TRANSISTO&S RECUP%RA*E!");
  my $out = { "anchor" => "goto1235" }; return $out;
}

sub goto920 {
  # 920  Z=Z+1
  # 930  IF Z=4 THEN 1110
  if ( $state->{"Z"} == 4 ) { my $out = { "anchor" => "goto1110" }; return $out; }
  gameMessage( "I GOT YOU. IT WON'T BE LONG NOW. POST ".$state->{"X"}." WAS HIT.");
  # 950  IF Z=1 THEN 990
  if ( $state->{"Z"} == 1 ) { my $out = { "anchor" => "goto990" }; return $out; }
  # 960  IF Z=2 THEN 1030
  if ( $state->{"Z"} == 2 ) { my $out = { "anchor" => "goto1030" }; return $out; }
  # 970  IF Z=3 THEN 1070
  if ( $state->{"Z"} == 3 ) { my $out = { "anchor" => "goto1070" }; return $out; }
}

sub goto990 {
  gameMessage( "YOU HAVE ONLY THREE OUTPOSTS LEFT.");
  blank();
  blank();
  my $out = { "anchor" => "goto500" }; return $out;
}

sub goto1030 {
  gameMessage( "YOU HAVE ONLY TWO OUTPOSTS LEFT.");
  blank();
  blank();
  my $out = { "anchor" => "goto500" }; return $out;
}

sub goto1070 {
  gameMessage( "YOU HAVE ONLY ONE OUTPOST LEFT.");
  blank();
  blank();
  my $out = { "anchor" => "goto500" }; return $out;
}

sub goto1110 {
  gameMessage( "YOU'RE DEAD. YOUR LAST OUTPOST WAS AT ".$state->{"X"}.". HA, HA, HA.");
  gameMessage( "BETTER LUCK NEXT TIME.");
  my $out = { "anchor" => "goto1235" }; return $out;
}

sub goto1160 {
  # 1160  P=P+1
  $state->{"P"} = $state->{"P"}+1;
  # 1170  N=P-1
  $state->{"N"} = $state->{"P"}-1;
  # 1180  FOR T=1 TO N
  for my $t (1 .. $state->{"N"}) {
    # 1190  IF M=M(T) THEN 570
    if ( $state->{"M"} == $state->{"Ma"}[$t] ) { my $out = { "anchor" => "goto570" }; return $out; }
  }
  # 1200  NEXT T
  # 1210  X=M
  $state->{"X"} = $state->{"M"};
  # 1220  M(P)=M
  $state->{"Ma"}[$p] = $state->{"M"};
  my $out = { "anchor" => "goto580" }; return $out;
}

sub goto1235 {
  # 1235  END
  my $out = { "anchor" => "quit_game" }; return $out;
}

1;

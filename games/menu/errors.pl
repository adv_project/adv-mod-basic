#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

sub no_next_page {
  message($BOLD.$MAGENTA."  Already in last page.");
  my $o = {
    "anchor" => "process_user_entry",
    "hook" => "reply"
  };
  return $o;
}

sub no_previous_page {
  message($BOLD.$MAGENTA."  Already in first page.");
  my $o = {
    "anchor" => "process_user_entry",
    "hook" => "reply"
  };
  return $o;
}

sub game_not_implemented {
  message($BOLD.$MAGENTA."  Game not implemented yet.");
  my $o = {
    "anchor" => "process_user_entry",
    "hook" => "reply"
  };
  return $o;
}

sub unknown_command {
  message($BOLD.$MAGENTA."  Unknown command or nonexistant game.");
  my $o = {
    "anchor" => "process_user_entry",
    "hook" => "reply"
  };
  return $o;
}

1;

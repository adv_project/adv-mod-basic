#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

require $moddir . "/games/menu/banners.pl";

sub no_banner_warning() {
  message("Wrong banner");
}

sub print_banner {
  my $banner = $userconfig->{"banner_theme"};
  my $retval = eval "print_banner_".$banner;
#  print_banner_metal() unless ($retval == 1);
#  no_banner_warning() unless ($retval == 1);
}

sub initialize_menu {
  my $gamesdir = $moddir . "/games/";
  my $menuhash;
  local $/;
  opendir my $gdh, $gamesdir;
  my @dirs = readdir $gdh;
  closedir $gdh;
  my @sorted_dirs = sort(@dirs);
  my $c = 0;
  for my $d (0 .. (@sorted_dirs - 1)) {
    next if ($sorted_dirs[$d] =~ /^\.\/$/);
    next if ($sorted_dirs[$d] =~ /^\.\.\/$/);
    my $mfile = $moddir . "/games/" . $sorted_dirs[$d] . "/manifest.json";
    local $/;
    next unless (open my $fh, '<', $mfile);
    my $json = <$fh>;
    close $fh;
    my $m = JSON::decode_json($json);
    my $id = $m->{"id"};
    my $name = $m->{"name"};
    my @description = @{$m->{"description"}};
    my @categories = @{$m->{"categories"}};
    my @tags = @{$m->{"tags"}};
    my @greptags = @{$m->{"tags"}};
    next if ( grep( /^skip_menu$/, @greptags ) );
    $c = $c+1;
    $menuhash->{$c}->{"id"} = $id;
    $menuhash->{$c}->{"name"} = $name;
    $menuhash->{$c}->{"description"} = dclone(\@description);
    $menuhash->{$c}->{"categories"} = dclone(\@categories);
    $menuhash->{$c}->{"tags"} = dclone(\@tags);
  }
  my $json = JSON->new->pretty(1)->encode($menuhash);
  my $menufile = $wdir . "/menu.json";
  open my $fh, '>', $menufile;
  print $fh $json;
  close $fh;
}

sub show_menu_items {
  my $s = shift; my $i = shift;
  my $string;

  my $istr = $i."";
  if ( exists($s->{$istr}) ) {
    my $index = sprintf "%2s", $istr;
    my $name = sprintf '%-18s', $s->{$istr}->{"name"};
    my $label;
    my $f = $moddir."/games/".$s->{$istr}->{"id"}."/manifest.json";
    local $/;
    open my $fh, '<', $f;
    my $json = <$fh>;
    close $fh;
    my $m = JSON::decode_json($json);
    if ( grep( /^not_implemented$/, @{ $m->{'tags'} } ) ) {
      $label = $RESET.$usercolors->{"menu_color_not_implemented"}.$name;
    } else {
      $label = $RESET.$usercolors->{"menu_color_title"}.$name;
    }
    $string = "    ".$RESET.$usercolors->{"menu_color_bracket"}."[".$RESET.$usercolors->{"menu_color_item"}.$index.$RESET.$usercolors->{"menu_color_bracket"}."] ".$label;
  } else {
    return;
  }
  my $iplus = $i+5;
  $istr = $iplus."";
  if ( exists($s->{$istr}) ) {
    my $index = sprintf "%2s", $istr;
    my $name = sprintf '%-18s', $s->{$istr}->{"name"};
    my $label;
    my $f = $moddir."/games/".$s->{$istr}->{"id"}."/manifest.json";
    local $/;
    open my $fh, '<', $f;
    my $json = <$fh>;
    close $fh;
    my $m = JSON::decode_json($json);
    if ( grep( /^not_implemented$/, @{ $m->{'tags'} } ) ) {
      $label = $RESET.$usercolors->{"menu_color_not_implemented"}.$name;
    } else {
      $label = $RESET.$usercolors->{"menu_color_title"}.$name;
    }
    $string = $string." ".$RESET.$usercolors->{"menu_color_bracket"}."[".$RESET.$usercolors->{"menu_color_item"}.$index.$RESET.$usercolors->{"menu_color_bracket"}."] ".$label;
  }
  message($string);
}

sub show_next_page {
  my $player = $input->{'player'};
  my $current_page = $state->{$player}->{'page'};
  my $next_page = $current_page + 1;
  local $/;
  open my $fh, '<', $wdir."/menu.json";
  my $json = <$fh>;
  close $fh;
  my $menu = JSON::decode_json($json);
  my $i = 1 + ($next_page - 1) * 10;
  if ( exists( $menu->{$i.""} ) ) {
    $state->{$player}->{'page'} = $next_page;
    my $o = { "anchor" => "show_menu" };
    return $o;
  } else {
    my $o = { "anchor" => "no_next_page" };
    return $o;
  }
}

sub show_previous_page {
  my $player = $input->{'player'};
  my $current_page = $state->{$player}->{'page'};
  my $previous_page = $current_page - 1;
  local $/;
  open my $fh, '<', $wdir."/menu.json";
  my $json = <$fh>;
  close $fh;
  my $menu = JSON::decode_json($json);
  my $i = 1 + ($previous_page - 1) * 10;
  if ( exists( $menu->{$i.""} ) ) {
    $state->{$player}->{'page'} = $previous_page;
    my $o = { "anchor" => "show_menu" };
    return $o;
  } else {
    my $o = { "anchor" => "no_previous_page" };
    return $o;
  }
}

sub print_menu {
  my $f = $wdir . "/menu.json";
  my $player = $input->{'player'};
  my $page = $state->{$player}->{'page'};
  local $/;
  open my $fh, '<', $f;
  my $json = <$fh>;
  close $fh;
  my $m = JSON::decode_json($json);
  my $start = 1 + ($page - 1) * 10;
  my $end = $start + 4;

  blank();
  for my $i ($start .. $end) {
    show_menu_items($m ,$i);
    #show_menu_items($s[$i], $m->{$s[$i]});
  }
  blank();
}

1;

#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

require $moddir . "/games/menu/menu.pl";
require $moddir . "/games/menu/help.pl";
require $moddir . "/games/menu/games.pl";
require $moddir . "/games/menu/errors.pl";

sub initialize {
  initialize_menu();
  my $o = { "anchor" => "show_banner" };
  return $o;
}

sub show_banner {
  print_banner();
  my $o = { "anchor" => "show_short_help" };
  return $o;
}

sub show_short_help {
  print_short_help();
  my $o = { "anchor" => "show_menu", };
  return $o;
}

sub show_instructions {
  print_instructions();
  my $o = {
    "anchor" => "process_user_entry",
    "hook" => "reply"
  };
  return $o;
}

sub show_menu {
  print_menu();
  my $o = {
    "anchor" => "process_user_entry",
    "hook" => "reply"
  };
  return $o;
}

sub back_to_work {
  message($BOLD.$GREEN."Back to work.");
  my $o = {
    "anchor" => "process_user_entry",
    "hook" => "reply"
  };
  return $o;
}

sub new_game {
  message($BOLD.$MAGENTA."  Sorry, this feature isn't implemented yet :(");
  my $o = {
    "anchor" => "process_user_entry",
    "hook" => "reply"
  };
  return $o;
}

sub process_user_entry {
  my $entry = $input->{"action"}[0];

  if ($entry eq "") {
    # Player has just reentered the room
    my $o = { "anchor" => "show_banner" };
    return $o;
  }
  if (($entry eq "h") or ($entry eq "help")) {
    my $o = { "anchor" => "show_short_help" };
    return $o;
  }
  if (($entry eq "i") or ($entry eq "instructions")) {
    my $o = { "anchor" => "show_instructions" };
    return $o;
  }
  if (($entry eq "n") or ($entry eq "next")) {
    my $o = { "anchor" => "show_next_page" };
    return $o;
  }
  if (($entry eq "p") or ($entry eq "previous")) {
    my $o = { "anchor" => "show_previous_page" };
    return $o;
  }
  if ($entry eq "reload") {
    my $o = { "anchor" => "initialize" };
    return $o;
  }
  local $/;
  open my $fh, '<', $wdir."/menu.json";
  my $json = <$fh>;
  close $fh;
  my $menu = JSON::decode_json($json);
  if ( exists( $menu->{$entry} ) ) {
    if ( grep(
      /^not_implemented$/,
      @{ $menu->{$entry}->{'tags'} }
    ) ) {
      my $o = { "anchor" => "game_not_implemented" };
      return $o;
    } else {
      my $o = {
        "anchor" => "back_to_work",
        "hook" => "create_game",
        "game_id" => $menu->{$entry}->{"id"},
        "game_name" => $menu->{$entry}->{"name"}
      };
      return $o;
    }
  }
  my $o = { "anchor" => "unknown_command", };
  return $o;
}

1;

#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

sub print_short_help {
  blank();
  message($usercolors->{"help_color"}."  Use ".$BOLD."n|next".$NOTBOLD." and ".$BOLD."p|previous".$NOTBOLD." to navigate the list, ".$BOLD."h|help".$NOTBOLD." to show this");
  message($usercolors->{"help_color"}."  help text,".$BOLD."i|instructions".$NOTBOLD." for more comprehensive help, or enter any");
  message($usercolors->{"help_color"}."  valid number to create and play a game.");
  message($usercolors->{"help_color"}."  Use ".$BOLD."QUIT".$NOTBOLD." at any point to exit the game and go back to the menu." );
  message($usercolors->{"help_color"}."  Games not yet implemented are shown ".$usercolors->{"menu_color_not_implemented"}."with a different color".$RESET.$usercolors->{"help_color"}.".");
}

sub print_instructions {
  for my $s (
    "",
    $BOLD."Game creation",
    "",
    "  When you select a game from the menu,  a  subgame  is  created  and",
    "  launched immediately. Some games offer the possibility  of  replay,",
    "  while others don't. and exit once you either win or lose.  There is",
    "  yet another case: games which play indefinitely, and never exit for",
    "  themselves. In any case, you can type '".$BOLD."QUIT".$NOTBOLD."' and the game will exit",
    "  immediately.",
    "",
    "  Use the command ".$BOLD."menu".$NOTBOLD." to see the games menu again.",
    "  When a game exits, no score is saved (for now), and there is no way",
    "  of saving the game, unless you happen to  have  a  datasette  or  a",
    "  diskette device mounted remotely on the server. (Just kidding.)",
    "  The command ".$BOLD."reload".$NOTBOLD." rebuilds the menu; use it if new games were uploaded",
    "  so you don't need to create a new game in the server.",
    "  Be advised that many commands entered during the  games  only  work",
    "  when typed in UPPER CASE, exactly like the original games. In fact,",
    "  the games are implemented as faithfully as possible to the original",
    "  code, mainly for nostalgia, but also because these gems seem to be,",
    "  in a sense, perfect just as they are. So, here they go: enjoy, have",
    "  fun, and drop us a line in the chat! :)",
    "",
    $BOLD."Configuration",
    "",
    "  The font colors, both for the menu and for the games, are configurable",
    "  from the menu. The  command ".$BOLD."config".$NOTBOLD." shows the current configuration.",
    "  The configurable parameters and the corresponding commands are as",
    "  follows:",
    "    ".$BOLD."set help".$NOTBOLD." sets the color for the text help;",
    "    ".$BOLD."set bracket".$NOTBOLD." sets the color for the brackets drawn around the menu",
    "                items;",
    "    ".$BOLD."set item".$NOTBOLD." sets the color for the menu items;",
    "    ".$BOLD."set title".$NOTBOLD." sets the color for the menu game titles;",
    "    ".$BOLD."set ni".$NOTBOLD." sets the color for not implemented games in the menu;",
    "    ".$BOLD."set game".$NOTBOLD." sets the font color during the game (".$BOLD."yes".$NOTBOLD.", you can play the",
    "             games with a green font)  ;-)",
    "    ".$BOLD."set banner".$NOTBOLD." sets the banner theme. Currently there is only one theme",
    "               available ('metal'); additional themes are on the way.",
    "    ".$BOLD."set defaults".$NOTBOLD." restores the default colors.",
    "",
    "    Colors must be given as a  comma-separated  list  of  valid  color",
    "    identifiers, which are: red, green,  blue,  cyan,  magenta,  white,",
    "    plus the additional modifiers: bold, italic,  underline,  positive,",
    "    and the opposite ones: notbold, notitalic, notunderline,  negative.",
    "",
    "    For example, try '".$BOLD."set game bold,green".$NOTBOLD."' and see what happens.",
    ""
  )
  { message($usercolors->{"help_color"}."  ".$s); }
}

1;

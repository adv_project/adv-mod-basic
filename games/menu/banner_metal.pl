#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

sub print_banner_metal {
  blank(2);
  message("    ".$WHITE."_      ".$WHITE."_");
  message("   ".$WHITE."/_\\  ".$WHITE."_".$CYAN."_| ".$CYAN."|_ ".$CYAN."__");
  message("  ".$CYAN."/ ".$CYAN."_ ".$CYAN."\\/ ".$CYAN."_` ".$CYAN."\\ ".$CYAN."V ".$CYAN."/");
  message(" ".$CYAN."/_/ ".$CYAN."\\_\\_".$MAGENTA."_,_|\\_/     ".$BLUE."_____ _____ ".$BLUE."____".$WHITE."_");
  message("     ".$MAGENTA."|  ".$MAGENTA."_ ".$MAGENTA."\\   ".$BLUE."/\\    ".$BLUE."/ ".$BLUE."____|_   ".$WHITE."_/ ".$WHITE."____|");
  message("     ".$BLUE."| ".$BLUE."|_) ".$BLUE."| ".$BLUE."/  ".$BLUE."\\  ".$BLUE."| ".$WHITE."(___   ".$WHITE."| ".$WHITE."|| ".$WHITE."|");
  message("     ".$BLUE."|  ".$BLUE."_ ".$BLUE."< ".$BLUE."/ ".$WHITE."/\\ ".$WHITE."\\  ".$WHITE."\\___ ".$WHITE."\\  ".$WHITE."| ".$CYAN."|| ".$CYAN."|");
  message("     ".$WHITE."| ".$WHITE."|_) ".$WHITE."/ ".$WHITE."____ ".$WHITE."\\ ".$WHITE."_".$CYAN."___) ".$CYAN."|_| ".$CYAN."|| ".$CYAN."|___".$MAGENTA."_");
  message("     ".$WHITE."|____/_/    ".$CYAN."\\_|_____/|__".$MAGENTA."___\\_____|");
  message("                    ".$CYAN."/ ".$CYAN."__|__ ".$BLUE."_ ".$BLUE."_ ".$BLUE."__  ".$BLUE."___ ".$BLUE."___");
  message("                   ".$BLUE."| ".$BLUE."(_ ".$BLUE."/ ".$BLUE."_` ".$BLUE."| ".$BLUE."'  ".$BLUE."\\".$WHITE."/ ".$WHITE."-_(_-<");
  message("                    ".$BLUE."\\___\\__".$WHITE.",_|_|_|_\\___/__/");
  blank();
  message("  ".$CYAN."Welcome ".$BLUE."to the good ol".$WHITE."d days of vid".$MAGENTA."eo gam".$CYAN."es!");
}

1;

#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

  #(<<<GOTO>> ) 25, 10, ONA30,110,200,220, 45, 30, ONB50,80,90, 280, 125, 110, ONG130,150,170,190, 358, 325, 380, 235, 220, ONM250,260,270, 300, 285, 310, 320, 330, 390, 350, 340, 360, 0, 8
  #(<<<GOSUB>> ) 

sub initialize {
  my $out = { "anchor" => "goto8" }; return $out;
}

sub goto8 {
  # 8  PRINT "YOU ARE A PILOT IN A WORLD WAR II BOMBER."
  gameMessage( "YOU ARE A PILOT IN A WORLD WAR II BOMBER.");
  my $out = { "anchor" => "goto10" }; return $out;
}

sub goto10 {
  # 10  INPUT "WHAT SIDE -- ITALY(1), ALLIES(2), JAPAN(3), GERMANY(4)";A
  gameMessage("WHAT SIDE -- ITALY(1), ALLIES(2), JAPAN(3), GERMANY(4)");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto10postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto10postinput {
  $state->{"A"} = $input->{"action"}[0];
  # 20  IF A>0 AND A<5 THEN 25
  if ( $state->{"A"} > 0 and $state->{"A"} < 5 ) { my $out = { "anchor" => "goto25" }; return $out; }
  # 22  PRINT "TRY AGAIN..." 
  gameMessage( "TRY AGAIN...");
  #(22)  GOTO 10
  my $out = { "anchor" => "goto10" }; return $out;
}

sub goto25 {
  # 25  ON A GOTO 30, 110, 200, 220
  if ( $state->{"A"} == 1 ) { my $out = { "anchor" => "goto30" }; return $out; }
  if ( $state->{"A"} == 2 ) { my $out = { "anchor" => "goto110" }; return $out; }
  if ( $state->{"A"} == 3 ) { my $out = { "anchor" => "goto200" }; return $out; }
  if ( $state->{"A"} == 4 ) { my $out = { "anchor" => "goto220" }; return $out; }
  my $out = { "anchor" => "goto30" }; return $out;
}

sub goto30 {
  # 30  INPUT "YOUR TARGET -- ALBANIA(1), GREECE(2), NORTH AFRICA(3)";B
  gameMessage("YOUR TARGET -- ALBANIA(1), GREECE(2), NORTH AFRICA(3)");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto30postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto30postinput {
  $state->{"B"} = $input->{"action"}[0];
  # 40  IF B>0 AND B<4 THEN 45
  if ( $state->{"B"} > 0 and $state->{"B"} < 4 ) { my $out = { "anchor" => "goto45" }; return $out; }
  # 42  PRINT "TRY AGAIN..." 
  gameMessage( "TRY AGAIN...");
  #(42)  GOTO 30
  my $out = { "anchor" => "goto30" }; return $out;
}

sub goto45 {
  # 45  PRINT 
  blank();
  #(45)  ON B GOTO 50, 80,90
#  if ( $state->{"B"} == 1 ) { my $out = { "anchor" => "goto50" }; return $out; }
  if ( $state->{"B"} == 2 ) { my $out = { "anchor" => "goto80" }; return $out; }
  if ( $state->{"B"} == 3 ) { my $out = { "anchor" => "goto90" }; return $out; }
  # 50  PRINT "SHOULD BE EASY -- YOU'RE FLYING A NAZI-MADE PLANE."
  gameMessage( "SHOULD BE EASY -- YOU'RE FLYING A NAZI-MADE PLANE.");
  # 60  GOTO 280
  my $out = { "anchor" => "goto280" }; return $out;
}

sub goto80 {
  # 80  PRINT "BE CAREFUL!!!" 
  gameMessage( "BE CAREFUL!!!");
  #(80)  GOTO 280
  my $out = { "anchor" => "goto280" }; return $out;
}

sub goto90 {
  # 90  PRINT "YOU'RE GOING FOR THE OIL, EH?" 
  gameMessage( "YOU'RE GOING FOR THE OIL, EH?");
  #(90)  GOTO 280
  my $out = { "anchor" => "goto280" }; return $out;
}

sub goto110 {
  # 110  INPUT "AIRCRAFT -- LIBERATOR(1), B-29(2), B-17(3), LANCASTER(4)";G
  gameMessage("AIRCRAFT -- LIBERATOR(1), B-29(2), B-17(3), LANCASTER(4)");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto110postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto110postinput {
  $state->{"G"} = $input->{"action"}[0];
  # 120  IF G>0 AND G<5 THEN 125
  if ( $state->{"G"} > 0 and $state->{"G"} < 5 ) { my $out = { "anchor" => "goto125" }; return $out; }
  # 122  PRINT "TRY AGAIN..." 
  gameMessage( "TRY AGAIN...");
  #(122)  GOTO 110
  my $out = { "anchor" => "goto110" }; return $out;
}

sub goto125 {
  # 125  PRINT 
  blank();
  #(125)  ON G GOTO 130, 150, 170, 190
#  if ( $state->{"G"} == 1 ) { my $out = { "anchor" => "goto130" }; return $out; }
  if ( $state->{"G"} == 2 ) { my $out = { "anchor" => "goto150" }; return $out; }
  if ( $state->{"G"} == 3 ) { my $out = { "anchor" => "goto170" }; return $out; }
  if ( $state->{"G"} == 4 ) { my $out = { "anchor" => "goto190" }; return $out; }
  # 130  PRINT "YOU'VE GOT 2 TONS OF BOMBS FLYING FOR PLOESTI." 
  gameMessage( "YOU'VE GOT 2 TONS OF BOMBS FLYING FOR PLOESTI.");
  #(130)  GOTO 280
  my $out = { "anchor" => "goto280" }; return $out;
}

sub goto150 {
  # 150  PRINT "YOU'RE DUMPING THE A-BOMB ON HIROSHIMA." 
  gameMessage( "YOU'RE DUMPING THE A-BOMB ON HIROSHIMA.");
  #(150)  GOTO 280
  my $out = { "anchor" => "goto280" }; return $out;
}

sub goto170 {
  # 170  PRINT "YOU'RE CHASING THE BISMARK IN THE NORTH SEA." 
  gameMessage( "YOU'RE CHASING THE BISMARK IN THE NORTH SEA.");
  #(170)  GOTO 280
  my $out = { "anchor" => "goto280" }; return $out;
}

sub goto190 {
  # 190  PRINT "YOU'RE BUSTING A GERMAN HEAVY WATER PLANT IN THE RUHR."
  gameMessage( "YOU'RE BUSTING A GERMAN HEAVY WATER PLANT IN THE RUHR.");
  # 195  GOTO 280
  my $out = { "anchor" => "goto280" }; return $out;
}

sub goto200 {
  # 200  PRINT "YOU'RE FLYING A KAMIKAZE MISSION OVER THE USS LEXINGTON."
  gameMessage( "YOU'RE FLYING A KAMIKAZE MISSION OVER THE USS LEXINGTON.");
  # 205  INPUT "YOUR FIRST KAMIKAZE MISSION(Y OR N)";F$
  gameMessage("YOUR FIRST KAMIKAZE MISSION(Y OR N)");
  my $out = {
    "anchor" => "goto205postinput",
    "echo" => " > ",
    "hook" => "reply"
  };
  return $out;
}

sub goto205postinput {
  $state->{"Fs"} = $input->{"action"}[0];
  # 207  IF F$="N" THEN S=0 
  #(207)  GOTO 358
  if ( $state->{"Fs"} =~ /^N$/ ) {
    $state->{"S"} = 0;
    my $out = { "anchor" => "goto358 " };
    return $out;
  }
  # 210  PRINT 
  blank();
  #(210)  IF RND(1)>.65 THEN 325
  if ( rand() > .65 ) { my $out = { "anchor" => "goto325" }; return $out; }
  # 215  GOTO 380
  my $out = { "anchor" => "goto380" }; return $out;
}

sub goto220 {
  # 220  PRINT "A NAZI, EH?  OH WELL.  ARE YOU GOING FOR RUSSIA(1),"
  gameMessage( "A NAZI, EH?  OH WELL.  ARE YOU GOING FOR RUSSIA(1),");
  # 230  INPUT "ENGLAND(2), OR FRANCE(3)";M 
  gameMessage("ENGLAND(2), OR FRANCE(3)");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto230postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto230postinput {
  $state->{"M"} = $input->{"action"}[0];
  #(230)  IF M>0 AND M<4 THEN 235
  if ( $state->{"M"} > 0 and $state->{"M"} < 4 ) { my $out = { "anchor" => "goto235" }; return $out; }
  # 232  PRINT "TRY AGAIN..." 
  gameMessage( "TRY AGAIN...");
  #(232)  GOTO 220
  my $out = { "anchor" => "goto220" }; return $out;
}

sub goto235 {
  # 235  PRINT 
  blank();
  #(235)  ON M GOTO 250, 260, 270
#  if ( $state->{"M"} == 1 ) { my $out = { "anchor" => "goto250" }; return $out; }
  if ( $state->{"M"} == 2 ) { my $out = { "anchor" => "goto260" }; return $out; }
  if ( $state->{"M"} == 3 ) { my $out = { "anchor" => "goto270" }; return $out; }
  # 250  PRINT "YOU'RE NEARING STALINGRAD." 
  gameMessage( "YOU'RE NEARING STALINGRAD.");
###HERE
  #(250)  GOTO 280
  my $out = { "anchor" => "goto280" }; return $out;
}

sub goto260 {
  # 260  PRINT "NEARING LONDON.  BE CAREFUL, THEY'VE GOT RADAR." 
  gameMessage( "NEARING LONDON.  BE CAREFUL, THEY'VE GOT RADAR.");
  #(260)  GOTO 280
  my $out = { "anchor" => "goto280" }; return $out;
}

sub goto270 {
  # 270  PRINT "NEARING VERSAILLES.  DUCK SOUP.  THEY'RE NEARLY DEFENSELESS."
  gameMessage( "NEARING VERSAILLES.  DUCK SOUP.  THEY'RE NEARLY DEFENSELESS.");
  my $out = { "anchor" => "goto280" }; return $out;
}

sub goto280 {
  # 280  PRINT
  blank();
  my $out = { "anchor" => "goto285" }; return $out;
}

sub goto285 {
  # 285  INPUT "HOW MANY MISSIONS HAVE YOU FLOWN";D
  gameMessage($state->{"_prefix"}."HOW MANY MISSIONS HAVE YOU FLOWN");
  $state->{"_prefix"} = "";
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto285postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto285postinput {
  $state->{"D"} = $input->{"action"}[0];
  # 290  IF D<160 THEN 300
  if ( $state->{"D"} < 160 ) { my $out = { "anchor" => "goto300" }; return $out; }
  # 292  PRINT "MISSIONS, NOT MILES..." 
  gameMessage( "MISSIONS, NOT MILES...");
  # 295  PRINT "150 MISSIONS IS HIGH EVEN FOR OLD-TIMERS."
  gameMessage( "150 MISSIONS IS HIGH EVEN FOR OLD-TIMERS.");
  # 297  PRINT "NOW THEN, "; 
  $state->{"_prefix"} = "NOW THEN, ";
  #(297)  GOTO 285
  my $out = { "anchor" => "goto285" }; return $out;
}

sub goto300 {
  # 300  PRINT
  blank();
  #(300) IF D<100 THEN 310
  if ( $state->{"D"} < 100 ) { my $out = { "anchor" => "goto310" }; return $out; }
  # 305  PRINT "THAT'S PUSHING THE ODDS!" 
  gameMessage( "THAT'S PUSHING THE ODDS!");
  #(305)  GOTO 320
  my $out = { "anchor" => "goto320" }; return $out;
}

sub goto310 {
  # 310  IF D<25 THEN PRINT "FRESH OUT OF TRAINING, EH?"
  if ($state->{"D"} < 25) { gameMessage( "FRESH OUT OF TRAINING, EH?"); }
  my $out = { "anchor" => "goto320" }; return $out;
}

sub goto320 {
  # 320  PRINT 
  blank();
  #(320)  IF D<160*RND(1) THEN 330
  if ( $state->{"D"} < 160*rand() ) { my $out = { "anchor" => "goto330" }; return $out; }
  my $out = { "anchor" => "goto325" }; return $out;
}

sub goto325 {
  # 325  PRINT "DIRECT HIT!!!! "INT(100*RND(1))"KILLED."
  gameMessage( "DIRECT HIT!!!! ".int(100*rand())." KILLED.");
  # 327  PRINT "MISSION SUCCESSFUL." 
  gameMessage( "MISSION SUCCESSFUL.");
  #(327)  GOTO 390
  my $out = { "anchor" => "goto390" }; return $out;
}

sub goto330 {
  # 330  PRINT "MISSED TARGET BY"INT(2+30*RND(1))"MILES!"
  gameMessage( "MISSED TARGET BY ".int(2+30*rand())." MILES!");
  # 335  PRINT "NOW YOU'RE REALLY IN FOR IT !!" 
  gameMessage( "NOW YOU'RE REALLY IN FOR IT !!");
  #(335)  PRINT
  blank();
  my $out = { "anchor" => "goto340" }; return $out;
}

sub goto340 {
  # 340  INPUT "DOES THE ENEMY HAVE GUNS(1), MISSILES(2), OR BOTH(3)";R
  gameMessage("DOES THE ENEMY HAVE GUNS(1), MISSILES(2), OR BOTH(3)");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto340postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto340postinput {
  $state->{"R"} = $input->{"action"}[0];
  # 345  IF R>0 AND R<4 THEN 350
  if ( $state->{"R"} > 0 and $state->{"R"} < 4 ) { my $out = { "anchor" => "goto350" }; return $out; }
  # 347  PRINT "TRY AGAIN..." 
  gameMessage( "TRY AGAIN...");
  #(347)  GOTO 340
  my $out = { "anchor" => "goto340" }; return $out;
}

sub goto350 {
  # 350  PRINT 
  blank();
  #(350)  T=0 
  $state->{"T"} = 0;
  #(350)  IF R=2 THEN 360
  if ( $state->{"R"} == 2 ) { my $out = { "anchor" => "goto360" }; return $out; }
  # 355  INPUT "WHAT'S THE PERCENT HIT RATE OF ENEMY GUNNERS (10 TO 50)";S
  gameMessage("WHAT'S THE PERCENT HIT RATE OF ENEMY GUNNERS (10 TO 50)");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto355postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto355postinput {
  $state->{"S"} = $input->{"action"}[0];
  # 357  IF S<10 THEN PRINT "YOU LIE, BUT YOU'LL PAY..."
  #(357)  GOTO 380
  if ($state->{"S"} < 10) {
    gameMessage( "YOU LIE, BUT YOU'LL PAY...");
    my $out = { "anchor" => "goto380" };
    return $out;
  }
  my $out = { "anchor" => "goto358" }; return $out;
}

sub goto358 {
  # 358  PRINT
  blank();
  my $out = { "anchor" => "goto360" }; return $out;
}

sub goto360 {
  # 360  PRINT 
  blank();
  #(360)  IF R>1 THEN T=35
  if ( $state->{"R"} > 1 ) { $state->{"T"} = 35; }
  # 365  IF S+T>100*RND(1) THEN 380
  if ( $state->{"S"}+$state->{"T"} > 100*rand() ) { my $out = { "anchor" => "goto380" }; return $out; }
  # 370  PRINT "YOU MADE IT THROUGH TREMENDOUS FLAK!!" 
  gameMessage( "YOU MADE IT THROUGH TREMENDOUS FLAK!!");
  #(370)  GOTO 390
  my $out = { "anchor" => "goto390" }; return $out;
}

sub goto380 {
  # 380  PRINT "* * * * BOOM * * * *"
  gameMessage( "* * * * BOOM * * * *");
  # 384  PRINT "YOU HAVE BEEN SHOT DOWN....."
  gameMessage( "YOU HAVE BEEN SHOT DOWN.....");
  # 386  PRINT "DEARLY BELOVED, WE ARE GATHERED HERE TODAY TO PAY OUR"
  gameMessage( "DEARLY BELOVED, WE ARE GATHERED HERE TODAY TO PAY OUR");
  # 387  PRINT "LAST TRIBUTE..."
  gameMessage( "LAST TRIBUTE...");
  my $out = { "anchor" => "goto390" }; return $out;
}

sub goto390 {
  # 390  PRINT
  blank();
  #(390) PRINT
  blank();
  #(390) PRINT
  blank();
  #(390) INPUT "ANOTHER MISSION (Y OR N)";U$
  gameMessage("ANOTHER MISSION (Y OR N)");
  my $out = {
    "anchor" => "goto390postinput",
    "echo" => " > ",
    "hook" => "reply"
  };
  return $out;
}

sub goto390postinput {
  $state->{"Us"} = $input->{"action"}[0];
  # 395  IF U$="Y" THEN 8
  if ( $state->{"Us"} =~ /^Y$/ ) { my $out = { "anchor" => "goto8" }; return $out; }
  # 400  PRINT "CHICKEN !!!" 
  gameMessage( "CHICKEN !!!");
  #(400)  PRINT 
  blank();
  #(400)  END
  my $out = { "anchor" => "quit_game" }; return $out;
}

1;

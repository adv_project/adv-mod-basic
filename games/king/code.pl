#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

use Storable qw(dclone);

$__kingmsg__ = '';

sub initialize {
  my $out = { "anchor" => "goto1" };
  return $out;
}

#1 PRINT TAB(34);"KING"
#2 PRINT TAB(15);"CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY"
#3 PRINT:PRINT:PRINT
#4 PRINT "DO YOU WANT INSTRUCTIONS";
#5 INPUT Z$
sub goto1 {
  gameMessageTab(34, "KING");
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  blank(3);
  gameMessage("DO YOU WANT INSTRUCTIONS");
  my $out = {
    "anchor" => "goto6",
    "echo" => " > ",
    "argc" => 1,
    "hook" => "reply"
  };
  return $out;
}

#6 N5=8
#10 IF LEFT$(Z$,1)="N" THEN 47
#11 IF Z$="AGAIN" THEN 1960
#12 PRINT:PRINT:PRINT
#20 PRINT "CONGRATULATIONS! YOU'VE JUST BEEN ELECTED PREMIER OF SETATS"
#22 PRINT "DETINU, A SMALL COMMUNIST ISLAND 30 BY 70 MILES LONG. YOUR"
#24 PRINT "JOB IS TO DECIDE UPON THE CONTRY'S BUDGET AND DISTRIBUTE"
#26 PRINT "MONEY TO YOUR COUNTRYMEN FROM THE COMMUNAL TREASURY."
#28 PRINT "THE MONEY SYSTEM IS RALLODS, AND EACH PERSON NEEDS 100"
#30 PRINT "RALLODS PER YEAR TO SURVIVE. YOUR COUNTRY'S INCOME COMES"
#32 PRINT "FROM FARM PRODUCE AND TOURISTS VISITING YOUR MAGNIFICENT"
#34 PRINT "FORESTS, HUNTING, FISHING, ETC. HALF YOUR LAND IS FARM LAND"
#36 PRINT "WHICH ALSO HAS AN EXCELLENT MINERAL CONTENT AND MAY BE SOLD"
#38 PRINT "TO FOREIGN INDUSTRY (STRIP MINING) WHO IMPORT AND SUPPORT"
#40 PRINT "THEIR OWN WORKERS. CROPS COST BETWEEN 10 AND 15 RALLODS PER"
#42 PRINT "SQUARE MILE TO PLANT."
#44 PRINT "YOUR GOAL IS TO COMPLETE YOUR";N5;"YEAR TERM OF OFFICE."
#46 PRINT "GOOD LUCK!"
sub goto6 {
  $state->{"N5"} = 8;
  $state->{"Z"} = $input->{"action"}[0];
  if ($state->{"Z"} =~ /^N/) {
    my $out = { "anchor" => "goto47" };
    return $out;
  }
  if ($state->{"Z"} eq "AGAIN") {
    my $out = { "anchor" => "goto1960" };
    return $out;
  }
  blank(3);
  gameMessage("CONGRATULATIONS! YOU'VE JUST BEEN ELECTED PREMIER OF SETATS");
  gameMessage("DETINU, A SMALL COMMUNIST ISLAND 30 BY 70 MILES LONG. YOUR");
  gameMessage("JOB IS TO DECIDE UPON THE CONTRY'S BUDGET AND DISTRIBUTE");
  gameMessage("MONEY TO YOUR COUNTRYMEN FROM THE COMMUNAL TREASURY.");
  gameMessage("THE MONEY SYSTEM IS RALLODS, AND EACH PERSON NEEDS 100");
  gameMessage("RALLODS PER YEAR TO SURVIVE. YOUR COUNTRY'S INCOME COMES");
  gameMessage("FROM FARM PRODUCE AND TOURISTS VISITING YOUR MAGNIFICENT");
  gameMessage("FORESTS, HUNTING, FISHING, ETC. HALF YOUR LAND IS FARM LAND");
  gameMessage("WHICH ALSO HAS AN EXCELLENT MINERAL CONTENT AND MAY BE SOLD");
  gameMessage("TO FOREIGN INDUSTRY (STRIP MINING) WHO IMPORT AND SUPPORT");
  gameMessage("THEIR OWN WORKERS. CROPS COST BETWEEN 10 AND 15 RALLODS PER");
  gameMessage("SQUARE MILE TO PLANT.");
  gameMessage("YOUR GOAL IS TO COMPLETE YOUR ".$state->{"N5"}." YEAR TERM OF OFFICE.");
  gameMessage("GOOD LUCK!");
  my $out = { "anchor" => "goto47" };
  return $out;
}

#47 PRINT
#50 A=INT(60000+(1000*RND(1))-(1000*RND(1)))
#55 B=INT(500+(10*RND(1))-(10*RND(1)))
#65 D=2000
sub goto47 {
  blank();
  $state->{"A"} = int(60000 + (1000 * rand()) - (1000 * rand()));
  $state->{"B"} = int(500 + (10 * rand()) - (10 * rand()));
  $state->{"D"} = 2000;
  my $out = { "anchor" => "goto100" };
  return $out;
}

#100 W=INT(10*RND(1)+95)
#102 PRINT
#105 PRINT "YOU NOW HAVE ";A;" RALLODS IN THE TREASURY."
#110 PRINT INT(B);:PRINT "COUNTRYMEN, ";
#115 V9=INT(((RND(1)/2)*10+10))
#120 IF C=0 THEN 140
#130 PRINT INT(C);"FOREIGN WORKERS, ";
sub goto100 {
  $state->{"W"} = int(10 * rand() + 95);
  blank();
  gameMessage("YOU NOW HAVE ".$state->{"A"}." RALLODS IN THE TREASURY.");
  $__kingmsg__ = int($state->{"B"})." COUNTRYMEN, ";
  $state->{"V9"} = int( (rand() / 2) * 10 + 10 );
  if ( $state->{"C"} == 0 ) {
    my $out = { "anchor" => "goto140" };
    return $out;
  }
  $__kingmsg__ = $__kingmsg__.int($state->{"C"})." FOREIGN WORKERS, ";
  my $out = { "anchor" => "goto140" };
  return $out;
}

#140 PRINT "AND";INT(D);"SQ. MILES OF LAND."
#150 PRINT "THIS YEAR INDUSTRY WILL BUY LAND FOR";W;
#152 PRINT "RALLODS PER SQUARE MILE."
#155 PRINT "LAND CURRENTLY COSTS";V9;"RALLODS PER SQUARE MILE TO PLANT."
#162 PRINT
sub goto140 {
  $__kingmsg__ = $__kingmsg__."AND ".int($state->{"D"})." SQ. MILES OF LAND.";
  gameMessage($__kingmsg__);
  $__kingmsg__ = '';
  gameMessage("THIS YEAR INDUSTRY WILL BUY LAND FOR ".$state->{"W"}." RALLODS PER SQUARE MILE.");
  gameMessage("LAND CURRENTLY COSTS ".$state->{"V9"}." RALLODS PER SQUARE MILE TO PLANT.");
  blank();
  my $out = { "anchor" => "goto200" };
  return $out;
}

#200 PRINT "HOW MANY SQUARE MILES DO YOU WISH TO SELL TO INDUSTRY";
#210 INPUT H
sub goto200 {
  gameMessage("HOW MANY SQUARE MILES DO YOU WISH TO SELL TO INDUSTRY");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto215",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

#215 IF H<0 THEN 200
#220 IF H<=D-1000 THEN 300
#230 PRINT "***  THINK AGAIN. YOU ONLY HAVE";D-1000;"SQUARE MILES OF FARM LAND."
#240 IF X<>0 THEN 200
#250 PRINT:PRINT "(FOREIGN INDUSTRY WILL ONLY BUY FARM LAND BECAUSE"
#260 PRINT "FOREST LAND IS UNECONOMICAL TO STRIP MINE DUE TO TREES,"
#270 PRINT "THICKER TOP SOIL, ETC.)"
#280 X=1
#299 GOTO 200
sub goto215 {
  $state->{"H"} = $input->{"action"}[0];
  if ($state->{"H"} < 0) {
    my $out = { "anchor" => "goto200" };
    return $out;
  }
  if ($state->{"H"} <= $state->{"D"} - 1000) {
    my $out = { "anchor" => "goto300" };
    return $out;
  }
  my $land = $state->{"D"} - 1000;
  gameMessage("***  THINK AGAIN. YOU ONLY HAVE ".$land." SQUARE MILES OF FARM LAND.");
  if ($state->{"X"} != 0) {
    my $out = { "anchor" => "goto200" };
    return $out;
  }
  blank();
  gameMessage("(FOREIGN INDUSTRY WILL ONLY BUY FARM LAND BECAUSE");
  gameMessage("FOREST LAND IS UNECONOMICAL TO STRIP MINE DUE TO TREES,");
  gameMessage("THICKER TOP SOIL, ETC.)");
  $state->{"X"} = 1;
  my $out = { "anchor" => "goto200" };
  return $out;
}

#300 D=INT(D-H)
#310 A=INT(A+(H*W))
sub goto300 {
  $state->{"D"} = int($state->{"D"} - $state->{"H"});
  $state->{"A"} = int($state->{"A"} + ($state->{"H"} * $state->{"W"}));
  my $out = { "anchor" => "goto320" };
  return $out;
}

#320 PRINT "HOW MANY RALLODS WILL YOU DISTRIBUTE AMONG YOUR COUNTRYMEN";
#340 INPUT I
sub goto320 {
  gameMessage("HOW MANY RALLODS WILL YOU DISTRIBUTE AMONG YOUR COUNTRYMEN");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto342",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

#342 IF I<0 THEN 320
#350 IF I<A THEN 400
#360 IF I=A THEN 380
#370 PRINT "   THINK AGAIN. YOU'VE ONLY";A;" RALLODS IN THE TREASURY"
#375 GOTO 320
sub goto342 {
  $state->{"I"} = $input->{"action"}[0];
  if ($state->{"I"} < 0) {
    my $out = { "anchor" => "goto320" };
    return $out;
  }
  if ($state->{"I"} < $state->{"A"}) {
    my $out = { "anchor" => "goto400" };
    return $out;
  }
  my $land = $state->{"D"} - 1000;
  if ($state->{"I"} == $state->{"A"}) {
    my $out = { "anchor" => "goto380" };
    return $out;
  }
  gameMessage("   THINK AGAIN. YOU'VE ONLY ".$state->{"A"}." RALLODS IN THE TREASURY");
  my $out = { "anchor" => "goto380" };
  return $out;
}

#380 J=0
#390 K=0
#395 A=0
#399 GOTO 1000
sub goto380 {
  $state->{"J"} = 0;
  $state->{"K"} = 0;
  $state->{"A"} = 0;
  my $out = { "anchor" => "goto1000" };
  return $out;
}

#400 A=INT(A-I)
sub goto400 {
  $state->{"A"} = int($state->{"A"} - $state->{"I"});
  my $out = { "anchor" => "goto410" };
  return $out;
}

#410 PRINT "HOW MANY SQUARE MILES DO YOU WISH TO PLANT";
#420 INPUT J
sub goto410 {
  gameMessage("HOW MANY SQUARE MILES DO YOU WISH TO PLANT");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto421",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

#421 IF J<0 THEN 410
#422 IF J<=B*2 THEN 426
#423 PRINT "   SORRY, BUT EACH COUNTRYMAN CAN ONLY PLANT 2 SQ. MILES."
#424 GOTO 410
sub goto421 {
  $state->{"J"} = $input->{"action"}[0];
  if ($state->{"J"} < 0) {
    my $out = { "anchor" => "goto410" };
    return $out;
  }
  if ($state->{"J"} <= $state->{"B"} * 2) {
    my $out = { "anchor" => "goto426" };
    return $out;
  }
  gameMessage("   SORRY, BUT EACH COUNTRYMAN CAN ONLY PLANT 2 SQ. MILES.");
  my $out = { "anchor" => "goto410" };
  return $out;
}

#426 IF J<=D-1000 THEN 430
#427 PRINT "   SORRY, BUT YOU'VE ONLY";D-1000;"SQ. MILES OF FARM LAND."
#428 GOTO 410
sub goto426 {
  if ($state->{"J"} <= $state->{"D"} - 1000) {
    my $out = { "anchor" => "goto430" };
    return $out;
  }
  my $land = $state->{"D"} - 1000;
  gameMessage("   SORRY, BUT YOU'VE ONLY ".$land." SQ. MILES OF FARM LAND.");
  my $out = { "anchor" => "goto410" };
  return $out;
}

#430 U1=INT(J*V9)
#435 IF U1<A THEN 500
#440 IF U1=A THEN 490
#450 PRINT "   THINK AGAIN. YOU'VE ONLY";A;" RALLODS LEFT IN THE TREASURY."
#460 GOTO 410
sub goto430 {
  $state->{"U1"} = int($state->{"J"} * $state->{"V9"});
  if ($state->{"U1"} < $state->{"A"}) {
    my $out = { "anchor" => "goto500" };
    return $out;
  }
  if ($state->{"U1"} == $state->{"A"}) {
    my $out = { "anchor" => "goto490" };
    return $out;
  }
  gameMessage("   THINK AGAIN. YOU'VE ONLY ".$state->{"A"}." RALLODS LEFT IN THE TREASURY.");
  my $out = { "anchor" => "goto410" };
  return $out;
}

#490 K=0
#495 A=0
#499 GOTO 1000
sub goto490 {
  $state->{"K"} = 0;
  $state->{"A"} = 0;
  my $out = { "anchor" => "goto1000" };
  return $out;
}

#500 A=A-U1
sub goto500 {
  $state->{"A"} = $state->{"A"} - $state->{"U1"};
  my $out = { "anchor" => "goto510" };
  return $out;
}

#510 PRINT "HOW MANY RALLODS DO YOU WISH TO SPEND ON POLLUTION CONTROL";
#520 INPUT K
sub goto510 {
  gameMessage("HOW MANY RALLODS DO YOU WISH TO SPEND ON POLLUTION CONTROL");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto522",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

#522 IF K<0 THEN 510
#530 IF K<=A THEN 1000
#540 PRINT "   THINK AGAIN. YOU ONLY HAVE ";A;" RALLODS REMAINING."
#550 GOTO 510
sub goto522 {
  $state->{"K"} = $input->{"action"}[0];
  if ($state->{"K"} < 0) {
    my $out = { "anchor" => "goto510" };
    return $out;
  }
  if ($state->{"K"} <= $state->{"A"}) {
    my $out = { "anchor" => "goto1000" };
    return $out;
  }
  gameMessage("   THINK AGAIN. YOU ONLY HAVE ".$state->{"A"}." RALLODS REMAINING.");
  my $out = { "anchor" => "goto510" };
  return $out;
}

#600 IF H<>0 THEN 1002
#602 IF I<>0 THEN 1002
#604 IF J<>0 THEN 1002
#606 IF K<>0 THEN 1002
#609 PRINT
#612 PRINT "GOODBYE."
#614 PRINT "(IF YOU WISH TO CONTINUE THIS GAME AT A LATER DATE, ANSWER"
#616 PRINT "'AGAIN' WHEN ASKED IF YOU WANT INSTRUCTIONS AT THE START"
#617 PRINT "OF THE GAME)."
#618 STOP
sub goto600 {
  if ($state->{"H"} != 0) {
    my $out = { "anchor" => "goto1002" };
    return $out;
  }
  if ($state->{"I"} != 0) {
    my $out = { "anchor" => "goto1002" };
    return $out;
  }
  if ($state->{"J"} != 0) {
    my $out = { "anchor" => "goto1002" };
    return $out;
  }
  if ($state->{"K"} != 0) {
    my $out = { "anchor" => "goto1002" };
    return $out;
  }
  blank();
  gameMessage("GOODBYE.");
  gameMessage("(IF YOU WISH TO CONTINUE THIS GAME AT A LATER DATE, ANSWER");
  gameMessage("'AGAIN' WHEN ASKED IF YOU WANT INSTRUCTIONS AT THE START");
  gameMessage("OF THE GAME).");
  my $out = { "anchor" => "quit_game" };
  return $out;
}

#1000 GOTO 600
sub goto1000 {
  my $out = { "anchor" => "goto600" };
  return $out;
}

#1002 PRINT
#1003 PRINT
#1010 A=INT(A-K)
#1020 A4=A
#1100 IF INT(I/100-B)>=0 THEN 1120
#1105 IF I/100<50 THEN 1700
#1110 PRINT INT(B-(I/100));"COUNTRYMEN DIED OF STARVATION"
sub goto1002 {
  blank(2);
  $state->{"A"} = int($state->{"A"} - $state->{"K"});
  $state->{"A4"} = $state->{"A"};
  if ( int( $state->{"I"} / 100 - $state->{"B"}) >= 0 ) {
    my $out = { "anchor" => "goto1120" };
    return $out;
  }
  if ( $state->{"I"} / 100 > 50 ) {
    my $out = { "anchor" => "goto1700" };
    return $out;
  }
  gameMessage(int($state->{"B"} - ($state->{"I"} / 100))." COUNTRYMEN DIED OF STARVATION");
  my $out = { "anchor" => "goto1120" };
  return $out;
}

#1120 F1=INT(RND(1)*(2000-D))
#1122 IF K<25 THEN 1130
#1125 F1=INT(F1/(K/25))
sub goto1120 {
  $state->{"F1"} = int( rand() * (2000 - $state->{"D"}) );
  if ( $state->{"K"} < 25 ) {
    my $out = { "anchor" => "goto1130" };
    return $out;
  }
  $state->{"F1"} = int( $state->{"F1"} / ( $state->{"K"} / 25 ) );
  my $out = { "anchor" => "goto1130" };
  return $out;
}

#1130 IF F1<=0 THEN 1150
#1140 PRINT F1;"COUNTRYMEN DIED OF CARBON-MONOXIDE AND DUST INHALATION"
sub goto1130 {
  if ( $state->{"F1"} <= 0 ) {
    my $out = { "anchor" => "goto1150" };
    return $out;
  }
  gameMessage($state->{"F1"}." COUNTRYMEN DIED OF CARBON-MONOXIDE AND DUST INHALATION");
  my $out = { "anchor" => "goto1150" };
  return $out;
}

#1150 IF INT((I/100)-B)<0 THEN 1170
#1160 IF F1>0 THEN 1180
#1165 GOTO 1200
sub goto1150 {
  if ( int(($state->{"I"} / 100) - $state->{"B"}) < 0 ) {
    my $out = { "anchor" => "goto1170" };
    return $out;
  }
  if ($state->{"F1"} > 0 ) {
    my $out = { "anchor" => "goto1180" };
    return $out;
  }
  my $out = { "anchor" => "goto1200" };
  return $out;
}

#1170 PRINT "   YOU WERE FORCED TO SPEND";INT((F1+(B-(I/100)))*9);
#1172 PRINT "RALLODS ON FUNERAL EXPENSES"
#1174 B5=INT(F1+(B-(I/100)))
#1175 A=INT(A-((F1+(B-(I/100)))*9))
#1176 GOTO 1185
sub goto1170 {
  gameMessage("   YOU WERE FORCED TO SPEND ".int(($state->{"F1"} + ($state->{"B"} - ($state->{"I"} / 100))) * 9)." RALLODS ON FUNERAL EXPENSES");
  $state->{"B5"} = int( $state->{"F1"} + ( $state->{"B"} - ( $state->{"I"} / 100 )));
  $state->{"A"} = int( $state->{"A"} - (( $state->{"F1"} + ( $state->{"B"} - ( $state->{"I"} / 100 ))) * 9 ));
  my $out = { "anchor" => "goto1185" };
  return $out;
}

#1180 PRINT "   YOU WERE FORCED TO SPEND ";INT(F1*9);"RALLODS ON ";
#1181 PRINT "FUNERAL EXPENSES."
#1182 B5=F1
#1183 A=INT(A-(F1*9))
sub goto1180 {
  gameMessage("   YOU WERE FORCED TO SPEND ".int($state->{"F1"} * 9)." RALLODS ON FUNERAL EXPENSES.");
  $state->{"B5"} = $state->{"F1"};
  $state->{"A"} = int( $state->{"A"} - ( $state->{"F1"} * 9 ));
  my $out = { "anchor" => "goto1185" };
  return $out;
}

#1185 IF A>=0 THEN 1194
#1187 PRINT "   INSUFFICIENT RESERVES TO COVER COST - LAND WAS SOLD"
#1189 D=INT(D+(A/W))
#1190 A=0
sub goto1185 {
  if ( $state->{"A"} >= 0 ) {
    my $out = { "anchor" => "goto1194" };
    return $out;
  }
  gameMessage("   INSUFFICIENT RESERVES TO COVER COST - LAND WAS SOLD");
  $state->{"D"} = int( $state->{"D"} + ( $state->{"A"} / $state->{"W"} ));
  $state->{"A"} = 0;
  my $out = { "anchor" => "goto1194" };
  return $out;
}

#1194 B=INT(B-B5)
sub goto1194 {
  $state->{"B"} = int( $state->{"B"} - $state->{"B5"} );
  my $out = { "anchor" => "goto1200" };
  return $out;
}

#1200 IF H=0 THEN 1250
#1220 C1=INT(H+(RND(1)*10)-(RND(1)*20))
#1224 IF C>0 THEN 1230
#1226 C1=C1+20
sub goto1200 {
  if ( $state->{"H"} == 0 ) {
    my $out = { "anchor" => "goto1250" };
    return $out;
  }
  $state->{"C1"} = int( $state->{"H"} + ( rand() * 10 ) - ( rand() * 20 ));
  if ( $state->{"C"} > 0 ) {
    my $out = { "anchor" => "goto1230" };
    return $out;
  }
  $state->{"C1"} = $state->{"C1"} + 20;
  my $out = { "anchor" => "goto1230" };
  return $out;
}

#1230 PRINT C1;"WORKERS CAME TO THE COUNTRY AND";
sub goto1230 {
  $__kingmsg__ = $state->{"C1"}." WORKERS CAME TO THE COUNTRY AND ";
  my $out = { "anchor" => "goto1250" };
  return $out;
}

#1250 P1=INT(((I/100-B)/10)+(K/25)-((2000-D)/50)-(F1/2))
#1255 PRINT ABS(P1);"COUNTRYMEN ";
#1260 IF P1<0 THEN 1275
#1265 PRINT "CAME TO";
#1270 GOTO 1280
sub goto1250 {
  $state->{"P1"} = int((( $state->{"I"} / 100 - $state->{"B"} ) / 10 ) + ( $state->{"K"} / 25 ) - (( 2000 - $state->{"D"} ) / 50 ) - ( $state->{"F1"} / 2 ));
  $__kingmsg__ = $__kingmsg__.abs($state->{"P1"})." COUNTRYMEN ";
  if ( $state->{"P1"} < 0 ) {
    my $out = { "anchor" => "goto1275" };
    return $out;
  }
  $__kingmsg__ = $__kingmsg__."CAME TO";
  my $out = { "anchor" => "goto1280" };
  return $out;
}

#1275 PRINT "LEFT";
sub goto1275 {
  $__kingmsg__ = $__kingmsg__."LEFT";
  my $out = { "anchor" => "goto1280" };
  return $out;
}

#1280 PRINT " THE ISLAND."
#1290 B=INT(B+P1)
#1292 C=INT(C+C1)
#1305 U2=INT(((2000-D)*((RND(1)+1.5)/2)))
#1310 IF C=0 THEN 1324
#1320 PRINT "OF ";INT(J);"SQ. MILES PLANTED,";
sub goto1280 {
  $__kingmsg__ = $__kingmsg__." THE ISLAND.";
  gameMessage($__kingmsg__);
  $__kingmsg__ = '';
  $state->{"B"} = int( $state->{"B"} + $state->{"P1"} );
  $state->{"C"} = int( $state->{"C"} + $state->{"C1"} );
  $state->{"U2"} = int((( 2000 - $state->{"D"} ) * (( rand() + 1.5 ) / 2 )));
  if ( $state->{"C"} == 0 ) {
    my $out = { "anchor" => "goto1324" };
    return $out;
  }
  $__kingmsg__ = "OF ".int($state->{"J"})." SQ. MILES PLANTED,";
  my $out = { "anchor" => "goto1324" };
  return $out;
}

#1324 IF J>U2 THEN 1330
#1326 U2=J
sub goto1324 {
  if ( $state->{"J"} > $state->{"U2"} ) {
    my $out = { "anchor" => "goto1330" };
    return $out;
  }
  $state->{"U2"} = $state->{"J"};
  my $out = { "anchor" => "goto1330" };
  return $out;
}

#1330 PRINT " YOU HARVESTED ";INT(J-U2);"SQ. MILES OF CROPS."
#1340 IF U2=0 THEN 1370
#1344 IF T1>=2 THEN 1370
#1350 PRINT "   (DUE TO ";
#1355 IF T1=0 THEN 1365
#1360 PRINT "INCREASED ";
sub goto1330 {
  $__kingmsg__ = $__kingmsg__." YOU HARVESTED ".int($state->{"J"} - $state->{"U2"})." SQ. MILES OF CROPS.";
  gameMessage($__kingmsg__);
  $__kingmsg__ = '';
  if ( $state->{"U2"} == 0 ) {
    my $out = { "anchor" => "goto1370" };
    return $out;
  }
  if ( $state->{"T1"} >= 2 ) {
    my $out = { "anchor" => "goto1370" };
    return $out;
  }
  $__kingmsg__ = "   (DUE TO ";
  if ( $state->{"T1"} == 0 ) {
    my $out = { "anchor" => "goto1365" };
    return $out;
  }
  $__kingmsg__ = $__kingmsg__."INCREASED ";
  my $out = { "anchor" => "goto1365" };
  return $out;
}

#1365 PRINT "AIR AND WATER POLLUTION FROM FOREIGN INDUSTRY.)"
sub goto1365 {
  $__kingmsg__ = $__kingmsg__."AIR AND WATER POLLUTION FROM FOREIGN INDUSTRY.)";
  gameMessage($__kingmsg__);
  $__kingmsg__ = '';
  my $out = { "anchor" => "goto1370" };
  return $out;
}

#1370 Q=INT((J-U2)*(W/2))
#1380 PRINT "MAKING";INT(Q);"RALLODS."
#1390 A=INT(A+Q)
#1400 V1=INT(((B-P1)*22)+(RND(1)*500))
#1405 V2=INT((2000-D)*15)
#1410 PRINT " YOU MADE";ABS(INT(V1-V2));"RALLODS FROM TOURIST TRADE."
#1420 IF V2=0 THEN 1450
#1425 IF V1-V2>=V3 THEN 1450
#1430 PRINT "   DECREASE BECAUSE ";
#1435 G1=10*RND(1)
#1440 IF G1<=2 THEN 1460
#1442 IF G1<=4 THEN 1465
#1444 IF G1<=6 THEN 1470
#1446 IF G1<=8 THEN 1475
#1448 IF G1<=10 THEN 1480
sub goto1370 {
  $state->{"Q"} = int(( $state->{"J"} - $state->{"U2"} ) * ( $state->{"W"} / 2 ));
  gameMessage("MAKING ".int($state->{"Q"})." RALLODS.");
  $state->{"A"} = int( $state->{"A"} + $state->{"Q"} );
  $state->{"V1"} = int((( $state->{"B"} - $state->{"P1"} ) * 22 ) + ( rand() * 500 ));
  $state->{"V2"} = int(( 2000 - $state->{"D"} ) * 15 );
  gameMessage(" YOU MADE ".abs(int( $state->{"V1"} - $state->{"V2"} ))." RALLODS FROM TOURIST TRADE.");
  if ( $state->{"V2"} == 0 ) {
    my $out = { "anchor" => "goto1450" };
    return $out;
  }
  if ( $state->{"V1"} - $state->{"V2"} >= $state->{"V3"} ) {
    my $out = { "anchor" => "goto1450" };
    return $out;
  }
  $__kingmsg__ = "   DECREASE BECAUSE ";
  $state->{"G1"} = 10 * rand();
  if ( $state->{"G1"} <= 2 ) {
    my $out = { "anchor" => "goto1460" };
    return $out;
  }
  if ( $state->{"G1"} <= 4 ) {
    my $out = { "anchor" => "goto1465" };
    return $out;
  }
  if ( $state->{"G1"} <= 6 ) {
    my $out = { "anchor" => "goto1470" };
    return $out;
  }
  if ( $state->{"G1"} <= 8 ) {
    my $out = { "anchor" => "goto1475" };
    return $out;
  }
  # Commenting this just to be more 'defensive', since
  # 10 * rand() should always be <= 10. But we don't
  # want to return undef here, so just in case...
#  if ( $state->{"G1"} <= 10 ) {
    my $out = { "anchor" => "goto1480" };
    return $out;
#  }
}

#1450 V3=INT(A+V3)
#1451 A=INT(A+V3)
#1452 GOTO 1500
sub goto1450 {
  $state->{"V3"} = int($state->{"A"} + $state->{"V3"});
  $state->{"A"} = int($state->{"A"} + $state->{"V3"});
  my $out = { "anchor" => "goto1500" };
  return $out;
}

#1460 PRINT "FISH POPULATION HAS DWINDLED DUE TO WATER POLLUTION."
#1462 GOTO 1450
sub goto1460 {
  $__kingmsg__ = $__kingmsg__."FISH POPULATION HAS DWINDLED DUE TO WATER POLLUTION.";
  gameMessage($__kingmsg__);
  $__kingmsg__ = '';
  my $out = { "anchor" => "goto1450" };
  return $out;
}

#1465 PRINT "AIR POLLUTION IS KILLING GAME BIRD POPULATION."
#1467 GOTO 1450
sub goto1465 {
  $__kingmsg__ = $__kingmsg__."AIR POLLUTION IS KILLING GAME BIRD POPULATION.";
  gameMessage($__kingmsg__);
  $__kingmsg__ = '';
  my $out = { "anchor" => "goto1450" };
  return $out;
}

#1470 PRINT "MINERAL BATHS ARE BEING RUINED BY WATER POLLUTION."
#1472 GOTO 1450
sub goto1470 {
  $__kingmsg__ = $__kingmsg__."MINERAL BATHS ARE BEING RUINED BY WATER POLLUTION.";
  gameMessage($__kingmsg__);
  $__kingmsg__ = '';
  my $out = { "anchor" => "goto1450" };
  return $out;
}

#1475 PRINT "UNPLEASANT SMOG IS DISCOURAGING SUN BATHERS."
#1477 GOTO 1450
sub goto1475 {
  $__kingmsg__ = $__kingmsg__."UNPLEASANT SMOG IS DISCOURAGING SUN BATHERS.";
  gameMessage($__kingmsg__);
  $__kingmsg__ = '';
  my $out = { "anchor" => "goto1450" };
  return $out;
}

#1480 PRINT "HOTELS ARE LOOKING SHABBY DUE TO SMOG GRIT."
#1482 GOTO 1450
sub goto1480 {
  $__kingmsg__ = $__kingmsg__."HOTELS ARE LOOKING SHABBY DUE TO SMOG GRIT.";
  gameMessage($__kingmsg__);
  $__kingmsg__ = '';
  my $out = { "anchor" => "goto1450" };
  return $out;
}

#1500 IF B5>200 THEN 1600
#1505 IF B<343 THEN 1700
#1510 IF (A4/100)>5 THEN 1800
sub goto1500 {
  if ( $state->{"B5"} > 200 ) {
    my $out = { "anchor" => "goto1600" };
    return $out;
  }
  if ( $state->{"B"} < 343 ) {
    my $out = { "anchor" => "goto1700" };
    return $out;
  }
  if ( $state->{"A"} / 4 > 5 ) {
    my $out = { "anchor" => "goto1800" };
    return $out;
  }
  my $out = { "anchor" => "goto1515" };
  return $out;
}

#1515 IF C>B THEN 1550
#1520 IF N5-1=X5 THEN 1900
#1545 GOTO 2000
sub goto1515 {
  if ( $state->{"C"} > $state->{"B"} ) {
    my $out = { "anchor" => "goto1550" };
    return $out;
  }
  if ( $state->{"N5"} - 1 > $state->{"X5"} ) {
    my $out = { "anchor" => "goto1900" };
    return $out;
  }
  my $out = { "anchor" => "goto2000" };
  return $out;
}

#1550 PRINT
#1552 PRINT
#1560 PRINT "THE NUMBER OF FOREIGN WORKERS HAS EXCEEDED THE NUMBER"
#1562 PRINT "OF COUNTRYMEN. AS A MINORITY, THEY HAVE REVOLTED AND"
#1564 PRINT "TAKEN OVER THE COUNTRY."
sub goto1550 {
  blank(2);
  gameMessage("THE NUMBER OF FOREIGN WORKERS HAS EXCEEDED THE NUMBER");
  gameMessage("OF COUNTRYMEN. AS A MINORITY, THEY HAVE REVOLTED AND");
  gameMessage("TAKEN OVER THE COUNTRY.");
  my $out = { "anchor" => "goto1570" };
  return $out;
}

#1570 IF RND(1)<=.5 THEN 1580
#1574 PRINT "YOU HAVE BEEN THROWN OUT OF OFFICE AND ARE NOW"
#1576 PRINT "RESIDING IN PRISON."
#1578 GOTO 1590
sub goto1570 {
  if ( rand() <= 0.5 ) {
    my $out = { "anchor" => "goto1580" };
    return $out;
  }
  gameMessage("YOU HAVE BEEN THROWN OUT OF OFFICE AND ARE NOW");
  gameMessage("RESIDING IN PRISON.");
  my $out = { "anchor" => "goto1590" };
  return $out;
}

#1580 PRINT "YOU HAVE BEEN ASSASSINATED."
sub goto1580 {
  gameMessage("YOU HAVE BEEN ASSASSINATED.");
  my $out = { "anchor" => "goto1590" };
  return $out;
}

#1590 PRINT
#1592 PRINT
#1596 STOP
sub goto1590 {
  blank(2);
  my $out = { "anchor" => "quit_game" };
  return $out;
}

#1600 PRINT
#1602 PRINT
#1610 PRINT B5;"COUNTRYMEN DIED IN ONE YEAR!!!!!"
#1615 PRINT "DUE TO THIS EXTREME MISMANAGEMENT, YOU HAVE NOT ONLY"
#1620 PRINT "BEEN IMPEACHED AND THROWN OUT OF OFFICE, BUT YOU"
#1622 M6=INT(RND(1)*10)
#1625 IF M6<=3 THEN 1670
#1630 IF M6<=6 THEN 1680
#1635 IF M6<=10 THEN 1690
sub goto1600 {
  blank(2);
  gameMessage($state->{"B5"}." COUNTRYMEN DIED IN ONE YEAR!!!!!");
  gameMessage("DUE TO THIS EXTREME MISMANAGEMENT, YOU HAVE NOT ONLY");
  gameMessage("BEEN IMPEACHED AND THROWN OUT OF OFFICE, BUT YOU");
  $state->{"M6"} = int( rand() * 10 );
  if ( $state->{"M6"} <= 3 ) {
    my $out = { "anchor" => "goto1670" };
    return $out;
  }
  if ( $state->{"M6"} <= 6 ) {
    my $out = { "anchor" => "goto1680" };
    return $out;
  }
#  if ( $state->{"M6"} <= 10 ) {
    my $out = { "anchor" => "goto1690" };
    return $out;
#  }
}

#1670 PRINT "ALSO HAD YOUR LEFT EYE GOUGED OUT!"
#1672 GOTO 1590
sub goto1670 {
  gameMessage("ALSO HAD YOUR LEFT EYE GOUGED OUT!");
  my $out = { "anchor" => "goto1590" };
  return $out;
}

#1680 PRINT "HAVE ALSO GAINED A VERY BAD REPUTATION."
#1682 GOTO 1590
sub goto1680 {
  gameMessage("HAVE ALSO GAINED A VERY BAD REPUTATION.");
  my $out = { "anchor" => "goto1590" };
  return $out;
}

#1690 PRINT "HAVE ALSO BEEN DECLARED NATIONAL FINK."
#1692 GOTO 1590
sub goto1690 {
  gameMessage("HAVE ALSO BEEN DECLARED NATIONAL FINK.");
  my $out = { "anchor" => "goto1590" };
  return $out;
}

#1700 PRINT
#1702 PRINT
#1710 PRINT "OVER ONE THIRD OF THE POPULTATION HAS DIED SINCE YOU"
#1715 PRINT "WERE ELECTED TO OFFICE. THE PEOPLE (REMAINING)"
#1720 PRINT "HATE YOUR GUTS."
#1730 GOTO 1570
sub goto1700 {
  blank(2);
  gameMessage("OVER ONE THIRD OF THE POPULATION HAS DIED SINCE YOU");
  gameMessage("WERE ELECTED TO OFFICE. THE PEOPLE (REMAINING)");
  gameMessage("HATE YOUR GUTS.");
  my $out = { "anchor" => "goto1570" };
  return $out;
}

#1800 IF B5-F1<2 THEN 1515
#1807 PRINT
#1815 PRINT "MONEY WAS LEFT OVER IN THE TREASURY WHICH YOU DID"
#1820 PRINT "NOT SPEND. AS A RESULT, SOME OF YOUR COUNTRYMEN DIED"
#1825 PRINT "OF STARVATION. THE PUBLIC IS ENRAGED AND YOU HAVE"
#1830 PRINT "BEEN FORCED TO EITHER RESIGN OR COMMIT SUICIDE."
#1835 PRINT "THE CHOICE IS YOURS."
#1840 PRINT "IF YOU CHOOSE THE LATTER, PLEASE TURN OFF YOUR COMPUTER"
#1845 PRINT "BEFORE PROCEEDING."
#1850 GOTO 1590
sub goto1800 {
  if ( $state->{"B5"} - $state->{"F1"} < 2 ) {
    my $out = { "anchor" => "goto1515" };
    return $out;
  }
  blank();
  gameMessage("MONEY WAS LEFT OVER IN THE TREASURY WHICH YOU DID");
  gameMessage("NOT SPEND. AS A RESULT, SOME OF YOUR COUNTRYMEN DIED");
  gameMessage("OF STARVATION. THE PUBLIC IS ENRAGED AND YOU HAVE");
  gameMessage("BEEN FORCED TO EITHER RESIGN OR COMMIT SUICIDE.");
  gameMessage("THE CHOICE IS YOURS.");
  gameMessage("IF YOU CHOOSE THE LATTER, PLEASE TURN OFF YOUR COMPUTER");
  gameMessage("BEFORE PROCEEDING.");
  my $out = { "anchor" => "goto1590" };
  return $out;
}

#1900 PRINT
#1902 PRINT
#1920 PRINT "CONGRATULATIONS!!!!!!!!!!!!!!!!!!"
#1925 PRINT "YOU HAVE SUCCESFULLY COMPLETED YOUR";N5;"YEAR TERM"
#1930 PRINT "OF OFFICE. YOU WERE, OF COURSE, EXTREMELY LUCKY, BUT"
#1935 PRINT "NEVERTHELESS, IT'S QUITE AN ACHIEVEMENT. GOODBYE AND GOOD"
#1940 PRINT "LUCK - YOU'LL PROBABLY NEED IT IF YOU'RE THE TYPE THAT"
#1945 PRINT "PLAYS THIS GAME."
#1950 GOTO 1590
sub goto1900 {
  blank(2);
  gameMessage("CONGRATULATIONS!!!!!!!!!!!!!!!!!!");
  gameMessage("YOU HAVE SUCCESFULLY COMPLETED YOUR ".$state->{"N5"}." YEAR TERM");
  gameMessage("OF OFFICE. YOU WERE, OF COURSE, EXTREMELY LUCKY, BUT");
  gameMessage("NEVERTHELESS, IT'S QUITE AN ACHIEVEMENT. GOODBYE AND GOOD");
  gameMessage("LUCK - YOU'LL PROBABLY NEED IT IF YOU'RE THE TYPE THAT");
  gameMessage("PLAYS THIS GAME.");
  my $out = { "anchor" => "goto1590" };
  return $out;
}

#1960 PRINT "HOW MANY YEARS HAD YOU BEEN IN OFFICE WHEN INTERRUPTED";
#1961 INPUT X5
sub goto1960 {
  gameMessage("HOW MANY YEARS HAD YOU BEEN IN OFFICE WHEN INTERRUPTED");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto1962",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

#1962 IF X5<0 THEN 1590
#1963 IF X5<8 THEN 1969
#1965 PRINT "   COME ON, YOUR TERM IN OFFICE IS ONLY";N5;"YEARS."
#1967 GOTO 1960
sub goto1962 {
  $state->{"X5"} = $input->{"action"}[0];
  if ( $state->{"X5"} < 0 ) {
    my $out = { "anchor" => "goto1590" };
    return $out;
  }
  if ( $state->{"X5"} < 8 ) {
    my $out = { "anchor" => "goto1969" };
    return $out;
  }
  gameMessage("   COME ON, YOUR TERM IN OFFICE IS ONLY ".$state->{"N5"}." YEARS.");
  my $out = { "anchor" => "goto1960" };
  return $out;
}

#1969 PRINT "HOW MUCH DID YOU HAVE IN THE TREASURY";
#1970 INPUT A
sub goto1969 {
  gameMessage("HOW MUCH DID YOU HAVE IN THE TREASURY");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto1971",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

#1971 IF A<0 THEN 1590
#1975 PRINT "HOW MANY COUNTRYMEN";    
#1976 INPUT B
sub goto1971 {
  $state->{"A"} = $input->{"action"}[0];
  if ( $state->{"A"} < 0 ) {
    my $out = { "anchor" => "goto1590" };
    return $out;
  }
  gameMessage("HOW MANY COUNTRYMEN");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto1977",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

#1977 IF B<0 THEN 1590
#1980 PRINT "HOW MANY WORKERS";
#1981 INPUT C
sub goto1977 {
  $state->{"B"} = $input->{"action"}[0];
  if ( $state->{"B"} < 0 ) {
    my $out = { "anchor" => "goto1590" };
    return $out;
  }
  gameMessage("HOW MANY WORKERS");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto1982",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

#1982 IF C<0 THEN 1590
sub goto1982 {
  $state->{"C"} = $input->{"action"}[0];
  if ( $state->{"C"} < 0 ) {
    my $out = { "anchor" => "goto1590" };
    return $out;
  }
  my $out = { "anchor" => "goto1990" };
  return $out;
}

#1990 PRINT "HOW MANY SQUARE MILES OF LAND";
#1991 INPUT D
sub goto1990 {
  gameMessage("HOW MANY SQUARE MILES OF LAND");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto1992",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

#1992 IF D<0 THEN 1590
#1993 IF D>2000 THEN 1996
#1994 IF D>1000 THEN 100
sub goto1992 {
  $state->{"D"} = $input->{"action"}[0];
  if ( $state->{"D"} < 0 ) {
    my $out = { "anchor" => "goto1590" };
    return $out;
  }
  if ( $state->{"D"} > 2000 ) {
    my $out = { "anchor" => "goto1996" };
    return $out;
  }
  if ( $state->{"D"} > 1000 ) {
    my $out = { "anchor" => "goto100" };
    return $out;
  }
}

#1996 PRINT "   COME ON, YOU STARTED WITH 1000 SQ. MILES OF FARM LAND"
#1997 PRINT "   AND 10,000 SQ. MILES OF FOREST LAND."
#1998 GOTO 1990
sub goto1996 {
  gameMessage("   COME ON, YOU STARTED WITH 1000 SQ. MILES OF FARM LAND");
  gameMessage("   AND 10,000 SQ. MILES OF FOREST LAND.");
  my $out = { "anchor" => "goto1990" };
  return $out;
}

#2000 X5=X5+1
#2020 B5=0
#2040 GOTO 100
#2046 END
sub goto2000 {
  $state->{"X5"} = $state->{"X5"} + 1;
  $state->{"B5"} = 0;
  my $out = { "anchor" => "goto100" };
  return $out;
}

1;

#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

  #(<<<GOTO>> ) 490, 400, 430, 470, 310, 500, 270
  #(<<<GOSUB>> ) 

sub initialize {
  my $out = { "anchor" => "goto2" }; return $out;
}

sub goto2 {
  # 2  PRINT TAB(33);"ORBIT"
  gameMessageTab(33, "ORBIT");
  # 4  PRINT TAB(15);"CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY"
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  # 6  PRINT
  blank();
  #(6) PRINT
  blank();
  #(6) PRINT
  blank();
  # 10  PRINT "SOMEWHERE ABOVE YOUR PLANET IS A ROMULAN SHIP."
  gameMessage( "SOMEWHERE ABOVE YOUR PLANET IS A ROMULAN SHIP.");
  # 15  PRINT
  blank();
  # 20  PRINT "THE SHIP IS IN A CONSTANT POLAR ORBIT.  ITS"
  gameMessage( "THE SHIP IS IN A CONSTANT POLAR ORBIT.  ITS");
  # 25  PRINT "DISTANCE FROM THE CENTER OF YOUR PLANET IS FROM"
  gameMessage( "DISTANCE FROM THE CENTER OF YOUR PLANET IS FROM");
  # 30  PRINT "10,000 TO 30,000 MILES AND AT ITS PRESENT VELOCITY CAN"
  gameMessage( "10,000 TO 30,000 MILES AND AT ITS PRESENT VELOCITY CAN");
  # 31  PRINT "CIRCLE YOUR PLANET ONCE EVERY 12 TO 36 HOURS."
  gameMessage( "CIRCLE YOUR PLANET ONCE EVERY 12 TO 36 HOURS.");
  # 35  PRINT
  blank();
  # 40  PRINT "UNFORTUNATELY, THEY ARE USING A CLOAKING DEVICE SO"
  gameMessage( "UNFORTUNATELY, THEY ARE USING A CLOAKING DEVICE SO");
  # 45  PRINT "YOU ARE UNABLE TO SEE THEM, BUT WITH A SPECIAL"
  gameMessage( "YOU ARE UNABLE TO SEE THEM, BUT WITH A SPECIAL");
  # 50  PRINT "INSTRUMENT YOU CAN TELL HOW NEAR THEIR SHIP YOUR"
  gameMessage( "INSTRUMENT YOU CAN TELL HOW NEAR THEIR SHIP YOUR");
  # 55  PRINT "PHOTON BOMB EXPLODED.  YOU HAVE SEVEN HOURS UNTIL THEY"
  gameMessage( "PHOTON BOMB EXPLODED.  YOU HAVE SEVEN HOURS UNTIL THEY");
  # 60  PRINT "HAVE BUILT UP SUFFICIENT POWER IN ORDER TO ESCAPE"
  gameMessage( "HAVE BUILT UP SUFFICIENT POWER IN ORDER TO ESCAPE");
  # 65  PRINT "YOUR PLANET'S GRAVITY."
  gameMessage( "YOUR PLANET'S GRAVITY.");
  # 70  PRINT
  blank();
  # 75  PRINT "YOUR PLANET HAS ENOUGH POWER TO FIRE ONE BOMB AN HOUR."
  gameMessage( "YOUR PLANET HAS ENOUGH POWER TO FIRE ONE BOMB AN HOUR.");
  # 80  PRINT
  blank();
  # 85  PRINT "AT THE BEGINNING OF EACH HOUR YOU WILL BE ASKED TO GIVE AN"
  gameMessage( "AT THE BEGINNING OF EACH HOUR YOU WILL BE ASKED TO GIVE AN");
  # 90  PRINT "ANGLE (BETWEEN 0 AND 360) AND A DISTANCE IN UNITS OF"
  gameMessage( "ANGLE (BETWEEN 0 AND 360) AND A DISTANCE IN UNITS OF");
  # 95  PRINT "100 MILES (BETWEEN 100 AND 300), AFTER WHICH YOUR BOMB'S"
  gameMessage( "100 MILES (BETWEEN 100 AND 300), AFTER WHICH YOUR BOMB'S");
  # 100  PRINT "DISTANCE FROM THE ENEMY SHIP WILL BE GIVEN."
  gameMessage( "DISTANCE FROM THE ENEMY SHIP WILL BE GIVEN.");
  # 105  PRINT
  blank();
  # 110  PRINT "AN EXPLOSION WITHIN 5,000 MILES OF THE ROMULAN SHIP"
  gameMessage( "AN EXPLOSION WITHIN 5,000 MILES OF THE ROMULAN SHIP");
  # 111  PRINT "WILL DESTROY IT."
  gameMessage( "WILL DESTROY IT.");
  # 114  PRINT
  blank();
  # 115  PRINT "BELOW IS A DIAGRAM TO HELP YOU VISUALIZE YOUR PLIGHT."
  gameMessage( "BELOW IS A DIAGRAM TO HELP YOU VISUALIZE YOUR PLIGHT.");
  # 116  PRINT
  blank();
  # 117  PRINT
  blank();
  # 168  PRINT "                          90"
  gameMessage( "                          90");
  # 170  PRINT "                    0000000000000"
  gameMessage( "                    0000000000000");
  # 171  PRINT "                 0000000000000000000"
  gameMessage( "                 0000000000000000000");
  # 172  PRINT "               000000           000000"
  gameMessage( "               000000           000000");
  # 173  PRINT "             00000                 00000"
  gameMessage( "             00000                 00000");
  # 174  PRINT "            00000    XXXXXXXXXXX    00000"
  gameMessage( "            00000    XXXXXXXXXXX    00000");
  # 175  PRINT "           00000    XXXXXXXXXXXXX    00000"
  gameMessage( "           00000    XXXXXXXXXXXXX    00000");
  # 176  PRINT "          0000     XXXXXXXXXXXXXXX     0000"
  gameMessage( "          0000     XXXXXXXXXXXXXXX     0000");
  # 177  PRINT "         0000     XXXXXXXXXXXXXXXXX     0000"
  gameMessage( "         0000     XXXXXXXXXXXXXXXXX     0000");
  # 178  PRINT "        0000     XXXXXXXXXXXXXXXXXXX     0000"
  gameMessage( "        0000     XXXXXXXXXXXXXXXXXXX     0000");
  # 179  PRINT "180<== 00000     XXXXXXXXXXXXXXXXXXX     00000 ==>0"
  gameMessage( "180<== 00000     XXXXXXXXXXXXXXXXXXX     00000 ==>0");
  # 180  PRINT "        0000     XXXXXXXXXXXXXXXXXXX     0000"
  gameMessage( "        0000     XXXXXXXXXXXXXXXXXXX     0000");
  # 181  PRINT "         0000     XXXXXXXXXXXXXXXXX     0000"
  gameMessage( "         0000     XXXXXXXXXXXXXXXXX     0000");
  # 182  PRINT "          0000     XXXXXXXXXXXXXXX     0000"
  gameMessage( "          0000     XXXXXXXXXXXXXXX     0000");
  # 183  PRINT "           00000    XXXXXXXXXXXXX    00000"
  gameMessage( "           00000    XXXXXXXXXXXXX    00000");
  # 184  PRINT "            00000    XXXXXXXXXXX    00000"
  gameMessage( "            00000    XXXXXXXXXXX    00000");
  # 185  PRINT "             00000                 00000"
  gameMessage( "             00000                 00000");
  # 186  PRINT "               000000           000000"
  gameMessage( "               000000           000000");
  # 187  PRINT "                 0000000000000000000"
  gameMessage( "                 0000000000000000000");
  # 188  PRINT "                    0000000000000"
  gameMessage( "                    0000000000000");
  # 190  PRINT "                         270"
  gameMessage( "                         270");
  # 192  PRINT
  blank();
  # 195  PRINT "X - YOUR PLANET"
  gameMessage( "X - YOUR PLANET");
  # 196  PRINT "O - THE ORBIT OF THE ROMULAN SHIP"
  gameMessage( "O - THE ORBIT OF THE ROMULAN SHIP");
  # 197  PRINT
  blank();
  # 198  PRINT "ON THE ABOVE DIAGRAM, THE ROMULAN SHIP IS CIRCLING"
  gameMessage( "ON THE ABOVE DIAGRAM, THE ROMULAN SHIP IS CIRCLING");
  # 199  PRINT "COUNTERCLOCKWISE AROUND YOUR PLANET.  DON'T FORGET THAT"
  gameMessage( "COUNTERCLOCKWISE AROUND YOUR PLANET.  DON'T FORGET THAT");
  # 200  PRINT "WITHOUT SUFFICIENT POWER THE ROMULAN SHIP'S ALTITUDE"
  gameMessage( "WITHOUT SUFFICIENT POWER THE ROMULAN SHIP'S ALTITUDE");
  # 210  PRINT "AND ORBITAL RATE WILL REMAIN CONSTANT."
  gameMessage( "AND ORBITAL RATE WILL REMAIN CONSTANT.");
  # 220  PRINT 
  blank();
  # 230  PRINT "GOOD LUCK.  THE FEDERATION IS COUNTING ON YOU."
  gameMessage( "GOOD LUCK.  THE FEDERATION IS COUNTING ON YOU.");
  my $out = { "anchor" => "goto270" }; return $out;
}

sub goto270 {
  # 270  A=INT(360*RND(1))
  $state->{"A"} = int(360*rand());
  # 280  D=INT(200*RND(1)+200)
  $state->{"D"} = int(200*rand()+200);
  # 290  R=INT(20*RND(1)+10)
  $state->{"R"} = int(20*rand()+10);
  # 300  H=0
  $state->{"H"} = 0;
  my $out = { "anchor" => "goto310" }; return $out;
}

sub goto310 {
  # 310  IF H=7 THEN 490
  if ( $state->{"H"} == 7 ) { my $out = { "anchor" => "goto490" }; return $out; }
  # 320  H=H+1
  $state->{"H"} = $state->{"H"}+1;
  # 325  PRINT
  blank();
  # 326  PRINT
  blank();
  # 330  PRINT "THIS IS HOUR";H;", AT WHAT ANGLE DO YOU WISH TO SEND"
  gameMessage( "THIS IS HOUR ".$state->{"H"}.", AT WHAT ANGLE DO YOU WISH TO SEND");
  # 335  PRINT "YOUR PHOTON BOMB";
  gameMessage( "YOUR PHOTON BOMB");
  # 340  INPUT A1
  my @argv = ("number");
  my $out = {
    "anchor" => "goto340postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto340postinput {
  $state->{"A1"} = $input->{"action"}[0];
  # 350  PRINT "HOW FAR OUT DO YOU WISH TO DETONATE IT";
  gameMessage( "HOW FAR OUT DO YOU WISH TO DETONATE IT");
  # 360  INPUT D1
  my @argv = ("number");
  my $out = {
    "anchor" => "goto360postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto360postinput {
  $state->{"D1"} = $input->{"action"}[0];
  # 365  PRINT
  blank();
  # 366  PRINT
  blank();
  # 370  A=A+R
  $state->{"A"} = $state->{"A"}+$state->{"R"};
  # 380  IF A<360 THEN 400
  if ( $state->{"A"} < 360 ) { my $out = { "anchor" => "goto400" }; return $out; }
  # 390  A=A-360
  $state->{"A"} = $state->{"A"}-360;
  my $out = { "anchor" => "goto400" }; return $out;
}

sub goto400 {
  # 400  T=ABS(A-A1)
  $state->{"T"} = abs($state->{"A"}-$state->{"A1"});
  # 410  IF T<180 THEN 430
  if ( $state->{"T"} < 180 ) { my $out = { "anchor" => "goto430" }; return $out; }
  # 420  T=360-T
  $state->{"T"} = 360-$state->{"T"};
  my $out = { "anchor" => "goto430" }; return $out;
}

sub goto430 {
  # 430  C=SQR(D*D+D1*D1-2*D*D1*COS(T*3.14159/180))
  $state->{"C"} = sqrt($state->{"D"}*$state->{"D"}+$state->{"D1"}*$state->{"D1"}-2*$state->{"D"}*$state->{"D1"}*cos($state->{"T"}*3.14159/180));
  # 440  PRINT "YOUR PHOTON BOMB EXPLODED";C;"*10^2 MILES FROM THE"
  gameMessage( "YOUR PHOTON BOMB EXPLODED ".$state->{"C"}."*10^2 MILES FROM THE");
  # 445  PRINT "ROMULAN SHIP."
  gameMessage( "ROMULAN SHIP.");
  # 450  IF C<=50 THEN 470
  if ( $state->{"C"} <= 50 ) { my $out = { "anchor" => "goto470" }; return $out; }
  # 460  GOTO 310
  my $out = { "anchor" => "goto310" }; return $out;
}

sub goto470 {
  # 470  PRINT "YOU HAVE SUCCESFULLY COMPLETED YOUR MISSION."
  gameMessage( "YOU HAVE SUCCESFULLY COMPLETED YOUR MISSION.");
  # 480  GOTO 500
  my $out = { "anchor" => "goto500" }; return $out;
}

sub goto490 {
  # 490  PRINT "YOU HAVE ALLOWED THE ROMULANS TO ESCAPE."
  gameMessage( "YOU HAVE ALLOWED THE ROMULANS TO ESCAPE.");
  my $out = { "anchor" => "goto500" }; return $out;
}

sub goto500 {
  # 500  PRINT "ANOTHER ROMULAN SHIP HAS GONE INTO ORBIT."
  gameMessage( "ANOTHER ROMULAN SHIP HAS GONE INTO ORBIT.");
  # 510  PRINT "DO YOU WISH TO TRY TO DESTROY IT";
  gameMessage( "DO YOU WISH TO TRY TO DESTROY IT");
  # 520  INPUT C$
  my $out = {
    "anchor" => "goto520postinput",
    "echo" => " > ",
    "hook" => "reply"
  };
  return $out;
}

sub goto520postinput {
  $state->{"Cs"} = $input->{"action"}[0];
  # 530  IF C$="YES" THEN 270
  if ( $state->{"Cs"} eq "YES" ) { my $out = { "anchor" => "goto270" }; return $out; }
  # 540  PRINT "GOOD BYE."
  gameMessage( "GOOD BYE.");
  # 999  END
  my $out = { "anchor" => "quit_game" }; return $out;
}

1;

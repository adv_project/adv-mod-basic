#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

  #(<<<GOTO>> ) 500, 285, 440, 670, 660, 720, 710
  #(<<<GOSUB>> ) 610

sub initialize {
  my $out = { "anchor" => "goto10" }; return $out;
}

sub goto10 {
  # 10  PRINT TAB(33);"HURKLE"
  gameMessageTab(33, "HURKLE");
  # 20  PRINT TAB(15);"CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY"
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  # 30  PRINT
  blank();
  #(30) PRINT
  blank();
  #(30) PRINT
  blank();
  # 110  N=5
  $state->{"N"} = 5;
  # 120  G=10
  $state->{"G"} = 10;
  # 210  PRINT
  blank();
  # 220  PRINT "A HURKLE IS HIDING ON A";G;"BY";G;"GRID. HOMEBASE"
  gameMessage( "A HURKLE IS HIDING ON A ".$state->{"G"}." BY ".$state->{"G"}." GRID. HOMEBASE");
  # 230  PRINT "ON THE GRID IS POINT 0,0 IN THE SOUTHWEST CORNER,"
  gameMessage( "ON THE GRID IS POINT 0,0 IN THE SOUTHWEST CORNER,");
  # 235  PRINT "AND ANY POINT ON THE GRID IS DESIGNATED BY A"
  gameMessage( "AND ANY POINT ON THE GRID IS DESIGNATED BY A");
  # 240  PRINT "PAIR OF WHOLE NUMBERS SEPERATED BY A COMMA. THE FIRST"
  gameMessage( "PAIR OF WHOLE NUMBERS SEPERATED BY A COMMA. THE FIRST");
  # 245  PRINT "NUMBER IS THE HORIZONTAL POSITION AND THE SECOND NUMBER"
  gameMessage( "NUMBER IS THE HORIZONTAL POSITION AND THE SECOND NUMBER");
  # 246  PRINT "IS THE VERTICAL POSITION. YOU MUST TRY TO"
  gameMessage( "IS THE VERTICAL POSITION. YOU MUST TRY TO");
  # 250  PRINT "GUESS THE HURKLE'S GRIDPOINT. YOU GET";N;"TRIES."
  gameMessage( "GUESS THE HURKLE'S GRIDPOINT. YOU GET ".$state->{"N"}." TRIES.");
  # 260  PRINT "AFTER EACH TRY, I WILL TELL YOU THE APPROXIMATE"
  gameMessage( "AFTER EACH TRY, I WILL TELL YOU THE APPROXIMATE");
  # 270  PRINT "DIRECTION TO GO TO LOOK FOR THE HURKLE."
  gameMessage( "DIRECTION TO GO TO LOOK FOR THE HURKLE.");
  # 280  PRINT
  blank();
  my $out = { "anchor" => "goto285" }; return $out;
}

sub goto285 {
  # 285  A=INT(G*RND(1))
  $state->{"A"} = int($state->{"G"}*rand());
  # 286  B=INT(G*RND(1))
  $state->{"B"} = int($state->{"G"}*rand());
  # 310  FOR K=1 TO N
  $state->{"K"} = 0;
  my $out = { "anchor" => "nextK" }; return $out;
}

sub nextK {
  $state->{"K"} = $state->{"K"}+1;
  if ($state->{"K"} > $state->{"N"}) {
    $state->{"K"} = $state->{"N"};
    my $out = { "anchor" => "postnextK" }; return $out;
  }
  # 320  PRINT "GUESS #";K;
  gameMessage( "GUESS #".$state->{"K"});
  # 330  INPUT X,Y
  my @argv = ("integer", "integer");
  my $out = {
    "anchor" => "goto330postinput",
    "echo" => " > ",
    "argc" => 2,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto330postinput {
  $state->{"X"} = $input->{"action"}[0];
  $state->{"Y"} = $input->{"action"}[1];
  # 340  IF ABS(X-A)+ABS(Y-B)=0 THEN 500
  if ( abs($state->{"X"}-$state->{"A"})+abs($state->{"Y"}-$state->{"B"}) == 0 ) { my $out = { "anchor" => "goto500" }; return $out; }
  # 350  REM PRINT INFO
  # 360  GOSUB 610
  gosub610();
  # 370  PRINT
  blank();
  # 380  NEXT K
  my $out = { "anchor" => "nextK" }; return $out;
}

sub postnextK {
  # 410  PRINT
  blank();
  # 420  PRINT "SORRY, THAT'S";N;"GUESSES."
  gameMessage( "SORRY, THAT'S ".$state->{"N"}." GUESSES.");
  # 430  PRINT "THE HURKLE IS AT ";A;",";B
  gameMessage( "THE HURKLE IS AT ".$state->{"A"}.",".$state->{"B"});
  my $out = { "anchor" => "goto440" }; return $out;
}

sub goto440 {
  # 440  PRINT
  blank();
  # 450  PRINT "LET'S PLAY AGAIN, HURKLE IS HIDING."
  gameMessage( "LET'S PLAY AGAIN, HURKLE IS HIDING.");
  # 460  PRINT
  blank();
  # 470  GOTO 285
  my $out = { "anchor" => "goto285" }; return $out;
}

sub goto500 {
  # 500  REM
  # 510  PRINT
  blank();
  # 520  PRINT "YOU FOUND HIM IN";K;"GUESSES!"
  gameMessage( "YOU FOUND HIM IN ".$state->{"K"}." GUESSES!");
  # 540  GOTO 440
  my $out = { "anchor" => "goto440" }; return $out;
}

sub gosub610 {
  # 610  PRINT "GO ";
  # 620  IF Y=B THEN 670
  # 630  IF Y<B THEN 660
  my $_string = "";
  if ($state->{"Y"} < $state->{"B"}) { $_string = "NORTH"; }
  if ($state->{"Y"} > $state->{"B"}) { $_string = "SOUTH"; }
  # 640  PRINT "SOUTH";
  # 650  GOTO 670
  # 660  PRINT "NORTH";
  # 670  IF X=A THEN 720
  # 680  IF X<A THEN 710
  if ($state->{"X"} < $state->{"A"}) { $_string = $_string."EAST"; }
  if ($state->{"X"} > $state->{"A"}) { $_string = $_string."WEST"; }
  # 690  PRINT "WEST";
  # 700  GOTO 720
  # 710  PRINT "EAST";
  gameMessage("GO ".$_string);
  # 720  PRINT
  #blank();
  # 730  RETURN
  return;
}

sub goto999 {
  # 999  END
  my $out = { "anchor" => "quit_game" }; return $out;
}

1;

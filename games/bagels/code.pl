#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

  #(<<<GOTO>> ) 150, 200, 160, 630, 300, 320, 230, 650, 390, 410, 440, 460, 490, 680, 545, 580, 600, 700, 750
  #(<<<GOSUB>> ) 

@_bagels = ();

sub initialize {
  my $out = { "anchor" => "goto5" }; return $out;
}

sub goto5 {
  # 5  PRINT TAB(33);"BAGELS"
  gameMessageTab(33, "BAGELS");
  # 10  PRINT TAB(15);"CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY"
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  #(10) PRINT
  blank();
  #(10) PRINT
  blank();
  # 15  REM *** BAGLES NUMBER GUESSING GAME
  # 20  REM *** ORIGINAL SOURCE UNKNOWN BUT SUSPECTED TO BE
  # 25  REM *** LAWRENCE HALL OF SCIENCE, U.C. BERKELY
  # 30  DIM A1(6),A(3),B(3)
  my @a = ();
  $state->{"A1"} = dclone(\@a);
  $state->{"A"} = dclone(\@a);
  $state->{"B"} = dclone(\@a);
  # 40  Y=0
  $state->{"Y"} = 0;
  #(40) T=255
  $state->{"T"} = 255;
  # 50  PRINT
  blank();
  #(50) PRINT
  blank();
  #(50) PRINT
  blank();
  # 70  INPUT "WOULD YOU LIKE THE RULES (YES OR NO)";A$
  gameMessage("WOULD YOU LIKE THE RULES (YES OR NO)");
  my $out = {
    "anchor" => "goto70postinput",
    "echo" => " > ",
    "argc" => 1,
    "hook" => "reply"
  };
  return $out;
}

sub goto70postinput {
  $state->{"As"} = $input->{"action"}[0];
  # 90  IF LEFT$(A$,1)="N" THEN 150
  if ( $state->{"As"} =~ /^N/ ) { my $out = { "anchor" => "goto150" }; return $out; }
  # 100  PRINT
  blank();
  #(100) PRINT "I AM THINKING OF A THREE-DIGIT NUMBER.  TRY TO GUESS"
  gameMessage( "I AM THINKING OF A THREE-DIGIT NUMBER.  TRY TO GUESS");
  # 110  PRINT "MY NUMBER AND I WILL GIVE YOU CLUES AS FOLLOWS
  gameMessage( "MY NUMBER AND I WILL GIVE YOU CLUES AS FOLLOWS:");
  # 120  PRINT "   PICO   - ONE DIGIT CORRECT BUT IN THE WRONG POSITION"
  gameMessage( "   PICO   - ONE DIGIT CORRECT BUT IN THE WRONG POSITION");
  # 130  PRINT "   FERMI  - ONE DIGIT CORRECT AND IN THE RIGHT POSITION"
  gameMessage( "   FERMI  - ONE DIGIT CORRECT AND IN THE RIGHT POSITION");
  # 140  PRINT "   BAGELS - NO DIGITS CORRECT"
  gameMessage( "   BAGELS - NO DIGITS CORRECT");
  my $out = { "anchor" => "goto150" }; return $out;
}

sub goto150 {
  # 150  FOR I=1 TO 3
  $state->{"I"} = 0;
  my $out = { "anchor" => "nextI" }; return $out;
}

sub nextI {
  $state->{"I"} = $state->{"I"}+1;
  if ($state->{"I"} > 3) {
    $state->{"I"} = 3;
    my $out = { "anchor" => "postnextI" }; return $out;
  }
  my $out = { "anchor" => "goto160" }; return $out;
}

sub goto160 {
  # 160  A(I)=INT(10*RND(1))
  $state->{"A"}[$state->{"I"}] = int(10*rand());
  # 165  IF I-1=0 THEN 200
  if ( $state->{"I"}-1 == 0 ) { my $out = { "anchor" => "goto200" }; return $out; }
  # 170  FOR J=1 TO I-1
  $state->{"J"} = 0;
  $state->{"_I-1_"} = $state->{"I"}-1;
  my $out = { "anchor" => "nextJ" }; return $out;
}

sub nextJ {
  $state->{"J"} = $state->{"J"}+1;
  if ($state->{"J"} > $state->{"_I-1_"}) {
    $state->{"J"} = $state->{"_I-1_"};
    my $out = { "anchor" => "goto200" }; return $out;
  }
  # 180  IF A(I)=A(J) THEN 160
  if ( $state->{"A"}[$state->{"I"}] == $state->{"A"}[$state->{"J"}] ) { my $out = { "anchor" => "goto160" }; return $out; }
  # 190  NEXT J
  my $out = { "anchor" => "nextJ" }; return $out;
}

sub goto200 {
  # 200  NEXT I
  my $out = { "anchor" => "nextI" }; return $out;
}

sub postnextI {
  # 210  PRINT
  blank();
  #(210) PRINT "O.K.  I HAVE A NUMBER IN MIND."
  gameMessage( "O.K.  I HAVE A NUMBER IN MIND.");
  # 220  FOR I=1 TO 20
  $state->{"I"} = 0;
  my $out = { "anchor" => "nextI_2" }; return $out;
}

sub nextI_2 {
  $state->{"I"} = $state->{"I"}+1;
  if ($state->{"I"} > 20) {
    $state->{"I"} = 20;
    my $out = { "anchor" => "postnextI_2" }; return $out;
  }
  my $out = { "anchor" => "goto230" }; return $out;
}

sub goto230 {
  # 230  PRINT "GUESS #";I,
  gameMessage( "GUESS #".$state->{"I"});
  # 240  INPUT A$
  my $out = {
    "anchor" => "goto240postinput",
    "echo" => " > ",
    "argc" => 1,
    "hook" => "reply"
  };
  return $out;
}

sub goto240postinput {
  $state->{"As"} = $input->{"action"}[0]+0;
  # 245  IF LEN(A$)<>3 THEN 630
#  if (($state->{"As"} < 100) or ($state->{"As"} > 999)) { my $out = { "anchor" => "goto630" }; return $out; }
  # 250  FOR Z=1 TO 3
  #(250) A1(Z)=ASC(MID$(A$,Z,1))
  #(250) NEXT Z
  my $mten = $state->{"As"} % 10;
  my $mhun = ($state->{"As"} % 100); 
  my $mint = $state->{"As"}; 
  $state->{"A1"}[3] = $mten;
  $state->{"A1"}[2] = ($mhun-$mten)/10;
  $state->{"A1"}[1] = ($mint-$mhun)/100;
#  # 260  FOR J=1 TO 3
#  # 270  IF A1(J)<48 THEN 300
#  # 280  IF A1(J)>57 THEN 300
#  # 285  B(J)=A1(J)-48
#  # 290  NEXT J
  $state->{"B"}[3] = $state->{"A1"}[3];
  $state->{"B"}[2] = $state->{"A1"}[2];
  $state->{"B"}[1] = $state->{"A1"}[1];
  # 295  GOTO 320
  my $out = { "anchor" => "goto320" }; return $out;
}

#sub goto300 {
#  # 300  PRINT "WHAT?"
#  gameMessage( "WHAT?");
#  # 310  GOTO 230
#  my $out = { "anchor" => "goto230" }; return $out;
#}

sub goto320 {
  # 320  IF B(1)=B(2) THEN 650
  if ( $state->{"B"}[1]+0 == $state->{"B"}[2]+0 ) { my $out = { "anchor" => "goto650" }; return $out; }
  # 330  IF B(2)=B(3) THEN 650
  if ( $state->{"B"}[2]+0 == $state->{"B"}[3]+0 ) { my $out = { "anchor" => "goto650" }; return $out; }
  # 340  IF B(3)=B(1) THEN 650
  if ( $state->{"B"}[1]+0 == $state->{"B"}[3]+0 ) { my $out = { "anchor" => "goto650" }; return $out; }
  # 350  C=0
  $state->{"C"} = 0;
  #(350) D=0
  $state->{"D"} = 0;
  # 360  FOR J=1 TO 2
  $state->{"J"} = 0;
  my $out = { "anchor" => "nextJ_2" }; return $out;
}

sub nextJ_2 {
  $state->{"J"} = $state->{"J"}+1;
  if ($state->{"J"} > 2) {
    $state->{"J"} = 2;
    my $out = { "anchor" => "postnextJ_2" }; return $out;
  }
  # 370  IF A(J)<>B(J+1) THEN 390
  if ( $state->{"A"}[$state->{"J"}] != $state->{"B"}[$state->{"J"}+1] ) { my $out = { "anchor" => "goto390" }; return $out; }
  # 380  C=C+1
  $state->{"C"} = $state->{"C"}+1;
  my $out = { "anchor" => "goto390" }; return $out;
}

sub goto390 {
  # 390  IF A(J+1)<>B(J) THEN 410
  if ( $state->{"A"}[$state->{"J"}+1] != $state->{"B"}[$state->{"J"}] ) { my $out = { "anchor" => "goto410" }; return $out; }
  # 400  C=C+1
  $state->{"C"} = $state->{"C"}+1;
  my $out = { "anchor" => "goto410" }; return $out;
}

sub goto410 {
  # 410  NEXT J
  my $out = { "anchor" => "nextJ_2" }; return $out;
}

sub postnextJ_2 {
  # 420  IF A(1)<>B(3) THEN 440
  if ( $state->{"A"}[1] != $state->{"B"}[3] ) { my $out = { "anchor" => "goto440" }; return $out; }
  # 430  C=C+1
  $state->{"C"} = $state->{"C"}+1;
  my $out = { "anchor" => "goto440" }; return $out;
}

sub goto440 {
  # 440  IF A(3)<>B(1) THEN 460
  if ( $state->{"A"}[3] != $state->{"B"}[1] ) { my $out = { "anchor" => "goto460" }; return $out; }
  # 450  C=C+1
  $state->{"C"} = $state->{"C"}+1;
  my $out = { "anchor" => "goto460" }; return $out;
}

sub goto460 {
  # 460  FOR J=1 TO 3
  $state->{"J"} = 0;
  my $out = { "anchor" => "nextJ_3" }; return $out;
}

sub nextJ_3 {
  $state->{"J"} = $state->{"J"}+1;
  if ($state->{"J"} > 3) {
    $state->{"J"} = 3;
    my $out = { "anchor" => "postnextJ_3" }; return $out;
  }
  # 470  IF A(J)<>B(J) THEN 490
  if ( $state->{"A"}[$state->{"J"}] != $state->{"B"}[$state->{"J"}] ) { my $out = { "anchor" => "goto490" }; return $out; }
  # 480  D=D+1
  $state->{"D"} = $state->{"D"}+1;
  my $out = { "anchor" => "goto490" }; return $out;
}

sub goto490 {
  # 490  NEXT J
  my $out = { "anchor" => "nextJ_3" }; return $out;
}

sub postnextJ_3 {
  # 500  IF D=3 THEN 680
  if ( $state->{"D"} == 3 ) { my $out = { "anchor" => "goto680" }; return $out; }
  # 505  IF C=0 THEN 545
  if ( $state->{"C"} == 0 ) { my $out = { "anchor" => "goto545" }; return $out; }
  # 520  FOR J=1 TO C
  $state->{"J"} = 0;
  $state->{"_C_"} = $state->{"C"};
  my $out = { "anchor" => "nextJ_4" }; return $out;
}

sub nextJ_4 {
  $state->{"J"} = $state->{"J"}+1;
  if ($state->{"J"} > $state->{"_C_"}) {
    $state->{"J"} = $state->{"_C_"};
    my $out = { "anchor" => "postnextJ_4" }; return $out;
  }
  # 530  PRINT "PICO ";
  #gameMessage( "PICO ");
  push(@_bagels, "PICO");
  # 540  NEXT J
  my $out = { "anchor" => "nextJ_4" }; return $out;
}

sub postnextJ_4 {
  my $out = { "anchor" => "goto545" }; return $out;
}

sub goto545 {
  # 545  IF D=0 THEN 580
  if ( $state->{"D"} == 0 ) { my $out = { "anchor" => "goto580" }; return $out; }
  # 550  FOR J=1 TO D
  $state->{"J"} = 0;
  $state->{"_D_"} = $state->{"D"};
  my $out = { "anchor" => "nextJ_5" }; return $out;
}

sub nextJ_5 {
  $state->{"J"} = $state->{"J"}+1;
  if ($state->{"J"} > $state->{"_D_"}) {
    $state->{"J"} = $state->{"_D_"};
    my $out = { "anchor" => "postnextJ_5" }; return $out;
  }
  # 560  PRINT "FERMI ";
  #gameMessage( "FERMI ");
  push(@_bagels, "FERMI");
  # 570  NEXT J
  my $out = { "anchor" => "nextJ_5" }; return $out;
}

sub postnextJ_5 {
  my $out = { "anchor" => "goto580" }; return $out;
}

sub goto580 {
  # 580  IF C+D<>0 THEN 600
  if ( $state->{"C"}+$state->{"D"} != 0 ) { my $out = { "anchor" => "goto600" }; return $out; }
  # 590  PRINT "BAGELS";
  #gameMessage( "BAGELS");
  push(@_bagels, "BAGELS");
  my $out = { "anchor" => "goto600" }; return $out;
}

sub goto600 {
  # 600  PRINT
  gameMessage(join(' ', @_bagels));
  blank();
  # 605  NEXT I
  my $out = { "anchor" => "nextI_2" }; return $out;
}

sub postnextI_2 {
  # 610  PRINT "OH WELL."
  gameMessage( "OH WELL.");
  # 615  PRINT "THAT'S TWNETY GUESSES.  MY NUMBER WAS ";100*A(1)+10*A(2)+A(3)
  my $num = 100*$state->{"A"}[1] + 10*$state->{"A"}[2] + $state->{"A"}[3];
  gameMessage( "THAT'S TWENTY GUESSES.  MY NUMBER WAS".$num); 
  # 620  GOTO 700
  my $out = { "anchor" => "goto700" }; return $out;
}

sub goto630 {
  # 630  PRINT "TRY GUESSING A THREE-DIGIT NUMBER."
  gameMessage( "TRY GUESSING A THREE-DIGIT NUMBER.");
  #(630) GOTO 230
  my $out = { "anchor" => "goto230" }; return $out;
}

sub goto650 {
  # 650  PRINT "OH, I FORGOT TO TELL YOU THAT THE NUMBER I HAVE IN MIND"
  gameMessage( "OH, I FORGOT TO TELL YOU THAT THE NUMBER I HAVE IN MIND");
  # 660  PRINT "HAS NO TWO DIGITS THE SAME."
  gameMessage( "HAS NO TWO DIGITS THE SAME.");
  #(660) GOTO 230
  my $out = { "anchor" => "goto230" }; return $out;
}

sub goto680 {
  # 680  PRINT "YOU GOT IT!!!"
  gameMessage( "YOU GOT IT!!!");
  #(680) PRINT
  blank();
  # 690  Y=Y+1
  $state->{"Y"} = $state->{"Y"}+1;
  my $out = { "anchor" => "goto700" }; return $out;
}

sub goto700 {
  # 700  INPUT "PLAY AGAIN (YES OR NO)";A$
  gameMessage("PLAY AGAIN (YES OR NO)");
  my $out = {
    "anchor" => "goto700postinput",
    "echo" => " > ",
    "argc" => 1,
    "hook" => "reply"
  };
  return $out;
}

sub goto700postinput {
  $state->{"As"} = $input->{"action"}[0];
  # 720  IF LEFT$(A$,1)="YES" THEN 150
  if ( $state->{"As"} =~ /^YES/ ) { my $out = { "anchor" => "goto150" }; return $out; }
  # 730  IF Y=0 THEN 750
  if ( $state->{"Y"} == 0 ) { my $out = { "anchor" => "goto750" }; return $out; }
  # 740  PRINT
  blank();
  #(740) PRINT "A";Y;"POINT BAGELS BUFF!!"
  gameMessage( "A ".$state->{"Y"}." POINT BAGELS BUFF!!");
  my $out = { "anchor" => "goto750" }; return $out;
}

sub goto750 {
  # 750  PRINT "HOPE YOU HAD FUN.  BYE."
  gameMessage( "HOPE YOU HAD FUN.  BYE.");
  # 999  END
  my $out = { "anchor" => "quit_game" }; return $out;
}

1;

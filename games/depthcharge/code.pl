#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

use Storable qw(dclone);

sub initialize {
  my $out = { "anchor" => "goto2" };
  return $out;
}

sub goto2 {
  gameMessageTab(30, "DEPTH CHARGE");
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  blank(3);
  gameMessage("DIMENSION OF SEARCH AREA");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto20",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto20 {
  # TODO: send the expected data type in the anchor and get the
  # engine to check and repeat until it matches.
  $state->{"G"} = $input->{"action"}[0];
  blank(1);
  $state->{"N"} = int( log($state->{"G"}) / log(2) + 1 );
  gameMessage("YOU ARE THE CAPTAIN OF THE DESTROYER USS COMPUTER");
  gameMessage("AN ENEMY SUB HAS BEEN CAUSING YOU TROUBLE.  YOUR");
  gameMessage("MISSION IS TO DESTROY IT.  YOU HAVE " . $state->{"N"} . " SHOTS.");
  gameMessage("SPECIFY DEPTH CHARGE EXPLOSION POINT WITH A");
  gameMessage("TRIO OF NUMBERS -- THE FIRST TWO ARE THE");
  gameMessage("SURFACE COORDINATES; THE THIRD IS THE DEPTH.");
  my $out = { "anchor" => "goto100" };
  return $out;
}

sub goto100 {
  blank(1);
  gameMessage("GOOD LUCK !");
  blank(1);
  $state->{"A"} = int( $state->{"G"} * rand() );
  $state->{"B"} = int( $state->{"G"} * rand() );
  $state->{"C"} = int( $state->{"G"} * rand() );
  $state->{"D"} = 0;
  my $out = { "anchor" => "goto120" };
  return $out;
}

sub goto120 {
  $state->{"D"} = int( $state->{"D"} + 1 );
  if ( $state->{"D"} > $state->{"N"} ) {
    my $out = { "anchor" => "goto200" };
    return $out;
  }
  blank(1);
  gameMessage("TRIAL #" . $state->{"D"});
  my @argv = ("integer", "integer", "integer");
  my $out = {
    "anchor" => "goto130",
    "echo" => " > ",
    "argc" => 3,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto130 {
  $state->{"X"} = $input->{"action"}[0];
  $state->{"Y"} = $input->{"action"}[1];
  $state->{"Z"} = $input->{"action"}[2];
  my $xdiff = abs($state->{"X"} - $state->{"A"});
  my $ydiff = abs($state->{"Y"} - $state->{"B"});
  my $zdiff = abs($state->{"Z"} - $state->{"C"});
  if ( ($xdiff + $ydiff + $zdiff ) == 0 ) {
    my $out = { "anchor" => "goto300" };
    return $out;
  }
  gosub500();
  blank(1);
  my $out = { "anchor" => "goto120" };
  return $out;
}

sub goto200 {
  blank(1);
  gameMessage("YOU HAVE BEEN TORPEDOED!  ABANDON SHIP!");
  gameMessage("THE SUBMARINE WAS AT " . $state->{"A"} . " , " . $state->{"B"} . " , " . $state->{"C"});
  my $out = { "anchor" => "goto400" };
  return $out;
}

sub goto300 {
  blank(1);
  gameMessage("B O O M ! ! YOU FOUND IT IN " . $state->{"D"} . " TRIES!");
  my $out = { "anchor" => "goto400" };
  return $out;
}

sub goto400 {
  blank(2);
  gameMessage("ANOTHER GAME (Y OR N)");
  my $out = {
    "anchor" => "goto410",
    "echo" => " > ",
    "argc" => 1,
    "hook" => "reply"
  };
}

sub goto410 {
  if ( $input->{"action"}[0] eq "Y" ) {
    my $out = { "anchor" => "goto100" };
    return $out;
  }
  gameMessage("OK.  HOPE YOU ENJOYED YOURSELF.");
  my $out = { "anchor" => "quit_game" };
  return $out;
}

sub gosub500 {
  my $string = "SONAR REPORTS SHOT WAS ";
  if ( $state->{"Y"} > $state->{"B"} ) { $string = $string . "NORTH"; }
  if ( $state->{"Y"} < $state->{"B"} ) { $string = $string . "SOUTH"; }
  if ( $state->{"X"} > $state->{"A"} ) { $string = $string . "EAST"; }
  if ( $state->{"X"} < $state->{"A"} ) { $string = $string . "WEST"; }
  if (
      ( $state->{"Y"} != $state->{"B"} )
    or
      ( $state->{"X"} != $state->{"A"} )
     )  { $string = $string . " AND "; }
  if ( $state->{"Z"} > $state->{"C"} ) { $string = $string . "TOO LOW."; }
  if ( $state->{"Z"} < $state->{"C"} ) { $string = $string . "TOO HIGH."; }
  if ( $state->{"Z"} == $state->{"C"} ) { $string = $string . "DEPTH OK."; }
  gameMessage($string);
  return;
}

1;

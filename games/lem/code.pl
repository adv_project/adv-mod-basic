#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

$__lemmsg__ = '';

sub initialize {
  my $out = { "anchor" => "goto2" }; return $out;
}

#2 PRINT TAB(34);"LEM"
#4 PRINT TAB(15);"CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY"
#7 REM ROCKT2 IS AN INTERACTIVE GAME THAT SIMULATES A LUNAR
#8 REM LANDING IS SIMILAR TO THAT OF THE APOLLO PROGRAM.
#9 REM THERE IS ABSOLUTELY NO CHANCE INVOLVED
#10 Z$="GO"
#15 B1=1
sub goto2 {
  gameMessageTab(34, "LEM");
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  $state->{"Zs"} = "GO";
  $state->{"B1"} = 1;
  my $out = { "anchor" => "goto20" }; return $out;
}

#20 M=17.95
#25 F1=5.25
#30 N=7.5
#35 R0=926
#40 V0=1.29
#45 T=0
#50 H0=60
#55 R=R0+H0
#60 A=-3.425
#65 R1=0
#70 A1=8.84361E-04
#75 R3=0
#80 A3=0
#85 M1=7.45
#90 M0=M1
#95 B=750
#100 T1=0
#105 F=0
#110 P=0
#115 N=1
#120 M2=0
#125 S=0
#130 C=0
#135 IF Z$="YES" THEN 1150
#140 PRINT
#145 PRINT "LUNAR LANDING SIMULATION"
#150 PRINT
#155 PRINT "HAVE YOU FLOWN AN APOLLO/LEM MISSION BEFORE";
sub goto20 {
  $state->{"M"} = 17.95;
  $state->{"F1"} = 5.25;
  $state->{"N"} = 7.5;
  $state->{"R0"} = 926;
  $state->{"V0"} = 1.29;
  $state->{"T"} = 0;
  $state->{"H0"} = 60;
  $state->{"R"} = $state->{"R0"} + $state->{"H0"};
  $state->{"A"} = -3.425;
  $state->{"R1"} = 0;
  $state->{"A1"} = 8.84361E-04;
  $state->{"R3"} = 0;
  $state->{"A3"} = 0;
  $state->{"M1"} = 7.45;
  $state->{"M0"} = $state->{"M1"};
  $state->{"B"} = 750;
  $state->{"T1"} = 0;
  $state->{"F"} = 0;
  $state->{"P"} = 0;
  $state->{"N"} = 1;
  $state->{"M2"} = 0;
  $state->{"S"} = 0;
  $state->{"C"} = 0;
  if ($state->{"Zs"} eq "YES")
    { my $out = { "anchor" => "goto1150" }; return $out; }
  blank();
  gameMessage("LUNAR LANDING SIMULATION");
  blank();
  $__lemmsg__ = "HAVE YOU FLOWN AN APOLLO/LEM MISSION BEFORE";
  my $out = { "anchor" => "goto160" }; return $out;
}

#160 PRINT " (YES OR NO)";
#165 INPUT Q$
sub goto160 {
  $__lemmsg__ = $__lemmsg__." (YES OR NO)";
  gameMessage($__lemmsg__); $__lemmsg__ = '';
  my $out = {
    "anchor" => "goto170",
    "echo" => " > ",
    "hook" => "reply"
  };
  return $out;
}

#170 IF Q$="YES" THEN 190
#175 IF Q$="NO" THEN 205
#180 PRINT "JUST ANSWER THE QUESTION, PLEASE, ";
#185 GOTO 160
sub goto170 {
  $state->{"Qs"} = $input->{"action"}[0];
  if ($state->{"Qs"} eq "YES")
    { my $out = { "anchor" => "goto190" }; return $out; }
  if ($state->{"Qs"} eq "NO")
    { my $out = { "anchor" => "goto205" }; return $out; }
  $__lemmsg__ = "JUST ANSWER THE QUESTION, PLEASE, ";
  my $out = { "anchor" => "goto160" }; return $out;
}

#190 PRINT
#195 PRINT "INPUT MEASUREMENT OPTION NUMBER";
#200 GOTO 225
sub goto190 {
  blank();
  gameMessage("INPUT MEASUREMENT OPTION NUMBER");
  my $out = { "anchor" => "goto225" }; return $out;
}

#205 PRINT
#210 PRINT "WHICH SYSTEM OF MEASUREMENT DO YOU PREFER?"
#215 PRINT " 1=METRIC     0=ENGLISH"
sub goto205 {
  blank();
  gameMessage("WHICH SYSTEM OF MEASUREMENT DO YOU PREFER?");
  gameMessage(" 1=METRIC     0=ENGLISH");
  my $out = { "anchor" => "goto220" }; return $out;
}

#220 PRINT "ENTER THE APPROPRIATE NUMBER";
sub goto220 {
  gameMessage("ENTER THE APPROPRIATE NUMBER");
  my $out = { "anchor" => "goto225" }; return $out;
}

#225 INPUT K
sub goto225 {
  my $out = {
    "anchor" => "goto230",
    "echo" => " > ",
    "hook" => "reply"
  };
  return $out;
}

#230 PRINT
#235 IF K=0 THEN 280
#240 IF K=1 THEN 250
#245 GOTO 220
sub goto230 {
  $state->{"K"} = $input->{"action"}[0];
  blank();
  if ($state->{"K"} == 0)
    { my $out = { "anchor" => "goto280" }; return $out; }
  if ($state->{"K"} == 1)
    { my $out = { "anchor" => "goto250" }; return $out; }
  my $out = { "anchor" => "goto220" }; return $out;
}

#250 Z=1852.8
#255 M$="METERS"
#260 G3=3.6
#265 N$=" KILOMETERS"
#270 G5=1000
#275 GOTO 305
sub goto250 {
  $state->{"Z"} = 1852.8;
  $state->{"Ms"} = "METERS";
  $state->{"G3"} = 3.6;
  $state->{"Ns"} = " KILOMETERS";
  $state->{"G5"} = 1000;
  my $out = { "anchor" => "goto305" }; return $out;
}

#280 Z=6080
#285 M$="FEET"
#290 G3=.592
#295 N$="N.MILES"
#300 G5=Z
sub goto280 {
  $state->{"Z"} = 6080;
  $state->{"Ms"} = "FEET";
  $state->{"G3"} = 0.592;
  $state->{"Ns"} = "N.MILES";
  $state->{"G5"} = $state->{"Z"};
  my $out = { "anchor" => "goto305" }; return $out;
}

#305 IF B1=3 THEN 670
#310 IF Q$="YES" THEN 485
#315 PRINT
#320 PRINT "  YOU ARE ON A LUNAR LANDING MISSION.  AS THE PILOT OF"
#325 PRINT "THE LUNAR EXCURSION MODULE, YOU WILL BE EXPECTED TO"
#330 PRINT "GIVE CERTAIN COMMANDS TO THE MODULE NAVIGATION SYSTEM."
#335 PRINT "THE ON-BOARD COMPUTER WILL GIVE A RUNNING ACCOUNT"
#340 PRINT "OF INFORMATION NEEDED TO NAVIGATE THE SHIP."
#345 PRINT
#350 PRINT
#355 PRINT "THE ATTITUDE ANGLE CALLED FOR IS DESCRIBED AS FOLLOWS."
#360 PRINT "+ OR -180 DEGREES IS DIRECTLY AWAY FROM THE MOON"
#365 PRINT "-90 DEGREES IS ON A TANGENT IN THE DIRECTION OF ORBIT"
#370 PRINT "+90 DEGREES IS ON A TANGENT FROM THE DIRECTION OF ORBIT"
#375 PRINT "0 (ZERO) DEGREES IS DIRECTLY TOWARD THE MOON"
#380 PRINT
#385 PRINT TAB(30);"-180|+180"
#390 PRINT TAB(34);"^"
#395 PRINT TAB(27);"-90 < -+- > +90"
#400 PRINT TAB(34);"!"
#405 PRINT TAB(34);"0"
#410 PRINT TAB(21);"<<<< DIRECTION OF ORBIT <<<<"
#415 PRINT
#420 PRINT TAB(20);"------ SURFACE OF MOON ------"
#425 PRINT
#430 PRINT
#435 PRINT "ALL ANGLES BETWEEN -180 AND +180 DEGREES ARE ACCEPTED."
#440 PRINT
#445 PRINT "1 FUEL UNIT = 1 SEC. AT MAX THRUST"
#450 PRINT "ANY DISCREPANCIES ARE ACCOUNTED FOR IN THE USE OF FUEL"
#455 PRINT "FOR AN ATTITUDE CHANGE."
#460 PRINT "AVAILABLE ENGINE POWER: 0 (ZERO) AND ANY VALUE BETWEEN"
#465 PRINT "10 AND 100 PERCENT."
#470 PRINT
#475 PRINT"NEGATIVE THRUST OR TIME IS PROHIBITED."
#480 PRINT
sub goto305 {
  if ($state->{"B1"} == 3)
    { my $out = { "anchor" => "goto670" }; return $out; }
  if ($state->{"Qs"} eq "YES")
    { my $out = { "anchor" => "goto485" }; return $out; }
  blank();
  gameMessage("  YOU ARE ON A LUNAR LANDING MISSION.  AS THE PILOT OF");
  gameMessage("THE LUNAR EXCURSION MODULE, YOU WILL BE EXPECTED TO");
  gameMessage("GIVE CERTAIN COMMANDS TO THE MODULE NAVIGATION SYSTEM.");
  gameMessage("THE ON-BOARD COMPUTER WILL GIVE A RUNNING ACCOUNT");
  gameMessage("OF INFORMATION NEEDED TO NAVIGATE THE SHIP.");
  blank();
  blank();
  gameMessage("THE ATTITUDE ANGLE CALLED FOR IS DESCRIBED AS FOLLOWS.");
  gameMessage("+ OR -180 DEGREES IS DIRECTLY AWAY FROM THE MOON");
  gameMessage("-90 DEGREES IS ON A TANGENT IN THE DIRECTION OF ORBIT");
  gameMessage("+90 DEGREES IS ON A TANGENT FROM THE DIRECTION OF ORBIT");
  gameMessage("0 (ZERO) DEGREES IS DIRECTLY TOWARD THE MOON");
  blank();
  gameMessageTab(30, "-180|+180");
  gameMessageTab(34, "^");
  gameMessageTab(27, "-90 < -+- > +90");
  gameMessageTab(34, "!");
  gameMessageTab(34, "0");
  gameMessageTab(21, "<<<< DIRECTION OF ORBIT <<<<");
  blank();
  gameMessageTab(20, "------ SURFACE OF MOON ------");
  blank();
  blank();
  gameMessage("ALL ANGLES BETWEEN -180 AND +180 DEGREES ARE ACCEPTED.");
  blank();
  gameMessage("1 FUEL UNIT = 1 SEC. AT MAX THRUST");
  gameMessage("ANY DISCREPANCIES ARE ACCOUNTED FOR IN THE USE OF FUEL");
  gameMessage("FOR AN ATTITUDE CHANGE.");
  gameMessage("AVAILABLE ENGINE POWER: 0 (ZERO) AND ANY VALUE BETWEEN");
  gameMessage("10 AND 100 PERCENT.");
  blank();
  gameMessage("NEGATIVE THRUST OR TIME IS PROHIBITED.");
  blank();
  my $out = { "anchor" => "goto485" }; return $out;
}

#485 PRINT
#490 PRINT "INPUT: TIME INTERVAL IN SECONDS ------ (T)"
#495 PRINT "       PERCENTAGE OF THRUST ---------- (P)"
#500 PRINT "       ATTITUDE ANGLE IN DEGREES ----- (A)"
#505 PRINT
#510 IF Q$="YES" THEN 535
#515 PRINT "FOR EXAMPLE:"
#520 PRINT "T,P,A? 10,65,-60"
#525 PRINT "TO ABORT THE MISSION AT ANY TIME, ENTER 0,0,0"
#530 PRINT
sub goto485 {
  blank();
  gameMessage("INPUT: TIME INTERVAL IN SECONDS ------ (T)");
  gameMessage("       PERCENTAGE OF THRUST ---------- (P)");
  gameMessage("       ATTITUDE ANGLE IN DEGREES ----- (A)");
  blank();
  if ($state->{"Qs"} eq "YES")
    { my $out = { "anchor" => "goto535" }; return $out; }
  gameMessage("FOR EXAMPLE:");
  gameMessage("T,P,A? 10 65 -60");
  gameMessage("TO ABORT THE MISSION AT ANY TIME, ENTER 0 0 0");
  blank();
  my $out = { "anchor" => "goto535" }; return $out;
}

#535 PRINT "OUTPUT: TOTAL TIME IN ELAPSED SECONDS"
#540 PRINT "        HEIGHT IN ";M$
#545 PRINT "        DISTANCE FROM LANDING SITE IN ";M$
#550 PRINT "        VERTICAL VELOCITY IN ";M$;"/SECOND"
#555 PRINT "        HORIZONTAL VELOCITY IN ";M$;"/SECOND"
#560 PRINT "        FUEL UNITS REMAINING"
#565 PRINT
#570 GOTO 670
sub goto535 {
  gameMessage("OUTPUT: TOTAL TIME IN ELAPSED SECONDS");
  gameMessage("        HEIGHT IN ".$state->{"Ms"});
  gameMessage("        DISTANCE FROM LANDING SITE IN ".$state->{"Ms"});
  gameMessage("        VERTICAL VELOCITY IN ".$state->{"Ms"}."/SECOND");
  gameMessage("        HORIZONTAL VELOCITY IN ".$state->{"Ms"}."/SECOND");
  gameMessage("        FUEL UNITS REMAINING");
  blank();
  my $out = { "anchor" => "goto670" }; return $out;
}

#575 PRINT
sub goto575 {
  blank();
  my $out = { "anchor" => "goto580" }; return $out;
}

#580 PRINT "T,P,A";
#585 INPUT T1,F,P
sub goto580 {
  gameMessage("T,P,A");
  my @argv = ("number", "number", "number");
  my $out = {
    "anchor" => "goto590",
    "echo" => " > ",
    "argc" => 3,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

#590 F=F/100
#595 IF T1<0 THEN 905
#600 IF T1=0 THEN 1090
#605 IF ABS(F-.05)>1 THEN 945
#610 IF ABS(F-.05)<.05 THEN 945
#615 IF ABS(P)>180 THEN 925
sub goto590 {
  $state->{"T1"} = $input->{"action"}[0];
  $state->{"F"} = $input->{"action"}[1];
  $state->{"P"} = $input->{"action"}[2];
  $state->{"F"} = $state->{"F"}/100;
  if ($state->{"T1"} < 0) { my $out = { "anchor" => "goto905" }; return $out; }
  if ($state->{"T1"} == 0) { my $out = { "anchor" => "goto1090" }; return $out; }
  if (abs($state->{"F"} - 0.05) > 1) { my $out = { "anchor" => "goto945" }; return $out; }
  if (abs($state->{"F"} - 0.05) < 0.05) { my $out = { "anchor" => "goto945" }; return $out; }
  if (abs($state->{"P"}) > 180) { my $out = { "anchor" => "goto925" }; return $out; }
  my $out = { "anchor" => "goto620" }; return $out;
}

#620 N=20
#625 IF T1<400 THEN 635
#630 N=T1/20
sub goto620 {
  $state->{"N"} = 20;
  if ($state->{"T1"} < 400) { my $out = { "anchor" => "goto635" }; return $out; }
  $state->{"N"} = $state->{"T1"} / 20;
  my $out = { "anchor" => "goto635" }; return $out;
}

#635 T1=T1/N
#640 P=P*3.14159/180
#645 S=SIN(P)
#650 C=COS(P)
#655 M2=M0*T1*F/B
#660 R3=-.5*R0*((V0/R)^2)+R*A1*A1
#665 A3=-2*R1*A1/R
sub goto635 {
  $state->{"T1"} = $state->{"T1"} / $state->{"N"};
  $state->{"P"} = $state->{"P"} * 3.14159 / 180;
  $state->{"S"} = sin($state->{"P"});
  $state->{"C"} = cos($state->{"P"});
  $state->{"M2"} = $state->{"M0"} * $state->{"T1"} * $state->{"F"} / $state->{"B"};
  $state->{"R3"} = -0.5 * $state->{"R0"} * (( $state->{"V0"} / $state->{"R"} )**2 ) + $state->{"R"} * $state->{"A1"} * $state->{"A1"};
  $state->{"A3"} = -2 * $state->{"R1"} * $state->{"A1"} / $state->{"R"}; 
  my $out = { "anchor" => "goto670" }; return $out;
}

#670 FOR I=1 TO N       # Simply declare I=1; _N_=N and, when NEXT is reached,
#675 IF M1=0 THEN 715   # increment I and check that I <= _N_.
#680 M1=M1-M2           # Copy N so that if N changes, the reference value
#685 IF M1>0 THEN 725   # of our loop doesn't, i.e. _N_ remains localized
#690 F=F*(1+M1/M2)      # inside the loop.
#695 M2=M1+M2
#700 PRINT "YOU ARE OUT OF FUEL."
#705 M1=0
#710 GOTO 725
sub goto670 {
  $state->{"_N_"} = $state->{"N"};
  $state->{"I"} = 1;
  my $out = { "anchor" => "goto675" }; return $out;
}

sub goto675 {
  if ($state->{"M1"} == 0)
    { my $out = { "anchor" => "goto715" }; return $out; }
  $state->{"M1"} = $state->{"M1"} - $state->{"M2"};
  if ($state->{"M1"} > 0)
    { my $out = { "anchor" => "goto725" }; return $out; }
  $state->{"F"} = $state->{"F"} * ( 1 + $state->{"M1"} / $state->{"M2"} );
  $state->{"M2"} = $state->{"M1"} + $state->{"M2"};
  gameMessage("YOU ARE OUT OF FUEL.");
  $state->{"M1"} = 0;
  my $out = { "anchor" => "goto725" }; return $out;
}

#715 F=0
#720 M2=0
sub goto715 {
  $state->{"F"} = 0;
  $state->{"M2"} = 0;
  my $out = { "anchor" => "goto725" }; return $out;
}

#725 M=M-.5*M2
#730 R4=R3
#735 R3=-.5*R0*((V0/R)^2)+R*A1*A1
#740 R2=(3*R3-R4)/2+.00526*F1*F*C/M
#745 A4=A3
#750 A3=-2*R1*A1/R
#755 A2=(3*A3-A4)/2+.0056*F1*F*S/(M*R)
#760 X=R1*T1+.5*R2*T1*T1
#765 R=R+X
#770 H0=H0+X
#775 R1=R1+R2*T1
#780 A=A+A1*T1+.5*A2*T1*T1
#785 A1=A1+A2*T1
#790 M=M-.5*M2
#795 T=T+T1
#800 IF H0<3.287828E-04 THEN 810
#805 NEXT I
sub goto725 {
  $state->{"M"} = $state->{"M"} - 0.5 * $state->{"M2"};
  $state->{"R4"} = $state->{"R3"};
  $state->{"R3"} = -0.5 * $state->{"R0"} * (( $state->{"V0"} / $state->{"R"} )**2 ) + $state->{"R"} * $state->{"A1"} * $state->{"A1"};
  $state->{"R2_DEBUG1"} = ( 3 * $state->{"R3"} - $state->{"R4"} ) / 2;
  $state->{"R2_DEBUG2"} = 0.00526 * $state->{"F1"} * $state->{"F"} * $state->{"C"} / $state->{"M"};
  $state->{"R2"} = ( 3 * $state->{"R3"} - $state->{"R4"} ) / 2 + 0.00526 * $state->{"F1"} * $state->{"F"} * $state->{"C"} / $state->{"M"};
  $state->{"A4"} = $state->{"A3"};
  $state->{"A3"} = -2 * $state->{"R1"} * $state->{"A1"} / $state->{"R"};
  $state->{"A2"} = (3 * $state->{"A3"} - $state->{"A4"} ) / 2 + 0.0056 * $state->{"F1"} * $state->{"F"} * $state->{"S"} / ( $state->{"M"} * $state->{"R"} );
  $state->{"X"} = $state->{"R1"} * $state->{"T1"} + 0.5 * $state->{"R2"} * $state->{"T1"} * $state->{"T1"};
  $state->{"R"} = $state->{"R"} + $state->{"X"};
  $state->{"H0"} = $state->{"H0"} + $state->{"X"};
  $state->{"R1"} = $state->{"R1"} + $state->{"R2"} * $state->{"T1"};
  $state->{"A"} = $state->{"A"} + $state->{"A1"} * $state->{"T1"} + 0.5 * $state->{"A2"} * $state->{"T1"} * $state->{"T1"};
  $state->{"A1"} = $state->{"A1"} + $state->{"A2"} * $state->{"T1"};
  $state->{"M"} = $state->{"M"} - 0.5 * $state->{"M2"};
  $state->{"T"} = $state->{"T"} + $state->{"T1"};
  if ($state->{"H0"} < 0.0003287828 )
    { my $out = { "anchor" => "goto810" }; return $out; }
  # ( NEXT I )
  $state->{"I"} = $state->{"I"} + 1;
  if ( $state->{"I"} <= $state->{"_N_"} )
    { my $out = { "anchor" => "goto675" }; return $out; }
  else { $state->{"I"} = $state->{"I"} - 1; }
  my $out = { "anchor" => "goto810" }; return $out;
}

#810 H=H0*Z
#815 H1=R1*Z
#820 D=R0*A*Z
#825 D1=R*A1*Z
#830 T2=M1*B/M0
#835 PRINT " ";T;TAB(10);H;TAB(23);D;
#840 PRINT TAB(37);H1;TAB(49);D1;TAB(60);T2
#845 IF H0<3.287828E-04 THEN 880
#850 IF R0*A>164.474 THEN 1050
#855 IF M1>0 THEN 580
#860 T1=20
#865 F=0
#870 P=0
#875 GOTO 620
sub goto810 {
  $state->{"H"} = $state->{"H0"} * $state->{"Z"};
  $state->{"H1"} = $state->{"R1"} * $state->{"Z"};
  $state->{"D"} = $state->{"R0"} * $state->{"A"} * $state->{"Z"};
  $state->{"D1"} = $state->{"R"} * $state->{"A1"} * $state->{"Z"};
  $state->{"T2"} = $state->{"M1"} * $state->{"B"} / $state->{"M0"};
#835 PRINT " ";T;TAB(10);H;TAB(23);D;
#840 PRINT TAB(37);H1;TAB(49);D1;TAB(60);T2
  my $t =  int($state->{"T"}+0);
  my $h =  sprintf('%.2g', $state->{"H"}+0);
  my $d =  sprintf('%.2g', $state->{"D"}+0);
  my $h1 =  sprintf('%.4f', $state->{"H1"}+0);
  my $d1 =  sprintf('%.2f', $state->{"D1"}+0);
  my $t2 =  sprintf('%.3f', $state->{"T2"}+0);
  gameMessage(" ".
    sprintf('%-9s', $t).
    sprintf('%-13s', $h).
    sprintf('%-14s', $d).
    sprintf('%-12s', $h1).
    sprintf('%-11s', $d1).$t2);
  if ($state->{"H0"} < 0.0003287828 )
    { my $out = { "anchor" => "goto880" }; return $out; }
  if ($state->{"R0"}*$state->{"A"} > 164.474 )
    { my $out = { "anchor" => "goto1050" }; return $out; }
  if ($state->{"M1"} > 0 )
    { my $out = { "anchor" => "goto580" }; return $out; }
  $state->{"T1"} = 20;
  $state->{"F"} = 0;
  $state->{"P"} = 0;
  my $out = { "anchor" => "goto620" }; return $out;
}

#880 IF R1<-8.21957E-04 THEN 1020
#885 IF ABS(R*A1)>4.93174E-04 THEN 1020
#890 IF H0<-3.287828E-04 THEN 1020
#895 IF ABS(D)>10*Z THEN 1065
#900 GOTO 995
sub goto880 {
  if ($state->{"R1"} < -0.000821957 )
    { my $out = { "anchor" => "goto1020" }; return $out; }
  if (abs($state->{"R"} * $state->{"A1"}) > 0.000493174 )
    { my $out = { "anchor" => "goto1020" }; return $out; }
  if ($state->{"H0"} < -0.0003287828 )
    { my $out = { "anchor" => "goto1020" }; return $out; }
  if (abs($state->{"D"}) > 10 * $state->{"Z"} )
    { my $out = { "anchor" => "goto1065" }; return $out; }
  my $out = { "anchor" => "goto995" }; return $out;
}

#905 PRINT
#910 PRINT "THIS SPACECRAFT IS NOT ABLE TO VIOLATE THE SPACE-";
#915 PRINT "TIME CONTINUUM."
#920 GOTO 575
sub goto905 {
  blank();
  gameMessage("THIS SPACECRAFT IS NOT ABLE TO VIOLATE THE SPACE-TIME CONTINUUM.");
  my $out = { "anchor" => "goto575" }; return $out;
}

#925 PRINT
#930 PRINT "IF YOU WANT TO SPIN AROUND, GO OUTSIDE THE MODULE"
#935 PRINT "FOR AN E.V.A."
#940 GOTO 575
sub goto925 {
  blank();
  gameMessage("IF YOU WANT TO SPIN AROUND, GO OUTSIDE THE MODULE");
  gameMessage("FOR AN E.V.A.");
  my $out = { "anchor" => "goto575" }; return $out;
}

#945 PRINT
#950 PRINT "IMPOSSIBLE THRUST VALUE ";
#955 IF F<0 THEN 985
#960 IF F-.05<.05 THEN 975
#965 PRINT "TOO LARGE"
#970 GOTO 575
sub goto945 {
  blank();
  $__lemmsg__ = "IMPOSSIBLE THRUST VALUE ";
  if ($state->{"F"} < 0 )
    { my $out = { "anchor" => "goto985" }; return $out; }
  if ($state->{"F"}-0.05 < 0.05 )
    { my $out = { "anchor" => "goto975" }; return $out; }
  $__lemmsg__ = $__lemmsg__."TOO LARGE";
  gameMessage($__lemmsg__); $__lemmsg__ = '';
  my $out = { "anchor" => "goto575" }; return $out;
}

#975 PRINT "TOO SMALL"
#980 GOTO 575
sub goto975 {
  $__lemmsg__ = $__lemmsg__."TOO SMALL";
  gameMessage($__lemmsg__); $__lemmsg__ = '';
  my $out = { "anchor" => "goto575" }; return $out;
}

#985 PRINT "NEGATIVE"
#990 GOTO 575
sub goto985 {
  $__lemmsg__ = $__lemmsg__."NEGATIVE";
  gameMessage($__lemmsg__); $__lemmsg__ = '';
  my $out = { "anchor" => "goto575" }; return $out;
}

#995 PRINT
#1000 PRINT "TRANQUILITY BASE HERE -- THE EAGLE HAS LANDED."
#1005 PRINT "CONGRATULATIONS -- THERE WAS NO SPACECRAFT DAMAGE."
#1010 PRINT "YOU MAY NOW PROCEED WITH SURFACE EXPLORATION."
#1015 GOTO 1100
sub goto995 {
  blank();
  gameMessage("TRANQUILITY BASE HERE -- THE EAGLE HAS LANDED.");
  gameMessage("CONGRATULATIONS -- THERE WAS NO SPACECRAFT DAMAGE.");
  gameMessage("YOU MAY NOW PROCEED WITH SURFACE EXPLORATION.");
  my $out = { "anchor" => "goto1100" }; return $out;
}

#1020 PRINT
#1025 PRINT "CRASH !!!!!!!!!!!!!!!!"
#1030 PRINT "YOUR IMPACT CREATED A CRATER";ABS(H);M$;" DEEP."
#1035 X1=SQR(D1*D1+H1*H1)*G3
#1040 PRINT "AT CONTACT YOU WERE TRAVELING";X1;N$;"/HR"
#1045 GOTO 1100
sub goto1020 {
  blank();
  gameMessage("CRASH !!!!!!!!!!!!!!!!");
  gameMessage("YOUR IMPACT CREATED A CRATER ".abs($state->{"H"})." ".$state->{"Ms"}." DEEP.");
  $state->{"X1"} = sqrt($state->{"D1"}*$state->{"D1"}+$state->{"H1"}*$state->{"H1"})*$state->{"G3"};
  gameMessage("AT CONTACT YOU WERE TRAVELING ".$state->{"X1"}." ".$state->{"Ns"}."/HR");
  my $out = { "anchor" => "goto1100" }; return $out;
}

#1050 PRINT
#1055 PRINT "YOU HAVE BEEN LOST IN SPACE WITH NO HOPE OF RECOVERY."
#1060 GOTO 1100
sub goto1050 {
  blank();
  gameMessage("YOU HAVE BEEN LOST IN SPACE WITH NO HOPE OF RECOVERY.");
  my $out = { "anchor" => "goto1100" }; return $out;
}

#1065 PRINT "YOU ARE DOWN SAFELY - "
#1075 PRINT
#1080 PRINT "BUT MISSED THE LANDING SITE BY";ABS(D/G5);N$;"."
#1085 GOTO 1100
sub goto1065 {
  gameMessage("YOU ARE DOWN SAFELY - ");
  blank();
  gameMessage("BUT MISSED THE LANDING SITE BY ".abs($state->{"D"}/$state->{"G5"}).$state->{"Ns"}.".");
  my $out = { "anchor" => "goto1100" }; return $out;
}

#1090 PRINT
#1095 PRINT "MISSION ABENDED"
sub goto1090 {
  blank();
  gameMessage("MISSION ABENDED");
  my $out = { "anchor" => "goto1100" }; return $out;
}

#1100 PRINT 
sub goto1100 {
  blank();
  my $out = { "anchor" => "goto1105" }; return $out;
}

#1105 PRINT "DO YOU WANT TO TRY IT AGAIN (YES/NO)?"
#1110 INPUT Z$
sub goto1105 {
  gameMessage("DO YOU WANT TO TRY IT AGAIN (YES/NO)?");
  my $out = {
    "anchor" => "goto1115",
    "echo" => " > ",
    "hook" => "reply"
  };
  return $out;
}

#1115 IF Z$="YES" THEN 20
#1120 IF Z$="NO" THEN 1130
#1125 GOTO 1105
sub goto1115 {
  $state->{"Zs"} = $input->{"action"}[0];
  if ($state->{"Zs"} eq "YES")
    { my $out = { "anchor" => "goto20" }; return $out; }
  if ($state->{"Zs"} eq "NO")
    { my $out = { "anchor" => "goto1130" }; return $out; }
  my $out = { "anchor" => "goto1105" }; return $out;
}

#1130 PRINT
#1135 PRINT "TOO BAD, THE SPACE PROGRAM HATES TO LOSE EXPERIENCED"
#1140 PRINT "ASTRONAUTS."
#1145 STOP
sub goto1130 {
  blank();
  gameMessage("TOO BAD, THE SPACE PROGRAM HATES TO LOSE EXPERIENCED");
  gameMessage("ASTRONAUTS.");
  my $out = { "anchor" => "quit_game" }; return $out;
}

#1150 PRINT
#1155 PRINT "OK, DO YOU WANT THE COMPLETE INSTRUCTIONS OR THE INPUT -"
#1160 PRINT "OUTPUT STATEMENTS?"
sub goto1150 {
  blank();
  gameMessage("OK, DO YOU WANT THE COMPLETE INSTRUCTIONS OR THE INPUT -");
  gameMessage("OUTPUT STATEMENTS?");
  my $out = { "anchor" => "goto1165" }; return $out;
}

#1165 PRINT "1=COMPLETE INSTRUCTIONS"
#1170 PRINT "2=INPUT-OUTPUT STATEMENTS"
#1175 PRINT "3=NEITHER"
#1180 INPUT B1
sub goto1165 {
  gameMessage("1=COMPLETE INSTRUCTIONS");
  gameMessage("2=INPUT-OUTPUT STATEMENTS");
  gameMessage("3=NEITHER");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto1185",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

#1185 Q$="NO"
#1190 IF B1=1 THEN 205
#1195 Q$="YES"
#1200 IF B1=2 THEN 190
#1205 IF B1=3 THEN 190
#1210 GOTO 1165
sub goto1185 {
  $state->{"B1"} = $input->{"action"}[0];
  $state->{"Qs"} = "NO";
  if (($state->{"B1"} == 1) or ($state->{"B1"} eq "1"))
    { my $out = { "anchor" => "goto205" }; return $out; }
  $state->{"Qs"} = "YES";
  if (($state->{"B1"} == 2) or ($state->{"B1"} eq "2"))
    { my $out = { "anchor" => "goto190" }; return $out; }
  if (($state->{"B1"} == 3) or ($state->{"B1"} eq "3"))
    { my $out = { "anchor" => "goto190" }; return $out; }
  my $out = { "anchor" => "goto1165" }; return $out;
}
#1215 END

1;

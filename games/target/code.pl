#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

use Storable qw(dclone);

sub initialize {
  my $out = { "anchor" => "goto10" }; return $out;
}

#10 PRINT TAB(33);"TARGET"
#20 PRINT TAB(15);"CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY"
#30 PRINT: PRINT: PRINT
#100 R=1: R1=57.296: P=3.14159
#110 PRINT "YOU ARE THE WEAPONS OFFICER ON THE STARSHIP ENTERPRISE"
#120 PRINT "AND THIS IS A TEST TO SEE HOW ACCURATE A SHOT YOU"
#130 PRINT "ARE IN A THREE-DIMENSIONAL RANGE.  YOU WILL BE TOLD"
#140 PRINT "THE RADIAN OFFSET FOR THE X AND Z AXES, THE LOCATION"
#150 PRINT "OF THE TARGET IN THREE DIMENSIONAL RECTANGULAR COORDINATES,"
#160 PRINT "THE APPROXIMATE NUMBER OF DEGREES FROM THE X AND Z"
#170 PRINT "AXES, AND THE APPROXIMATE DISTANCE TO THE TARGET."
#180 PRINT "YOU WILL THEN PROCEEED TO SHOOT AT THE TARGET UNTIL IT IS"
#190 PRINT "DESTROYED!": PRINT: PRINT "GOOD LUCK!!":PRINT: PRINT
sub goto10 {
  gameMessageTab(33, "TARGET");
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  blank(3);
  $state->{"R"} = 1;
  $state->{"R1"} = 57.296;
  $state->{"P"} = 3.14159;
  gameMessage("YOU ARE THE WEAPONS OFFICER ON THE STARSHIP ENTERPRISE");
  gameMessage("AND THIS IS A TEST TO SEE HOW ACCURATE A SHOT YOU");
  gameMessage("ARE IN A THREE-DIMENSIONAL RANGE.  YOU WILL BE TOLD");
  gameMessage("THE RADIAN OFFSET FOR THE X AND Z AXES, THE LOCATION");
  gameMessage("OF THE TARGET IN THREE DIMENSIONAL RECTANGULAR COORDINATES,");
  gameMessage("THE APPROXIMATE NUMBER OF DEGREES FROM THE X AND Z");
  gameMessage("AXES, AND THE APPROXIMATE DISTANCE TO THE TARGET.");
  gameMessage("YOU WILL THEN PROCEEED TO SHOOT AT THE TARGET UNTIL IT IS");
  gameMessage("GOOD LUCK!!");
  blank(2);
  my $out = { "anchor" => "goto220" }; return $out;
}

#220 A=RND(1)*2*P: B=RND(1)*2*P: Q=INT(A*R1): W=INT(B*R1)
#260 PRINT "RADIANS FROM X AXIS =";A;"   FROM Z AXIS =";B
#280 P1=100000*RND(1)+RND(1): X=SIN(B)*COS(A)*P1: Y=SIN(B)*SIN(A)*P1
#290 Z=COS(B)*P1
#340 PRINT "TARGET SIGHTED: APPROXIMATE COORDINATES:  X=";X;"  Y=";Y;"  Z=";Z
sub goto220 {
  $state->{"A"} = rand()*2*$state->{"P"};
  $state->{"B"} = rand()*2*$state->{"P"};
  $state->{"Q"} = int($state->{"A"}*$state->{"R1"});
  $state->{"W"} = int($state->{"B"}*$state->{"R1"});
  gameMessage("RADIANS FROM X AXIS =".$state->{"A"}."   FROM Z AXIS =".$state->{"B"});
  $state->{"P1"} = 100000*rand()+rand();
  $state->{"X"} = sin($state->{"B"})*cos($state->{"A"})*$state->{"P1"};
  $state->{"Y"} = sin($state->{"B"})*sin($state->{"A"})*$state->{"P1"};
  $state->{"Z"} = cos($state->{"B"})*$state->{"P1"};
  gameMessage("TARGET SIGHTED: APPROXIMATE COORDINATES:  X=".$state->{"X"}."  Y=".$state->{"Y"}."  Z=".$state->{"Z"});
  my $out = { "anchor" => "goto345" }; return $out;
}

#345 R=R+1: IF R>5 THEN 390
#350 ON R GOTO 355,360,365,370,375
sub goto345 {
  $state->{"R"} = $state->{"R"}+1;
  if ($state->{"R"} > 5) { my $out = { "anchor" => "goto390" }; return $out; }
  if ($state->{"R"} == 1) { my $out = { "anchor" => "goto355" }; return $out; }
  if ($state->{"R"} == 2) { my $out = { "anchor" => "goto360" }; return $out; }
  if ($state->{"R"} == 3) { my $out = { "anchor" => "goto365" }; return $out; }
  if ($state->{"R"} == 4) { my $out = { "anchor" => "goto370" }; return $out; }
  if ($state->{"R"} == 5) { my $out = { "anchor" => "goto375" }; return $out; }
}

#355 P3=INT(P1*.05)*20: GOTO 390
sub goto355 {
  $state->{"P3"} = int($state->{"P1"}*0.5)*20;
  my $out = { "anchor" => "goto390" }; return $out;
}

#360 P3=INT(P1*.1)*10: GOTO 390
sub goto360 {
  $state->{"P3"} = int($state->{"P1"}*0.1)*10;
  my $out = { "anchor" => "goto390" }; return $out;
}

#365 P3=INT(P1*.5)*2: GOTO 390
sub goto365 {
  $state->{"P3"} = int($state->{"P1"}*0.5)*2;
  my $out = { "anchor" => "goto390" }; return $out;
}

#370 P3=INT(P1): GOTO 390
sub goto370 {
  $state->{"P3"} = int($state->{"P1"});
  my $out = { "anchor" => "goto390" }; return $out;
}

#375 P3=P1
sub goto375 {
  $state->{"P3"} = $state->{"P1"};
  my $out = { "anchor" => "goto390" }; return $out;
}

#390 PRINT "     ESTIMATED DISTANCE:";P3
#400 PRINT:PRINT "INPUT ANGLE DEVIATION FROM X, DEVIATION FROM Z, DISTANCE";
#405 INPUT A1,B1,P2
sub goto390 {
  gameMessage("     ESTIMATED DISTANCE:".$state->{"P3"});
  blank();
  gameMessage("INPUT ANGLE DEVIATION FROM X, DEVIATION FROM Z, DISTANCE");
  my @argv = ("number", "number", "number");
  my $out = {
    "anchor" => "goto410",
    "echo" => " > ",
    "argc" => 3,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

#410 PRINT: IF P2<20 THEN PRINT "YOU BLEW YOURSELF UP!!": GOTO 580
#420 A1=A1/R1: B1=B1/R1: PRINT "RADIANS FROM X AXIS =";A1;"  ";
#425 PRINT "FROM Z AXIS =";B1
#480 X1=P2*SIN(B1)*COS(A1): Y1=P2*SIN(B1)*SIN(A1): Z1=P2*COS(B1)
#510 D=((X1-X)^2+(Y1-Y)^2+(Z1-Z)^2)^(1/2)
#520 IF D>20 THEN 670
#530 PRINT: PRINT " * * * HIT * * *   TARGET IS NON-FUNCTIONAL": PRINT
#550 PRINT "DISTANCE OF EXPLOSION FROM TARGET WAS";D;"KILOMETERS."
#570 PRINT: PRINT "MISSION ACCOMPLISHED IN ";R;" SHOTS."
sub goto410 {
  blank();
  if ($state->{"P2"} < 20) {
    gameMessage("YOU BLEW YOURSELF UP!!");
    my $out = { "anchor" => "goto580" }; return $out;
  }
  $state->{"A1"} = $state->{"A1"}/$state->{"R1"};
  $state->{"B1"} = $state->{"B1"}/$state->{"R1"};
  gameMessage("RADIANS FROM X AXIS =".$state->{"A1"}."  FROM Z AXIS =".$state->{"B1"});
  $state->{"X1"} = $state->{"P2"}*sin($state->{"B1"})*cos($state->{"A1"});
  $state->{"Y1"} = $state->{"P2"}*sin($state->{"B1"})*sin($state->{"A1"});
  $state->{"Z1"} = $state->{"P2"}*cos($state->{"B1"});
  $state->{"D"} = (($state->{"X1"}-$state->{"X"})**2+($state->{"Y1"}-$state->{"Y"})**2+($state->{"Z1"}-$state->{"Z"})**2)**(1/2);
  if ($state->{"D"} > 20) { my $out = { "anchor" => "goto670" }; return $out; }
  blank();
  gameMessage(" * * * HIT * * *   TARGET IS NON-FUNCTIONAL");
  blank();
  gameMessage("DISTANCE OF EXPLOSION FROM TARGET WAS".$state->{"D"}."KILOMETERS.");
  gameMessage("MISSION ACCOMPLISHED IN ".$state->{"R"}." SHOTS.");
  my $out = { "anchor" => "goto580" }; return $out;
}

#580 R=0: FOR I=1 TO 5: PRINT: NEXT I: PRINT "NEXT TARGET...": PRINT
#590 GOTO 220
sub goto580 {
  $state->{"R"} = 0;
  blank(5);
  gameMessage("NEXT TARGET...");
  blank();
  my $out = { "anchor" => "goto220" }; return $out;
}

#670 X2=X1-X: Y2=Y1-Y: Z2=Z1-Z: IF X2<0 THEN 730
#710 PRINT "SHOT IN FRONT OF TARGET";X2;"KILOMETERS.": GOTO 740
sub goto670 {
  $state->{"X2"} = $state->{"X1"}-$state->{"X"};
  $state->{"Y2"} = $state->{"Y1"}-$state->{"Y"};
  $state->{"Z2"} = $state->{"Z1"}-$state->{"Z"};
  if ($state->{"X2"} < 0) { my $out = { "anchor" => "goto730" }; return $out; }
  gameMessage("SHOT IN FRONT OF TARGET ".$state->{"X2"}." KILOMETERS.");
  my $out = { "anchor" => "goto740" }; return $out;
}

#730 PRINT "SHOT BEHIND TARGET";-X2;"KILOMETERS."
sub goto730 {
  gameMessage("SHOT BEHIND TARGET ".-$state->{"X2"}." KILOMETERS.");
  my $out = { "anchor" => "goto740" }; return $out;
}

#740 IF Y2<0 THEN 770
#750 PRINT "SHOT TO LEFT OF TARGET";Y2;"KILOMETERS.": GOTO 780
sub goto740 {
  if ($state->{"Y2"} < 0) { my $out = { "anchor" => "goto770" }; return $out; }
  gameMessage("SHOT TO LEFT OF TARGET ".$state->{"Y2"}." KILOMETERS.");
  my $out = { "anchor" => "goto780" }; return $out;
}

#770 PRINT "SHOT TO RIGHT OF TARGET";-Y2;"KILOMETERS."
sub goto740 {
  gameMessage("SHOT TO RIGHT OF TARGET ".-$state->{"Y2"}." KILOMETERS.");
  my $out = { "anchor" => "goto780" }; return $out;
}

#780 IF Z2<0 THEN 810
#790 PRINT "SHOT ABOVE TARGET";Z2;"KILOMETERS.": GOTO 820
sub goto780 {
  if ($state->{"Z2"} < 0) { my $out = { "anchor" => "goto810" }; return $out; }
  gameMessage("SHOT ABOVE TARGET ".$state->{"Z2"}." KILOMETERS.");
  my $out = { "anchor" => "goto820" }; return $out;
}

#810 PRINT "SHOT BELOW TARGET";-Z2;"KILOMETERS."
sub goto810 {
  gameMessage("SHOT BELOW TARGET ".-$state->{"Z2"}." KILOMETERS.");
  my $out = { "anchor" => "goto820" }; return $out;
}

#820 PRINT "APPROX POSITION OF EXPLOSION:  X=";X1;"   Y=";Y1;"   Z=";Z1
#830 PRINT "     DISTANCE FROM TARGET =";D: PRINT: PRINT: PRINT: GOTO 345
sub goto810 {
  essage("APPROX POSITION OF EXPLOSION:  X=".$state->{"X1"}."   Y=".$state->{"Y1"}."   Z=".$state->{"Z1"});
  gameMessage("     DISTANCE FROM TARGET =".$state->{"D"});
  blank(3);
  my $out = { "anchor" => "goto345" }; return $out;
}

#999 END
1;

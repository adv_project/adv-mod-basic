#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

use Storable qw(dclone);

sub initialize {
  my $out = { "anchor" => "goto10" };
  return $out;
}

sub goto10 {
  gameMessageTab(32, "HAMURABI");
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  blank(3);
  gameMessage("TRY YOUR HAND AT GOVERNING ANCIENT SUMERIA");
  gameMessage("FOR A TEN-YEAR TERM OF OFFICE.");
  blank();
  $state->{"D1"} = 0;
  $state->{"P1"} = 0;
  $state->{"Z"} = 0;
  $state->{"P"} = 95;
  $state->{"S"} = 2800;
  $state->{"H"} = 3000;
  $state->{"E"} = $state->{"H"} - $state->{"S"};
  $state->{"Y"} = 3;
  $state->{"A"} = $state->{"H"} / $state->{"Y"};
  $state->{"I"} = 5;
  $state->{"Q"} = 1;
  my $out = { "anchor" => "goto210" };
  return $out;
}

sub goto210 {
  $state->{"D"} = 0;
  my $out = { "anchor" => "goto215" };
  return $out;
}

sub goto215 {
  blank(2);
  gameMessage("HAMURABI:  I BEG TO REPORT TO YOU,");
  $state->{"Z"} = $state->{"Z"} + 1;
  gameMessage("IN YEAR ".$state->{"Z"}.", ".$state->{"D"}." PEOPLE STARVED, ".$state->{"I"}." CAME TO THE CITY,");
  $state->{"P"} = $state->{"P"} + $state->{"I"};
  if ($state->{"Q"} > 0) {
    my $out = { "anchor" => "goto230" };
    return $out;
  }
  $state->{"P"} = int($state->{"P"} / 2);
  gameMessage("A HORRIBLE PLAGUE STRUCK!  HALF THE PEOPLE DIED.");
  my $out = { "anchor" => "goto230" };
  return $out;
}

sub goto230 {
  gameMessage("POPULATION IS NOW ".$state->{"P"});
  gameMessage("THE CITY NOW OWNS ".$state->{"A"}." ACRES.");
  gameMessage("YOU HARVESTED ".$state->{"Y"}." BUSHELS PER ACRE.");
  gameMessage("THE RATS ATE ".$state->{"E"}." BUSHELS.");
  gameMessage("YOU NOW HAVE ".$state->{"S"}." BUSHELS IN STORE.");
  if ($state->{"Z"} == 11) {
    my $out = { "anchor" => "goto860" };
    return $out;
  }
  $state->{"C"} = int(10 * rand());
  $state->{"Y"} = $state->{"C"} + 17;
  gameMessage("LAND IS TRADING AT ".$state->{"Y"}." BUSHELS PER ACRE.");
  my $out = { "anchor" => "goto320" };
  return $out;
}

sub goto320 {
  gameMessage("HOW MANY ACRES DO YOU WISH TO BUY");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto321",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto321 {
  $state->{"Q"} = $input->{"action"}[0];
  if ($state->{"Q"} < 0) {
    my $out = { "anchor" => "goto850" };
    return $out;
  }
  if ($state->{"Y"} * $state->{"Q"} <= $state->{"S"}) {
    my $out = { "anchor" => "goto330" };
    return $out;
  }
  gosub710();
  my $out = { "anchor" => "goto320" };
  return $out;
}

sub goto330 {
  if ($state->{"Q"} == 0) {
    my $out = { "anchor" => "goto340" };
    return $out;
  }
  $state->{"A"} = $state->{"A"} + $state->{"Q"};
  $state->{"S"} = $state->{"S"} - $state->{"Y"} * $state->{"Q"};
  $state->{"C"} = 0;
  my $out = { "anchor" => "goto400" };
  return $out;
}

sub goto340 {
  gameMessage("HOW MANY ACRES DO YOU WISH TO SELL");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto341",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto341 {
  $state->{"Q"} = $input->{"action"}[0];
  if ($state->{"Q"} < 0) {
    my $out = { "anchor" => "goto850" };
    return $out;
  }
  if ($state->{"Q"} < $state->{"A"}) {
    my $out = { "anchor" => "goto350" };
    return $out;
  }
  gosub720();
  my $out = { "anchor" => "goto340" };
  return $out;
}

sub goto350 {
  $state->{"A"} = $state->{"A"} - $state->{"Q"};
  $state->{"S"} = $state->{"S"} + $state->{"Y"} * $state->{"Q"};
  $state->{"C"} = 0;
  my $out = { "anchor" => "goto400" };
  return $out;
}

sub goto400 {
  blank();
  my $out = { "anchor" => "goto410" };
  return $out;
}

sub goto410 {
  gameMessage("HOW MANY BUSHELS DO YOU WISH TO FEED YOUR PEOPLE");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto411",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto411 {
  $state->{"Q"} = $input->{"action"}[0];
  if ($state->{"Q"} < 0) {
    my $out = { "anchor" => "goto850" };
    return $out;
  }
  # *** TRYING TO USE MORE GRAIN THAN IS IN SILOS?
  if ($state->{"Q"} <= $state->{"S"}) {
    my $out = { "anchor" => "goto430" };
    return $out;
  }
  gosub710();
  my $out = { "anchor" => "goto410" };
  return $out;
}

sub goto430 {
  $state->{"S"} = $state->{"S"} - $state->{"Q"};
  $state->{"C"} = 1;
  blank(1);
  my $out = { "anchor" => "goto440" };
  return $out;
}

sub goto440 {
  gameMessage("HOW MANY ACRES DO YOU WISH TO PLANT WITH SEED");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto441",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto441 {
  $state->{"D"} = $input->{"action"}[0];
  if ($state->{"D"} == 0) {
    my $out = { "anchor" => "goto511" };
    return $out;
  }
  if ($state->{"D"} < 0) {
    my $out = { "anchor" => "goto850" };
    return $out;
  }
  # *** TRYING TO PLANT MORE ACRES THAN YOU OWN?
  if ($state->{"D"} <= $state->{"A"}) {
    my $out = { "anchor" => "goto450" };
    return $out;
  }
  gosub720();
  my $out = { "anchor" => "goto440" };
  return $out;
}

sub goto450 {
  # *** ENOUGH GRAIN FOR SEED?
  if (int($state->{"D"} / 2) <= $state->{"S"}) {
    my $out = { "anchor" => "goto455" };
    return $out;
  }
  gosub710();
  my $out = { "anchor" => "goto440" };
  return $out;
}

sub goto455 {
  # *** ENOUGH PEOPLE TO TEND THE CROPS?
  if ($state->{"D"} < 10 * $state->{"P"}) {
    my $out = { "anchor" => "goto510" };
    return $out;
  }
  gameMessage("BUT YOU HAVE ONLY ".$state->{"P"}." PEOPLE TO TEND THE FIELDS!  NOW THEN,");
  my $out = { "anchor" => "goto440" };
  return $out;
}

sub goto510 {
  $state->{"S"} = $state->{"S"} - int($state->{"D"} / 2);
  my $out = { "anchor" => "goto511" };
  return $out;
}

sub goto511 {
  gosub800();
  # *** A BOUNTIFUL HARVEST!
  $state->{"Y"} = $state->{"C"};
  $state->{"H"} = $state->{"D"} * $state->{"Y"};
  $state->{"E"} = 0;
  gosub800();
  if ( int($state->{"C"} / 2) != $state->{"C"} / 2 ) {
    my $out = { "anchor" => "goto530" };
    return $out;
  }
  # *** RATS ARE RUNNING WILD!!
  $state->{"E"} = int($state->{"S"} / $state->{"C"});
  my $out = { "anchor" => "goto530" };
  return $out;
}

sub goto530 {
  $state->{"S"} = $state->{"S"} - $state->{"E"} + $state->{"H"};
  gosub800();
  # *** LET'S HAVE SOME BABIES
  $state->{"I"} = int($state->{"C"} * ( 20 * $state->{"A"} + $state->{"S"} ) / $state->{"P"} / 100 + 1);
  # *** HOW MANY PEOPLE HAD FULL TUMMIES?
  $state->{"C"} = int($state->{"Q"} / 20);
  # *** HORROS, A 15% CHANCE OF PLAGUE
  $state->{"Q"} = int(10 * ( 2 * rand() - 0.3));
  if ($state->{"P"} < $state->{"C"}) {
    my $out = { "anchor" => "goto210" };
    return $out;
  }
  # *** STARVE ENOUGH FOR IMPEACHMENT?
  $state->{"D"} = $state->{"P"} - $state->{"C"};
  if ($state->{"D"} > 0.45 * $state->{"P"}) {
    my $out = { "anchor" => "goto560" };
    return $out;
  }
  $state->{"P1"} = ( ( $state->{"Z"} - 1 ) * $state->{"P1"} + $state->{"D"} * 100 / $state->{"P"} ) / $state->{"Z"};
  $state->{"P"} = $state->{"C"};
  $state->{"D1"} = $state->{"D1"} + $state->{"D"};
  my $out = { "anchor" => "goto215" };
  return $out;
}

sub goto560 {
  gameMessage("YOU STARVED ".$state->{"D"}." PEOPLE IN ONE YEAR!!!");
  my $out = { "anchor" => "goto565" };
  return $out;
}

sub goto565 {
  gameMessage("DUE TO THIS EXTREME MISMANAGEMENT YOU HAVE NOT ONLY");
  gameMessage("BEEN IMPEACHED AND THROWN OUT OF OFFICE BUT YOU HAVE");
  gameMessage("ALSO BEEN DECLARED NATIONAL FINK!!!!");
  my $out = { "anchor" => "goto990" };
  return $out;
}

sub gosub710 {
  gameMessage("HAMURABI:  THINK AGAIN.  YOU HAVE ONLY");
  gameMessage($state->{"S"}." BUSHELS OF GRAIN.  NOW THEN,");
  return;
}

sub gosub720 {
  gameMessage("HAMURABI:  THINK AGAIN.  YOU OWN ONLY ".$state->{"A"}." ACRES.  NOW THEN,");
  return;
}

sub gosub800 {
  $state->{"C"} = int( ( rand() * 5 ) + 1 );
  return;
}

sub goto850 {
  blank(2);
  gameMessage("HAMURABI:  I CANNOT DO WHAT YOU WISH.");
  gameMessage("GET YOURSELF ANOTHER STEWARD!!!!!");
  my $out = { "anchor" => "goto990" };
  return $out;
}

sub goto860 {
  gameMessage("IN YOUR 10-YEAR TERM OF OFFICE, ".$state->{"P1"}." PERCENT OF THE");
  gameMessage("POPULATION STARVED PER YEAR ON THE AVERAGE, I.E. A TOTAL OF");
  gameMessage($state->{"D1"}." PEOPLE DIED!!");
  $state->{"L"} = $state->{"A"} / $state->{"P"};
  gameMessage("YOU STARTED WITH 10 ACRES PER PERSON AND ENDED WITH");
  gameMessage($state->{"L"}." ACRES PER PERSON.");
  blank();
  if ($state->{"P1"} > 33) {
    my $out = { "anchor" => "goto565" };
    return $out;
  }
  if ($state->{"L"} < 7) {
    my $out = { "anchor" => "goto565" };
    return $out;
  }
  if ($state->{"P1"} > 10) {
    my $out = { "anchor" => "goto940" };
    return $out;
  }
  if ($state->{"L"} < 9) {
    my $out = { "anchor" => "goto940" };
    return $out;
  }
  if ($state->{"P1"} > 3) {
    my $out = { "anchor" => "goto960" };
    return $out;
  }
  if ($state->{"L"} < 10) {
    my $out = { "anchor" => "goto960" };
    return $out;
  }
  gameMessage("A FANTASTIC PERFORMANCE!!!  CHARLEMANGE, DISRAELI, AND");
  gameMessage("JEFFERSON COMBINED COULD NOT HAVE DONE BETTER!");
  my $out = { "anchor" => "goto990" };
  return $out;
}

sub goto940 {
  gameMessage("YOUR HEAVY-HANDED PERFORMANCE SMACKS OF NERO AND IVAN IV.");
  gameMessage("THE PEOPLE (REMIANING) FIND YOU AN UNPLEASANT RULER, AND,");
  gameMessage("FRANKLY, HATE YOUR GUTS!!");
  my $out = { "anchor" => "goto990" };
  return $out;
}

sub goto960 {
  gameMessage("YOUR PERFORMANCE COULD HAVE BEEN SOMEWHAT BETTER, BUT");
  gameMessage("REALLY WASN'T TOO BAD AT ALL. ".int( $state->{"P"} * 0.8 * rand() )." PEOPLE");
  gameMessage("WOULD DEARLY LIKE TO SEE YOU ASSASSINATED BUT WE ALL HAVE OUR");
  gameMessage("TRIVIAL PROBLEMS.");
  my $out = { "anchor" => "goto990" };
  return $out;
}

sub goto990 {
  gameMessage("SO LONG FOR NOW.");
  blank();
  my $out = { "anchor" => "quit_game" };
  return $out;
}

1;

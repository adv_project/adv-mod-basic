#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

sub initialize {
  my $out = { "anchor" => "goto10" }; return $out;
}

#10 PRINT TAB(33);"LUNAR"
#20 PRINT TAB(l5);"CREATIVE COMPUTING MORRISTOWN, NEW JERSEY" 
#25 PRINT:PRINT:PRINT
#30 PRINT "THIS IS A COMPUTER SIMULATION OF AN APOLLO LUNAR" 
#40 PRINT "LANDING CAPSULE.": PRINT: PRINT
#50 PRINT "THE ON-BOARD COMPUTER HAS FAILED (IT WAS MADE BY" 
#60 PRINT "XEROX) SO YOU HAVE TO LAND THE CAPSULE MANUALLY."
sub goto10 {
  gameMessageTab(33, "LUNAR");
  gameMessageTab(15, "CREATIVE COMPUTING MORRISTOWN, NEW JERSEY" );
  blank(3);
  gameMessage("THIS IS A COMPUTER SIMULATION OF AN APOLLO LUNAR" );
  gameMessage("LANDING CAPSULE.");
  blank(2);
  gameMessage("THE ON-BOARD COMPUTER HAS FAILED (IT WAS MADE BY" );
  gameMessage("XEROX) SO YOU HAVE TO LAND THE CAPSULE MANUALLY.");
  my $out = { "anchor" => "goto70" }; return $out;
}

#70 PRINT: PRINT "SET BURN RATE OF RETRO ROCKETS TO ANY VALUE BETWEEN" 
#80 PRINT "0 (FREE FALL) AND 200 (MAXIMUM BURN) POUNDS PER SECOND." 
#90 PRINT "SET NEW BURN RATE EVERY 10 SECONDS.": PRINT 
#100 PRINT "CAPSULE WEIGHT 32,500 LBS; FUEL WEIGHT 16,500 LBS."
#110 PRINT: PRINT: PRINT: PRINT "GOOD LUCK"
#120 L=0
#130 PRINT: PRINT "SEC","MI + FT","MPH","LB FUEL","BURN RATE":PRINT 
#140 A=120:V=1:M=33000:N=16500:G=1E-03:Z=1.8
sub goto70 {
  blank();
  gameMessage("SET BURN RATE OF RETRO ROCKETS TO ANY VALUE BETWEEN" );
  gameMessage("0 (FREE FALL) AND 200 (MAXIMUM BURN) POUNDS PER SECOND." );
  gameMessage("SET NEW BURN RATE EVERY 10 SECONDS.");
  blank();
  gameMessage("CAPSULE WEIGHT 32,500 LBS; FUEL WEIGHT 16,500 LBS.");
  blank(3);
  gameMessage("GOOD LUCK");
  $state->{"L"} = 0;
  blank();
  gameMessage(
    sprintf('%-16s', "SEC").
    sprintf('%-16s', "MI + FT").
    sprintf('%-16s', "MPH").
    sprintf('%-16s', "LB FUEL").
    sprintf('%-16s', "BURN RATE") );
  blank();
  $state->{"A"} = 120;
  $state->{"V"} = 1;
  $state->{"M"} = 33000;
  $state->{"N"} = 16500;
  $state->{"G"} = 1E-03;
  $state->{"Z"} = 1.8;
  $state->{"K"} = "-"; # Added this line
  my $out = { "anchor" => "goto150" }; return $out;
}

#150 PRINT L,INT(A);INT(5280*(A-INT(A))),3600*V,M-N,:INPUT K
sub goto150 {
  my $v = sprintf('%.2f', 3600*$state->{"V"});
  gameMessage(" ".
    sprintf('%-16s', $state->{"L"}).
    sprintf('%-16s', int($state->{"A"})."  ".int(5280*($state->{"A"}-int($state->{"A"})))).
    sprintf('%-16s', $v).
    sprintf('%-16s', $state->{"M"}-$state->{"N"}).
    sprintf('%-16s', $state->{"K"}) );
  my @argv = ("number");
  my $out = {
    "anchor" => "goto155",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

#T=10 
sub goto155 {
  $state->{"T"} = 10;
  $state->{"K"} = $input->{"action"}[0];
  my $out = { "anchor" => "goto160" }; return $out;
}

#160 IF M-N<1E-03 THEN 240
#170 IF T<1E-03 THEN 150
#180 S=T: IF M>=N+S*K THEN 200
#190 S=(M-N)/K
sub goto160 {
  if ($state->{"M"}-$state->{"N"} < 1E-03) { my $out = { "anchor" => "goto240" }; return $out; }
  if ($state->{"T"} < 1E-03) { my $out = { "anchor" => "goto150" }; return $out; }
  $state->{"S"} = $state->{"T"};
  if ($state->{"M"} >= $state->{"N"}+$state->{"S"}*$state->{"K"}) { my $out = { "anchor" => "goto200" }; return $out; }
  $state->{"S"} = ($state->{"M"}-$state->{"N"})/$state->{"K"};
  my $out = { "anchor" => "goto200" }; return $out;
}

#200 GOSUB 420: IF I<=O THEN 340
#210 IF V<=0 THEN 230
#220 IF J<0 THEN 370
sub goto200 {
  gosub420();
  if ($state->{"I"} <= 0) { my $out = { "anchor" => "goto340" }; return $out; }
  if ($state->{"V"} <= 0) { my $out = { "anchor" => "goto230" }; return $out; }
  if ($state->{"J"} < 0) { my $out = { "anchor" => "goto370" }; return $out; }
  my $out = { "anchor" => "goto230" }; return $out;
}

#230 GOSUB 330: GOTO 160
sub goto230 {
  gosub330();
  my $out = { "anchor" => "goto160" }; return $out;
}

#240 PRINT "FUEL OUT AT";L;"SECONDS":S=(-V+SQR(V*V+2*A*G))/G
#250 V=V+G*S: L=L+S
sub goto240 {
  gameMessage("FUEL OUT AT".$state->{"L"}."SECONDS");
  $state->{"S"} = (-$state->{"V"}+sqrt($state->{"V"}*$state->{"V"}+2*$state->{"A"}*$state->{"G"}))/$state->{"G"};
  $state->{"V"} = $state->{"V"}+$state->{"G"}*$state->{"S"};
  $state->{"L"} = $state->{"L"}+$state->{"S"};
  my $out = { "anchor" => "goto260" }; return $out;
}

#260 W=3600*V: PRINT "ON MOON AT";L;"SECONDS - IMPACT VELOCITY";W;"MPH" 
#274 IF W<=1.2 THEN PRINT "PERFECT LANDING!": GOTO 440 
#280 IF W<=10 THEN PRINT "GOOD LANDING (COULD RE BETTER)":GOTO 440 
#282 IF W>60 THEN 300
#284 PRINT "CRAFT DAMAGE... YOU'RE STRANDED HERE UNTIL A RESCUE" 
#286 PRINT "PARTY ARRIVES. HOPE YOU HAVE ENOUGH OXYGEN!" 
#288 GOTO 440
sub goto260 {
  $state->{"W"} = 3600*$state->{"V"};
  gameMessage("ON MOON AT ".sprintf('%.3f', $state->{"L"})." SECONDS - IMPACT VELOCITY ".sprintf('%.2f', $state->{"W"})." MPH" );
  if ($state->{"W"} <= 1.2) {
    gameMessage("PERFECT LANDING!");
    my $out = { "anchor" => "goto440" }; return $out;
  }
  if ($state->{"W"} <= 10) {
    gameMessage("GOOD LANDING (COULD RE BETTER)");
    my $out = { "anchor" => "goto440" }; return $out;
  }
  if ($state->{"W"} > 60) {
    my $out = { "anchor" => "goto300" }; return $out;
  }
  gameMessage("CRAFT DAMAGE... YOU'RE STRANDED HERE UNTIL A RESCUE" );
  gameMessage("PARTY ARRIVES. HOPE YOU HAVE ENOUGH OXYGEN!" );
  my $out = { "anchor" => "goto440" }; return $out;
}

#300 PRINT "SORRY THERE NERE NO SURVIVORS. YOU BLOW IT!"
#310 PRINT "IN FACT, YOU BLASTED A NEW LUNAR CRATER";W*.227;"FEET DEEP!"
#320 GOTO 440
sub goto300 {
  gameMessage("SORRY THERE NERE NO SURVIVORS. YOU BLOW IT!");
  gameMessage("IN FACT, YOU BLASTED A NEW LUNAR CRATER ".sprintf('%.1f', $state->{"W"}*.227)." FEET DEEP!");
  my $out = { "anchor" => "goto440" }; return $out;
}

#330 L=L+S: T=T-S: M=M-S*K: A=I: V=J: RETURN
sub gosub330 {
  $state->{"L"} = $state->{"L"}+$state->{"S"};
  $state->{"T"} = $state->{"T"}-$state->{"S"};
  $state->{"M"} = $state->{"M"}-$state->{"S"}*$state->{"K"};
  $state->{"A"} = $state->{"I"};
  $state->{"V"} = $state->{"J"};
  return;
}

#340 IF S<5E-03 THEN 260
#350 D=V+SQR(V*V+2*A*(G-Z*K/M)):S=2*A/D
#360 GOSUB 420: GOSUB 330: GOTO 340
sub goto340 {
  if ($state->{"S"} <= 5E-03) { my $out = { "anchor" => "goto260" }; return $out; }
  $state->{"D"} = $state->{"V"}+sqrt($state->{"V"}*$state->{"V"}+2*$state->{"A"}*($state->{"G"}-$state->{"Z"}*$state->{"K"}/$state->{"M"}));
  $state->{"S"} = 2*$state->{"A"}/$state->{"D"};
  gosub420(); gosub330();
  my $out = { "anchor" => "goto340" }; return $out;
}

#370 W=(1-M*G/(Z*K))/2: S=M*V/(Z*K*(W+SQR(W*W+V/Z)))+.05:GOSUB 420
#380 IF I<=0 THEN 340
#390 GOSUB 330: IF J>0 THEN 160
#400 IF V>0 THEN 370
#410 GOTO 160
sub goto370 {
  $state->{"W"} = (1-$state->{"M"}*$state->{"G"}/($state->{"Z"}*$state->{"K"}))/2;
  $state->{"S"} = $state->{"M"}*$state->{"V"}/($state->{"Z"}*$state->{"K"}*($state->{"W"}+sqrt($state->{"W"}*$state->{"W"}+$state->{"V"}/$state->{"Z"})))+0.05;
  gosub420();
  if ($state->{"I"} <= 0) { my $out = { "anchor" => "goto340" }; return $out; }
  gosub330();
  if ($state->{"J"} > 0) { my $out = { "anchor" => "goto160" }; return $out; }
  if ($state->{"V"} > 0) { my $out = { "anchor" => "goto370" }; return $out; }
  my $out = { "anchor" => "goto160" }; return $out;
}

#420 Q=S*K/M: J=V+G*S+Z*(-Q-Q*Q/2-Q^3/3-Q^4/4-Q^5/5)
#430 I=A-G*S*S/2-V*S+Z*S*(Q/2+Q^2/6+Q^3/12+Q^4/20+Q^5/30):RETURN
sub gosub420 {
  $state->{"Q"} = $state->{"S"}*$state->{"K"}/$state->{"M"};
  $state->{"J"} = $state->{"V"}+$state->{"G"}*$state->{"S"}+$state->{"Z"}*(-$state->{"Q"}-$state->{"Q"}*$state->{"Q"}/2-$state->{"Q"}**3/3-$state->{"Q"}**4/4-$state->{"Q"}**5/5);
  $state->{"I"} = $state->{"A"}-$state->{"G"}*$state->{"S"}*$state->{"S"}/2-$state->{"V"}*$state->{"S"}+$state->{"Z"}*$state->{"S"}*($state->{"Q"}/2+$state->{"Q"}**2/6+$state->{"Q"}**3/12+$state->{"Q"}**4/20+$state->{"Q"}**5/30);
  return;
  return;
}

#440 PRINT:PRINT:PRINT:PRINT "TRY AGAIN??": GOTO 70
sub goto440 {
  blank(3);
  gameMessage("TRY AGAIN??");
  my $out = { "anchor" => "goto70" }; return $out;
}

1;

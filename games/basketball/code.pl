#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

  #(<<<GOTO>> ) 2010, 420, 3000, 455, 460, 430, 1000, 510, 370, ONZ1040,1040, 1300, 8000, 1046, 1050, 1090, 1200, 1130, 1145, 1158, 5100, 1190, 1250, 1242, 1270, 1304, 1305, 1700, 1360, 1500, 1415, 1440, 1600, 1630, 1330, 425, 3018, 3500, 3100, 3200, 3150, 5000, 3175, 3310, 3800, 3600, 3110, 3520, 4050, 4100, 4040, 5010, 3165, 5120, 1160
  #(<<<GOSUB>> ) 600, 7000, 4000, 6000, 6010

sub initialize {
  my $out = { "anchor" => "goto5" }; return $out;
  my @a = ();
  $state->{"Sa"} = dclone(\@a);
}

sub goto5 {
  # 5  PRINT TAB(31);"BASKETBALL"
  gameMessageTab(31, "BASKETBALL");
  # 7  PRINT TAB(15);"CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY"
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  # 8  PRINT
  blank();
  #(8) PRINT
  blank();
  #(8) PRINT
  blank();
  # 10  PRINT "THIS IS DARTMOUTH COLLEGE BASKETBALL.  YOU WILL BE DARTMOUTH"
  gameMessage( "THIS IS DARTMOUTH COLLEGE BASKETBALL.  YOU WILL BE DARTMOUTH");
  # 20  PRINT " CAPTAIN AND PLAYMAKER.  CALL SHOTS AS FOLLOWS:  1. LONG"
  gameMessage( " CAPTAIN AND PLAYMAKER.  CALL SHOTS AS FOLLOWS  1. LONG");
  # 30  PRINT " (30 FT.) JUMP SHOT; 2. SHORT (15 FT.) JUMP SHOT; 3. LAY"
  gameMessage( " (30 FT.) JUMP SHOT; 2. SHORT (15 FT.) JUMP SHOT; 3. LAY");
  # 40  PRINT " UP; 4. SET SHOT."
  gameMessage( " UP; 4. SET SHOT.");
  # 60  PRINT "BOTH TEAMS WILL USE THE SAME DEFENSE.  CALL DEFENSE AS"
  # 70  PRINT "FOLLOWS
  #(70)   6. PRESS; 6.5 MAN-TO MAN; 7. ZONE; 7.5 NONE."
  gameMessage( "BOTH TEAMS WILL USE THE SAME DEFENSE.  CALL DEFENSE AS");
  gameMessage("FOLLOWS:  6. PRESS; 6.5 MAN-TO MAN; 7. ZONE; 7.5 NONE.");
  # 72  PRINT "TO CHANGE DEFENSE, JUST TYPE 0 AS YOUR NEXT SHOT."
  gameMessage( " TO CHANGE DEFENSE, JUST TYPE 0 AS YOUR NEXT SHOT.");
  # 76  INPUT "YOUR STARTING DEFENSE WILL BE";D
  gameMessage("YOUR STARTING DEFENSE WILL BE?");
  my @argv = ("number");
  my $out = {
    "anchor" => "goto76postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto76postinput {
  $state->{"D"} = $input->{"action"}[0];
  #(76) IF D<6 THEN 2010
  if ( $state->{"D"} < 6 ) { my $out = { "anchor" => "goto2010" }; return $out; }
  # 79  PRINT
  blank();
  # 80  INPUT "CHOOSE YOUR OPPONENT";O$
  gameMessage("CHOOSE YOUR OPPONENT?");
  my $out = {
    "anchor" => "goto80postinput",
    "echo" => " > ",
    "argc" => 1,
    "hook" => "reply"
  };
  return $out;
}

sub goto80postinput {
  $state->{"Os"} = join(' ', @{$input->{"action"}});
  $state->{"Sa"}[0] = 0;
  $state->{"Sa"}[1] = 0;
  my $out = { "anchor" => "goto370" }; return $out;
}

sub goto370 {
  # 370  PRINT "CENTER JUMP"
  gameMessage( "CENTER JUMP");
  # 390  IF RND(1)> 3/5 THEN 420
  if ( rand() > 3/5 ) { my $out = { "anchor" => "goto420" }; return $out; }
  # 400  PRINT O$;" CONTROLS THE TAP."
  gameMessage( $state->{"Os"}." CONTROLS THE TAP.");
  # 410  GOTO 3000
  my $out = { "anchor" => "goto3000" }; return $out;
}

sub goto420 {
  # 420  PRINT "DARTMOUTH CONTROLS THE TAP."
  gameMessage( "DARTMOUTH CONTROLS THE TAP.");
  my $out = { "anchor" => "goto425" }; return $out;
}

sub goto425 {
  # 425  PRINT
  blank();
  my $out = { "anchor" => "goto430" }; return $out;
}

sub goto430 {
  # 430  INPUT "YOUR SHOT";Z
  gameMessage("YOUR SHOT?");
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto430postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto430postinput {
  $state->{"Z"} = $input->{"action"}[0];
  # 440  P=0
  $state->{"P"} = 0;
  # 445  IF Z<>INT(Z) THEN 455
  if ( $state->{"Z"} != int($state->{"Z"}) ) { my $out = { "anchor" => "goto455" }; return $out; }
  # 446  IF Z<0 OR Z>4 THEN 455
  if (( $state->{"Z"} < 0 ) or ( $state->{"Z"} > 4 )) { my $out = { "anchor" => "goto455" }; return $out; }
  # 447  GOTO 460
  my $out = { "anchor" => "goto460" }; return $out;
}

sub goto455 {
  # 455  PRINT "INCORRECT ANSWER.  RETYPE IT. ";
  gameMessage( "INCORRECT ANSWER.  RETYPE IT. ");
  #(455) GOTO 430
  my $out = { "anchor" => "goto430" }; return $out;
}

sub goto460 {
  # 460  IF RND(1)<.5 THEN 1000
  if ( rand() < .5 ) { my $out = { "anchor" => "goto1000" }; return $out; }
  # 480  IF T<100 THEN 1000
  if ( $state->{"T"} < 100 ) { my $out = { "anchor" => "goto1000" }; return $out; }
  # 490  PRINT
  blank();
  # 491  IF S(1)<>S(0) THEN 510
  if ( $state->{"Sa"}[1] != $state->{"Sa"}[0] ) { my $out = { "anchor" => "goto510" }; return $out; }
  # 492  PRINT
  blank();
  #(492) PRINT "   ***** END OF SECOND HALF *****"
  gameMessage( "   ***** END OF SECOND HALF *****");
  #(492) PRINT
  blank();
  # 493  PRINT "SCORE AT END OF REGULATION TIME
  gameMessage( "SCORE AT END OF REGULATION TIME:  DARTMOUTH ".$state->{"Sa"}[1]."  ".$state->{"Os"}." ".$state->{"Sa"}[0]);
  #(493) "
  # 494  PRINT "        DARTMOUTH
  #(494) ";S(1);"  ";O$;"
  #(494) ";S(0)
  # 495  PRINT
  blank();
  # 496  PRINT "BEGIN TWO MINUTE OVERTIME PERIOD"
  gameMessage( "BEGIN TWO MINUTE OVERTIME PERIOD");
  # 499  T=93
  $state->{"T"} = 93;
  # 500  GOTO 370
  my $out = { "anchor" => "goto370" }; return $out;
}

sub goto510 {
  # 510  PRINT "   ***** END OF GAME *****"
  gameMessage( "   ***** END OF GAME *****");
  # 515  PRINT "FINAL SCORE
  gameMessage( "FINAL SCORE:");
  #(515)  DARTMOUTH
  #(515) ";S(1);"  ";O$;"
  #(515) ";S(0)
  gameMessage( "        DARTMOUTH ".$state->{"Sa"}[1]."  ".$state->{"Os"}." ".$state->{"Sa"}[0]);
  # 520  STOP
  my $out = { "anchor" => "quit_game" }; return $out;
}

sub gosub600 {
  # 600  PRINT
  blank();
  # 610  PRINT "   *** TWO MINUTES LEFT IN THE GAME ***"
  gameMessage( "   *** TWO MINUTES LEFT IN THE GAME ***");
  # 620  PRINT
  blank();
  # 630  RETURN
  return;
  my $out = { "anchor" => "goto1000" }; return $out;
}

sub goto1000 {
  # 1000  ON Z GOTO 1040,1040
  if ( $state->{"Z"} == 1 ) { my $out = { "anchor" => "goto1040" }; return $out; }
  if ( $state->{"Z"} == 2 ) { my $out = { "anchor" => "goto1040" }; return $out; }
  # 1030  GOTO 1300
  my $out = { "anchor" => "goto1300" }; return $out;
}

sub goto1040 {
  # 1040  T=T+1
  $state->{"T"} = $state->{"T"}+1;
  # 1041  IF T=50 THEN 8000
  if ( $state->{"T"} == 50 ) { my $out = { "anchor" => "goto8000" }; return $out; }
  # 1042  IF T=92 THEN 1046
  if ( $state->{"T"} == 92 ) { my $out = { "anchor" => "goto1046" }; return $out; }
  # 1043  GOTO 1050
  my $out = { "anchor" => "goto1050" }; return $out;
}

sub goto1046 {
  # 1046  GOSUB 600
  gosub600();
  my $out = { "anchor" => "goto1050" }; return $out;
}

sub goto1050 {
  # 1050  PRINT "JUMP SHOT"
  gameMessage( "JUMP SHOT");
  # 1060  IF RND(1)>.341*D/8 THEN 1090
  if ( rand() > .341*$state->{"D"}/8 ) { my $out = { "anchor" => "goto1090" }; return $out; }
  # 1070  PRINT "SHOT IS GOOD."
  gameMessage( "SHOT IS GOOD.");
  # 1075  GOSUB 7000
  gosub7000();
  # 1085  GOTO 3000
  my $out = { "anchor" => "goto3000" }; return $out;
}

sub goto1090 {
  # 1090  IF RND(1)>.682*D/8 THEN 1200
  if ( rand() > .682*$state->{"D"}/8 ) { my $out = { "anchor" => "goto1200" }; return $out; }
  # 1100  PRINT "SHOT IS OFF TARGET."
#  gameMessage( "SHOT IS OFF TARGET.");
  gameMessage( "AIRBALL.");
  # 1105  IF D/6*RND(1)>.45 THEN 1130
  if ( $state->{"D"}/6*rand() > .45 ) { my $out = { "anchor" => "goto1130" }; return $out; }
  # 1110  PRINT "DARTMOUTH CONTROLS THE REBOUND."
  gameMessage( "DARTMOUTH CONTROLS THE REBOUND.");
  # 1120  GOTO 1145
  my $out = { "anchor" => "goto1145" }; return $out;
}

sub goto1130 {
  # 1130  PRINT "REBOUND TO ";O$
  gameMessage( "REBOUND TO ".$state->{"Os"});
  # 1140  GOTO 3000
  my $out = { "anchor" => "goto3000" }; return $out;
}

sub goto1145 {
  # 1145  IF RND(1)>.4 THEN 1158
  if ( rand() > .4 ) { my $out = { "anchor" => "goto1158" }; return $out; }
  # 1150  GOTO 1300
  my $out = { "anchor" => "goto1300" }; return $out;
}

sub goto1158 {
  # 1158  IF D=6 THEN 5100
  if ( $state->{"D"} == 6 ) { my $out = { "anchor" => "goto5100" }; return $out; }
  my $out = { "anchor" => "goto1160" }; return $out;
}

sub goto1160 {
  # 1160  PRINT "BALL PASSED BACK TO YOU. ";
  gameMessage( "BALL PASSED BACK TO YOU. ");
  # 1170  GOTO 430
  my $out = { "anchor" => "goto430" }; return $out;
  # 1180  IF RND(1)>.9 THEN 1190
  if ( rand() > .9 ) { my $out = { "anchor" => "goto1190" }; return $out; }
  # 1185  PRINT "PLAYER FOULED, TWO SHOTS."
  gameMessage( "PLAYER FOULED, TWO SHOTS.");
  # 1187  GOSUB 4000
  gosub4000();
  # 1188  GOTO 3000
  my $out = { "anchor" => "goto3000" }; return $out;
}

sub goto1190 {
  # 1190  PRINT "BALL STOLEN. ";O$;"'S BALL."
  gameMessage( "BALL STOLEN. ".$state->{"Os"}."'S BALL.");
  # 1195  GOTO 3000
  my $out = { "anchor" => "goto3000" }; return $out;
}

sub goto1200 {
  # 1200  IF RND(1)>.782*D/8 THEN 1250
  if ( rand() > .782*$state->{"D"}/8 ) { my $out = { "anchor" => "goto1250" }; return $out; }
  # 1210  PRINT "SHOT IS BLOCKED.  BALL CONTROLLED BY ";
  # 1230  IF RND(1)>.5 THEN 1242
  if ( rand() > .5 ) { my $out = { "anchor" => "goto1242" }; return $out; }
  # 1235  PRINT "DARTMOUTH."
  gameMessage( "SHOT IS BLOCKED.  BALL CONTROLLED BY DARTMOUTH.");
  # 1240  GOTO 430
  my $out = { "anchor" => "goto430" }; return $out;
}

sub goto1242 {
  # 1242  PRINT O$;"."
  gameMessage( "SHOT IS BLOCKED.  BALL CONTROLLED BY ".$state->{"Os"}.".");
  # 1245  GOTO 3000
  my $out = { "anchor" => "goto3000" }; return $out;
}

sub goto1250 {
  # 1250  IF RND(1)>.843*D/8 THEN 1270
  if ( rand()>.843*$state->{"D"}/8 ) { my $out = { "anchor" => "goto1270" }; return $out; }
  # 1255  PRINT "SHOOTER IS FOULED.  TWO SHOTS."
  gameMessage( "SHOOTER IS FOULED.  TWO SHOTS.");
  # 1260  GOSUB 4000
  gosub4000();
  # 1265  GOTO 3000
  my $out = { "anchor" => "goto3000" }; return $out;
}

sub goto1270 {
  # 1270  PRINT "CHARGING FOUL.  DARTMOUTH LOSES BALL."
  gameMessage( "CHARGING FOUL.  DARTMOUTH LOSES BALL.");
  # 1280  GOTO 3000
  my $out = { "anchor" => "goto3000" }; return $out;
}

sub goto1300 {
  # 1300  T=T+1
  $state->{"T"} = $state->{"T"}+1;
  # 1301  IF T=50 THEN 8000
  if ( $state->{"T"} == 50 ) { my $out = { "anchor" => "goto8000" }; return $out; }
  # 1302  IF T=92 THEN 1304
  if ( $state->{"T"} == 92 ) { my $out = { "anchor" => "goto1304" }; return $out; }
  # 1303  GOTO 1305
  my $out = { "anchor" => "goto1305" }; return $out;
}

sub goto1304 {
  # 1304  GOSUB 600
  gosub600();
  my $out = { "anchor" => "goto1305" }; return $out;
}

sub goto1305 {
  # 1305  IF Z=0 THEN 2010
  if ( $state->{"Z"} == 0 ) { my $out = { "anchor" => "goto2010" }; return $out; }
  # 1310  IF Z>3 THEN 1700
  if ( $state->{"Z"} > 3 ) { my $out = { "anchor" => "goto1700" }; return $out; }
  # 1320  PRINT "LAY UP."
  gameMessage( "LAY UP.");
  my $out = { "anchor" => "goto1330" }; return $out;
}

sub goto1330 {
  # 1330  IF 7/D*RND(1)>.4 THEN 1360
  if ( 7/$state->{"D"}*rand()>.4 ) { my $out = { "anchor" => "goto1360" }; return $out; }
  # 1340  PRINT "SHOT IS GOOD.  TWO POINTS."
  gameMessage( "SHOT IS GOOD.  TWO POINTS.");
  # 1345  GOSUB 7000
  gosub7000();
  # 1355  GOTO 3000
  my $out = { "anchor" => "goto3000" }; return $out;
}

sub goto1360 {
  # 1360  IF 7/D*RND(1)>.7 THEN 1500
  if ( 7/$state->{"D"}*rand()>.7 ) { my $out = { "anchor" => "goto1500" }; return $out; }
  # 1370  PRINT "SHOT IS OFF THE RIM."
  gameMessage( "SHOT IS OFF THE RIM.");
  # 1380  IF RND(1)>2/3 THEN 1415
  if ( rand() > 2/3 ) { my $out = { "anchor" => "goto1415" }; return $out; }
  # 1390  PRINT O$;" CONTROLS THE REBOUND."
  gameMessage( $state->{"Os"}." CONTROLS THE REBOUND.");
  # 1400  GOTO 3000
  my $out = { "anchor" => "goto3000" }; return $out;
}

sub goto1415 {
  # 1415  PRINT "DARTMOUTH CONTROLS THE REBOUND."
  gameMessage( "DARTMOUTH CONTROLS THE REBOUND.");
  # 1420  IF RND(1)>.4 THEN 1440
  if ( rand()>.4 ) { my $out = { "anchor" => "goto1440" }; return $out; }
  # 1430  GOTO 1300
  my $out = { "anchor" => "goto1300" }; return $out;
}

sub goto1440 {
  # 1440  PRINT "BALL PASSED BACK TO YOU.";
  gameMessage( "BALL PASSED BACK TO YOU.");
  # 1450  GOTO 430
  my $out = { "anchor" => "goto430" }; return $out;
}

sub goto1500 {
  # 1500  IF 7/D*RND(1)>.875 THEN 1600
  if ( 7/$state->{"D"}*rand()>.875 ) { my $out = { "anchor" => "goto1600" }; return $out; }
  # 1510  PRINT "SHOOTER FOULED.  TWO SHOTS."
  gameMessage( "SHOOTER FOULED.  TWO SHOTS.");
  # 1520  GOSUB 4000
  gosub4000();
  # 1530  GOTO 3000
  my $out = { "anchor" => "goto3000" }; return $out;
}

sub goto1600 {
  # 1600  IF 7/D*RND(1)>.925 THEN 1630
  if ( 7/$state->{"D"}*rand()>.925 ) { my $out = { "anchor" => "goto1630" }; return $out; }
  # 1610  PRINT "SHOT BLOCKED. ";O$;"'S BALL."
  gameMessage( "SHOT BLOCKED. ".$state->{"Os"}."'S BALL.");
  # 1620  GOTO 3000
  my $out = { "anchor" => "goto3000" }; return $out;
}

sub goto1630 {
  # 1630  PRINT "CHARGING FOUL.  DARTMOUTH LOSES THE BALL."
  gameMessage( "CHARGING FOUL.  DARTMOUTH LOSES THE BALL.");
  # 1640  GOTO 3000
  my $out = { "anchor" => "goto3000" }; return $out;
}

sub goto1700 {
  # 1700  PRINT "SET SHOT."
  gameMessage( "SET SHOT.");
  # 1710  GOTO 1330
  my $out = { "anchor" => "goto1330" }; return $out;
}

sub goto2010 {
  # 2010  INPUT "YOUR NEW DEFENSIVE ALLIGNMENT IS";D
  gameMessage("YOUR NEW DEFENSIVE ALLIGNMENT IS?");
  my @argv = ("number");
  my $out = {
    "anchor" => "goto2010postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto2010postinput {
  $state->{"D"} = $input->{"action"}[0];
  # 2030  IF D<6 THEN 2010
  if ( $state->{"D"} < 6 ) { my $out = { "anchor" => "goto2010" }; return $out; }
  # 2040  GOTO 425
  my $out = { "anchor" => "goto425" }; return $out;
}

sub goto3000 {
  # 3000  P=1
  $state->{"P"} = 1;
  # 3005  T=T+1
  $state->{"T"} = $state->{"T"}+1;
  # 3008  IF T=50 THEN 8000
  if ( $state->{"T"} == 50 ) { my $out = { "anchor" => "goto8000" }; return $out; }
  # 3012  GOTO 3018
  my $out = { "anchor" => "goto3018" }; return $out;
  # 3015  GOSUB 600
  gosub600();
  my $out = { "anchor" => "goto3018" }; return $out;
}

sub goto3018 {
  # 3018  PRINT
  blank();
  # 3020  Z1=10/4*RND(1)+1
  $state->{"Z1"} = 10/4*rand()+1;
  # 3030  IF Z1>2 THEN 3500
  if ( $state->{"Z1"} > 2 ) { my $out = { "anchor" => "goto3500" }; return $out; }
  # 3040  PRINT "JUMP SHOT."
  gameMessage( "JUMP SHOT.");
  # 3050  IF 8/D*RND(1)>.35 THEN 3100
  if ( 8/$state->{"D"}*rand()>.35 ) { my $out = { "anchor" => "goto3100" }; return $out; }
  # 3060  PRINT "SHOT IS GOOD."
  gameMessage( "SHOT IS GOOD.");
  # 3080  GOSUB 6000
  gosub6000();
  # 3090  GOTO 425
  my $out = { "anchor" => "goto425" }; return $out;
}

sub goto3100 {
  # 3100  IF 8/D*RND(1)>.75 THEN 3200
  if ( 8/$state->{"D"}*rand()>.75 ) { my $out = { "anchor" => "goto3200" }; return $out; }
  # 3105  PRINT "SHOT IS OFF RIM."
  gameMessage( "SHOT IS OFF RIM.");
  my $out = { "anchor" => "goto3110" }; return $out;
}

sub goto3110 {
  # 3110  IF D/6*RND(1)>.5 THEN 3150
  if ( $state->{"D"}/6*rand()>.5 ) { my $out = { "anchor" => "goto3150" }; return $out; }
  # 3120  PRINT "DARTMOUTH CONTROLS THE REBOUND."
  gameMessage( "DARTMOUTH CONTROLS THE REBOUND.");
  # 3130  GOTO 425
  my $out = { "anchor" => "goto425" }; return $out;
}

sub goto3150 {
  # 3150  PRINT O$;" CONTROLS THE REBOUND."
  gameMessage( $state->{"Os"}." CONTROLS THE REBOUND.");
  # 3160  IF D=6 THEN 5000
  if ( $state->{"D"} == 6 ) { my $out = { "anchor" => "goto5000" }; return $out; }
  my $out = { "anchor" => "goto3165" }; return $out;
}

sub goto3165 {
  # 3165  IF RND(1)>.5 THEN 3175
  if ( rand() > .5 ) { my $out = { "anchor" => "goto3175" }; return $out; }
  # 3168  PRINT "PASS BACK TO ";O$;" GUARD."
  gameMessage( "PASS BACK TO ".$state->{"Os"}." GUARD.");
  # 3170  GOTO 3000
  my $out = { "anchor" => "goto3000" }; return $out;
}

sub goto3175 {
  # 3175  GOTO 3500
  my $out = { "anchor" => "goto3500" }; return $out;
}

sub goto3200 {
  # 3200  IF 8/D*RND(1)>.9 THEN 3310
  if ( 8/$state->{"D"}*rand() > .9 ) { my $out = { "anchor" => "goto3310" }; return $out; }
  # 3210  PRINT "PLAYER FOULED.  TWO SHOTS."
  gameMessage( "PLAYER FOULED.  TWO SHOTS.");
  # 3220  GOSUB 4000
  gosub4000();
  # 3230  GOTO 425
  my $out = { "anchor" => "goto425" }; return $out;
}

sub goto3310 {
  # 3310  PRINT "OFFENSIVE FOUL.  DARTMOUTH'S BALL."
  gameMessage( "OFFENSIVE FOUL.  DARTMOUTH'S BALL.");
  # 3320  GOTO 425
  my $out = { "anchor" => "goto425" }; return $out;
}

sub goto3500 {
  # 3500  IF Z1>3 THEN 3800
  if ( $state->{"Z1"} > 3 ) { my $out = { "anchor" => "goto3800" }; return $out; }
  # 3510  PRINT "LAY UP."
  gameMessage( "LAY UP.");
  my $out = { "anchor" => "goto3520" }; return $out;
}

sub goto3520 {
  # 3520  IF 7/D*RND(1)>.413 THEN 3600
  if ( 7/$state->{"D"}*rand()>.413 ) { my $out = { "anchor" => "goto3600" }; return $out; }
  # 3530  PRINT "SHOT IS GOOD."
  gameMessage( "SHOT IS GOOD.");
  # 3540  GOSUB 6000
  gosub6000();
  # 3550  GOTO 425
  my $out = { "anchor" => "goto425" }; return $out;
}

sub goto3600 {
  # 3600  PRINT "SHOT IS MISSED."
  gameMessage( "SHOT IS MISSED.");
  # 3610  GOTO 3110
  my $out = { "anchor" => "goto3110" }; return $out;
}

sub goto3800 {
  # 3800  PRINT "SET SHOT."
  gameMessage( "SET SHOT.");
  # 3810  GOTO 3520
  my $out = { "anchor" => "goto3520" }; return $out;
}

# 4000  REM *** FOUL SHOOTING ***

sub gosub4000 {
  # 4010  IF RND(1)>.49 THEN 4050
  if ( rand() > .49 ) { #my $out = { "anchor" => "goto4050" }; return $out; }

#   sub goto4050 {
      # 4050  IF RND(1)>.75 THEN 4100
      if ( rand()>.75 ) { #my $out = { "anchor" => "goto4100" }; return $out; }
#        sub goto4100 {
          # 4100  PRINT "BOTH SHOTS MISSED."
          gameMessage( "BOTH SHOTS MISSED.");
          # 4110  GOTO 4040
#          my $out = { "anchor" => "goto4040" }; return $out;
          # 4040  GOSUB 6010
          gosub6010();
          # 4041  RETURN
          return;
        }
#      # 4060  PRINT "SHOOTER MAKES ONE SHOT AND MISSES ONE."
      gameMessage( "SHOOTER MAKES ONE SHOT AND MISSES ONE.");
      # 4070  S(1-P)=S(1-P)+1
      $state->{"Sa"}[1-$state->{"P"}] = $state->{"Sa"}[1-$state->{"P"}]+1;
      # 4080  GOTO 4040
#      my $out = { "anchor" => "goto4040" }; return $out;
      # 4040  GOSUB 6010
      gosub6010();
      # 4041  RETURN
      return;
  }

  # 4020  PRINT "SHOOTER MAKES BOTH SHOTS."
  gameMessage( "SHOOTER MAKES BOTH SHOTS.");
  # 4030  S(1-P)=S(1-P)+2
  $state->{"Sa"}[1-$state->{"P"}] = $state->{"Sa"}[1-$state->{"P"}]+2;
#  my $out = { "anchor" => "goto4040" }; return $out;
#}
#
#sub goto4040 {
  # 4040  GOSUB 6010
  gosub6010();
  # 4041  RETURN
  return;
#  my $out = { "anchor" => "goto4050" }; return $out;
}

#sub goto4050 {
  # 4050  IF RND(1)>.75 THEN 4100
#  if ( rand()>.75 ) { my $out = { "anchor" => "goto4100" }; return $out; }
  # 4060  PRINT "SHOOTER MAKES ONE SHOT AND MISSES ONE."
#  gameMessage( "SHOOTER MAKES ONE SHOT AND MISSES ONE.");
  # 4070  S(1-P)=S(1-P)+1
#  $state->{"S"}[1-P] = S(1-P)+1;
  # 4080  GOTO 4040
#  my $out = { "anchor" => "goto4040" }; return $out;
#}

#sub goto4100 {
  # 4100  PRINT "BOTH SHOTS MISSED."
#  gameMessage( "BOTH SHOTS MISSED.");
  # 4110  GOTO 4040
#  my $out = { "anchor" => "goto4040" }; return $out;
#}

sub goto5000 {
  # 5000  IF RND(1)>.75 THEN 5010
  if ( rand() > .75 ) { my $out = { "anchor" => "goto5010" }; return $out; }
  # 5005  GOTO 3165
  my $out = { "anchor" => "goto3165" }; return $out;
}

sub goto5010 {
  # 5010  PRINT "BALL STOLEN.  EASY LAY UP FOR DARTMOUTH."
  gameMessage( "BALL STOLEN.  EASY LAY UP FOR DARTMOUTH.");
  # 5015  GOSUB 7000
  gosub7000();
  # 5030  GOTO 3000
  my $out = { "anchor" => "goto3000" }; return $out;
}

sub goto5100 {
  # 5100  IF RND(1)>.6 THEN 5120
  if ( rand() > .6 ) { my $out = { "anchor" => "goto5120" }; return $out; }
  # 5110  GOTO 1160
  my $out = { "anchor" => "goto1160" }; return $out;
}

sub goto5120 {
  # 5120  PRINT "PASS STOLEN BY ";O$;" EASY LAYUP."
  gameMessage( "PASS STOLEN BY ".$state->{"Os"}." EASY LAYUP.");
  # 5130  GOSUB 6000
  gosub6000();
  # 5140  GOTO 425
  my $out = { "anchor" => "goto425" }; return $out;
}

sub gosub6000 {
  # 6000  S(0)=S(0)+2
  $state->{"Sa"}[0] = $state->{"Sa"}[0]+2;
#  my $out = { "anchor" => "goto6010" }; return $out;
  # 6010  PRINT "SCORE
  gameMessage( "SCORE  ".$state->{"Sa"}[1]." TO ".$state->{"Sa"}[0]);
  #(6010)  ";S(1);"TO";S(0)
  # 6020  RETURN
  return;
}

sub gosub6010 {
  # 6010  PRINT "SCORE
  gameMessage( "SCORE  ".$state->{"Sa"}[1]." TO ".$state->{"Sa"}[0]);
  #(6010)  ";S(1);"TO";S(0)
  # 6020  RETURN
  return;
#  my $out = { "anchor" => "goto7000" }; return $out;
}

sub gosub7000 {
  # 7000  S(1)=S(1)+2
  $state->{"Sa"}[1] = $state->{"Sa"}[1]+2;
  # 7010  GOSUB 6010
  gosub6010();
  # 7020  RETURN
  return;
}

sub goto8000 {
  # 8000  PRINT
  blank();
  #(8000) PRINT "   ***** END OF FIRST HALF *****"
  gameMessage( "   ***** END OF FIRST HALF *****");
  #(8000) PRINT
  blank();
  # 8010  PRINT "SCORE
  gameMessage( "SCORE:  DARTMOUTH ".$state->{"Sa"}[1]."  ".$state->{"Os"}." ".$state->{"Sa"}[0]);
  #(8010)  DARTMOUTH
  #(8010) ";S(1);"  ";O$;"
  #(8010) ";S(0)
  # 8015  PRINT
  blank();
  # 8016  PRINT
  blank();
  # 8020  GOTO 370
  my $out = { "anchor" => "goto370" }; return $out;
  # 9999  END
}

1;

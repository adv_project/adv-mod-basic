#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

use Math::Trig;

$__golfmsg__;

sub advance_pointer {
  $state->{"pointer"} = $state->{"pointer"}+1;
  if ($state->{"pointer"} >= @{$state->{"DATA"}}) { $state->{"pointer"} = 0; }
}

sub initialize {
  $state->{"pointer"} = 0;
  my @d = ();
  $state->{"DATA"} = dclone(\@d);
  # 1700  DATA 361,4,4,2,389,4,3,3,206,3,4,2,500,5,7,2
  # 1702  DATA 408,4,2,4,359,4,6,4,424,4,4,2,388,4,4,4
  # 1704  DATA 196,3,7,2,400,4,7,2,560,5,7,2,132,3,2,2
  # 1706  DATA 357,4,4,4,294,4,2,4,475,5,2,3,375,4,4,2
  # 1708  DATA 180,3,6,2,550,5,6,6
  push(@{$state->{"DATA"}}, (361,4,4,2,389,4,3,3,206,3,4,2,500,5,7,2));
  push(@{$state->{"DATA"}}, (408,4,2,4,359,4,6,4,424,4,4,2,388,4,4,4));
  push(@{$state->{"DATA"}}, (196,3,7,2,400,4,7,2,560,5,7,2,132,3,2,2));
  push(@{$state->{"DATA"}}, (357,4,4,4,294,4,2,4,475,5,2,3,375,4,4,2));
  push(@{$state->{"DATA"}}, (180,3,6,2,550,5,6,6));
  my $out = { "anchor" => "goto1" }; return $out;
}

sub goto1 {
  # 1  PRINT TAB(34);"GOLF"
  gameMessageTab(34, "GOLF");
  # 2  PRINT TAB(15);"CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY"
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  # 3  PRINT
  blank();
  #(3) PRINT
  blank();
  #(3) PRINT
  blank();
  # 4  PRINT "WELCOME TO THE CREATIVE COMPUTING COUNTRY CLUB,"
  gameMessage( "WELCOME TO THE CREATIVE COMPUTING COUNTRY CLUB,");
  # 5  PRINT "AN EIGHTEEN HOLE CHAMPIONSHIP LAYOUT LOCATED A SHORT"
  gameMessage( "AN EIGHTEEN HOLE CHAMPIONSHIP LAYOUT LOCATED A SHORT");
  # 6  PRINT "DISTANCE FROM SCENIC DOWNTOWN MORRISTOWN.  THE"
  gameMessage( "DISTANCE FROM SCENIC DOWNTOWN MORRISTOWN.  THE");
  # 7  PRINT "COMMENTATOR WILL EXPLAIN THE GAME AS YOU PLAY."
  gameMessage( "COMMENTATOR WILL EXPLAIN THE GAME AS YOU PLAY.");
  # 8  PRINT "ENJOY YOUR GAME; SEE YOU AT THE 19TH HOLE..."
  gameMessage( "ENJOY YOUR GAME; SEE YOU AT THE 19TH HOLE...");
  # 9  PRINT
  blank();
  #(9) PRINT
  blank();
  #(9)  DIM L(10)
  my @a = ();
  $state->{"La"} = dclone(\@a);
  # 10  G1=18
  $state->{"G1"} = 18;
  # 20  G2=0
  $state->{"G2"} = 0;
  # 30  G3=0
  $state->{"G3"} = 0;
  # 40  A=0
  $state->{"A"} = 0;
  # 50  N=.8
  $state->{"N"} = 0.8;
  # 60  S2=0
  $state->{"S2"} = 0;
  # 70  F=1
  $state->{"F"} = 1;
  my $out = { "anchor" => "goto80" }; return $out;
}

sub goto80 {
  # 80  PRINT "WHAT IS YOUR HANDICAP";
  gameMessage( "WHAT IS YOUR HANDICAP");
  # 90  INPUT H
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto90postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto90postinput {
  $state->{"H"} = $input->{"action"}[0];
  #(90) PRINT
  blank();
  # 100  IF H>30 THEN 470
  if ( $state->{"H"} > 30 ) { my $out = { "anchor" => "goto470" }; return $out; }
  # 110  IF H<0 THEN 470
  if ( $state->{"H"} < 0 ) { my $out = { "anchor" => "goto470" }; return $out; }
  my $out = { "anchor" => "goto120" }; return $out;
}

sub goto120 {
  # 120  PRINT "DIFFICULTIES AT GOLF INCLUDE
  gameMessage( "DIFFICULTIES AT GOLF INCLUDE:");
  # 130  PRINT "0=HOOK, 1=SLICE, 2=POOR DISTANCE, 4=TRAP SHOTS, 5=PUTTING"
  gameMessage( "0=HOOK, 1=SLICE, 2=POOR DISTANCE, 4=TRAP SHOTS, 5=PUTTING");
  # 140  PRINT "WHICH ONE (ONLY ONE) IS YOUR WORST";
  gameMessage( "WHICH ONE (ONLY ONE) IS YOUR WORST");
  # 150  INPUT T
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto150postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto150postinput {
  $state->{"T"} = $input->{"action"}[0];
  #(150) PRINT
  blank();
  # 160  IF T>5 THEN 120
  if ( $state->{"T"} > 5 ) { my $out = { "anchor" => "goto120" }; return $out; }
  # 170  S1=0
  $state->{"S1"} = 0;
  # 210  REM
  my $out = { "anchor" => "goto230" }; return $out;
}

sub goto230 {
  # 230  L(0)=0
  $state->{"La"}[0] = 0;
  # 240  J=0
  $state->{"J"} = 0;
  # 245  Q=0
  $state->{"Q"} = 0;
  # 250  S2=S2+1
  $state->{"S2"} = $state->{"S2"}+1;
  # 260  K=0
  $state->{"K"} = 0;
  # 270  IF F=1 THEN 310
  if ( $state->{"F"} == 1 ) { my $out = { "anchor" => "goto310" }; return $out; }
  # 290  PRINT "YOUR SCORE ON HOLE";F-1;"WAS";S1
  my $f = $state->{"F"}-1;
  gameMessage( "YOUR SCORE ON HOLE ".$f." WAS ".$state->{"S1"});
  # 291  GOTO 1750
  my $out = { "anchor" => "goto1750" }; return $out;
}

sub goto292 {
  # 292  IF S1>P+2 THEN 297
  if ( $state->{"S1"} > $state->{"P"}+2 ) { my $out = { "anchor" => "goto297" }; return $out; }
  # 293  IF S1=P THEN 299
  if ( $state->{"S1"} == $state->{"P"} ) { my $out = { "anchor" => "goto299" }; return $out; }
  # 294  IF S1=P-1 THEN 301
  if ( $state->{"S1"} == $state->{"P"}-1 ) { my $out = { "anchor" => "goto301" }; return $out; }
  # 295  IF S1=P-2 THEN 303
  if ( $state->{"S1"} == $state->{"P"}-2 ) { my $out = { "anchor" => "goto303" }; return $out; }
  # 296  GOTO 310
  my $out = { "anchor" => "goto310" }; return $out;
}

sub goto297 {
  # 297  PRINT "KEEP YOUR HEAD DOWN."
  gameMessage( "KEEP YOUR HEAD DOWN.");
  # 298  GOTO 310
  my $out = { "anchor" => "goto310" }; return $out;
}

sub goto299 {
  # 299  PRINT "A PAR.  NICE GOING."
  gameMessage( "A PAR.  NICE GOING.");
  # 300  GOTO 310
  my $out = { "anchor" => "goto310" }; return $out;
}

sub goto301 {
  # 301  PRINT "A BIRDIE."
  gameMessage( "A BIRDIE.");
  # 302  GOTO 310
  my $out = { "anchor" => "goto310" }; return $out;
}

sub goto303 {
  # 303  IF P=3 THEN 306
  if ( $state->{"P"} == 3 ) { my $out = { "anchor" => "goto306" }; return $out; }
  # 304  PRINT "A GREAT BIG EAGLE."
  gameMessage( "A GREAT BIG EAGLE.");
  # 305  GOTO 310
  my $out = { "anchor" => "goto310" }; return $out;
}

sub goto306 {
  # 306  PRINT "A HOLE IN ONE."
  gameMessage( "A HOLE IN ONE.");
  my $out = { "anchor" => "goto310" }; return $out;
}

sub goto310 {
  # 310  IF F=19 THEN 1710
  if ( $state->{"F"} == 19 ) { my $out = { "anchor" => "goto1710" }; return $out; }
  # 315  S1=0
  $state->{"S1"} = 0;
  # 316  PRINT
  blank();
  my $out = { "anchor" => "goto320" }; return $out;
}

sub goto320 {
  # 320  IF S1=0 THEN 1590
  if ( $state->{"S1"} == 0 ) { my $out = { "anchor" => "goto1590" }; return $out; }
  # 330  IF L(0)<1 THEN 1150
  if ( $state->{"La"}[0] lt 1 ) { my $out = { "anchor" => "goto1150" }; return $out; }
  # 340  X=0
  $state->{"X"} = 0;
  # 350  IF L(0)>5 THEN 1190
  if ( $state->{"La"} == 5 ) { my $out = { "anchor" => "goto1190" }; return $out; }
  # 360  PRINT "SHOT WENT";D1;"YARDS.  IT'S";D2;"YARDS FROM THE CUP."
  gameMessage( "SHOT WENT ".$state->{"D1"}." YARDS.  IT'S ".$state->{"D2"}." YARDS FROM THE CUP.");
  # 362  PRINT "BALL IS";INT(O);"YARDS OFF LINE... IN ";
  $__golfmsg__ = "BALL IS ".int($state->{"O"})." YARDS OFF LINE... IN ";
  # 380  GOSUB 400
  gosub400();
  # 390  GOTO 620
  my $out = { "anchor" => "goto620" }; return $out;
}

sub gosub400 {
  # 400  IF L(X)=1 THEN 480
  if ( $state->{"La"}[$state->{"X"}] == 1 ) {
    $__golfmsg__ = $__golfmsg__."FAIRWAY.";
    gameMessage($__golfmsg__); $__golfmsg__ = '';
    return;
  }
  # 410  IF L(X)=2 THEN 500
  if ( $state->{"La"}[$state->{"X"}] == 2 ) {
    $__golfmsg__ = $__golfmsg__."ROUGH.";
    gameMessage($__golfmsg__); $__golfmsg__ = '';
    return;
  }
  # 420  IF L(X)=3 THEN 520
  if ( $state->{"La"}[$state->{"X"}] == 3 ) {
    $__golfmsg__ = $__golfmsg__."TREES.";
    gameMessage($__golfmsg__); $__golfmsg__ = '';
    return;
  }
  # 430  IF L(X)=4 THEN 540
  if ( $state->{"La"}[$state->{"X"}] == 4 ) {
    $__golfmsg__ = $__golfmsg__."ADJACENT FAIRWAY.";
    gameMessage($__golfmsg__); $__golfmsg__ = '';
    return;
  }
  # 440  IF L(X)=5 THEN 560
  if ( $state->{"La"}[$state->{"X"}] == 5 ) {
    $__golfmsg__ = $__golfmsg__."TRAP.";
    gameMessage($__golfmsg__); $__golfmsg__ = '';
    return;
  }
  # 450  IF L(X)=6 THEN 580
  if ( $state->{"La"}[$state->{"X"}] == 6 ) {
    $__golfmsg__ = $__golfmsg__."WATER.";
    gameMessage($__golfmsg__); $__golfmsg__ = '';
    return;
  }
  # 460  PRINT "OUT OF BOUNDS."
  $__golfmsg__ = $__golfmsg__."OUT OF BOUNDS.";
  gameMessage($__golfmsg__); $__golfmsg__ = '';
  # 465  GOTO 1690
  return;
}

sub goto470 {
  # 470  PRINT "PGA HANDICAPS RANGE FROM 0 TO 30."
  gameMessage( "PGA HANDICAPS RANGE FROM 0 TO 30.");
  # 472  GOTO 80
  my $out = { "anchor" => "goto80" }; return $out;
}

sub goto620 {
  # 620  IF A=1 THEN 629
  if ( $state->{"A"} == 1 ) { my $out = { "anchor" => "goto629" }; return $out; }
  # 621  PRINT "SELECTION OF CLUBS"
  gameMessage( "SELECTION OF CLUBS");
  # 622  PRINT "YARDAGE DESIRED                       SUGGESTED CLUBS"
  gameMessage( "YARDAGE DESIRED                       SUGGESTED CLUBS");
  # 623  PRINT "200 TO 280 YARDS                           1 TO 4"
  gameMessage( "200 TO 280 YARDS                           1 TO 4");
  # 624  PRINT "100 TO 200 YARDS                          19 TO 13"
  gameMessage( "100 TO 200 YARDS                          19 TO 13");
  # 625  PRINT "  0 TO 100 YARDS                          29 TO 23"
  gameMessage( "  0 TO 100 YARDS                          29 TO 23");
  # 626  A=1
  $state->{"A"} = 1;
  my $out = { "anchor" => "goto629" }; return $out;
}

sub goto629 {
  # 629  PRINT "WHAT CLUB DO YOU CHOOSE";
  gameMessage( "WHAT CLUB DO YOU CHOOSE");
  # 630  INPUT C
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto630postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto630postinput {
  $state->{"C"} = $input->{"action"}[0];
  # 632  PRINT
  blank();
  # 635  IF C<1 THEN 690
  if ( $state->{"C"} < 1 ) { my $out = { "anchor" => "goto690" }; return $out; }
  # 637  IF C>29 THEN 690
  if ( $state->{"C"} > 29 ) { my $out = { "anchor" => "goto690" }; return $out; }
  # 640  IF C>4 THEN 710
  if ( $state->{"C"} > 4 ) { my $out = { "anchor" => "goto710" }; return $out; }
  my $out = { "anchor" => "goto650" }; return $out;
}

sub goto650 {
  # 650  IF L(0)<=5 THEN 740
  if ( $state->{"La"}[0] <= 5 ) { my $out = { "anchor" => "goto740" }; return $out; }
  # 660  IF C=14 THEN 740
  if ( $state->{"C"} == 14 ) { my $out = { "anchor" => "goto740" }; return $out; }
  # 665  IF C=23 THEN 740
  if ( $state->{"C"} == 23 ) { my $out = { "anchor" => "goto740" }; return $out; }
  # 670  GOTO 690
  my $out = { "anchor" => "goto690" }; return $out;
}

sub goto680 {
  # 680  S1=S1-1
  $state->{"S1"} = $state->{"S1"}-1;
  my $out = { "anchor" => "goto690" }; return $out;
}

sub goto690 {
  # 690  PRINT "THAT CLUB IS NOT IN THE BAG."
  gameMessage( "THAT CLUB IS NOT IN THE BAG.");
  # 693  PRINT
  blank();
  # 700  GOTO 620
  my $out = { "anchor" => "goto620" }; return $out;
}

sub goto710 {
  # 710  IF C<12 THEN 690
  if ( $state->{"C"} < 12 ) { my $out = { "anchor" => "goto690" }; return $out; }
  # 720  C=C-6
  $state->{"C"} = $state->{"C"}-6;
  # 730  GOTO 650
  my $out = { "anchor" => "goto650" }; return $out;
}

sub goto740 {
  # 740  S1=S1+1
  $state->{"S1"} = $state->{"S1"}+1;
  # 741  W=1
  $state->{"W"} = 1;
  # 742  IF C>13 THEN 960
  if ( $state->{"C"} > 13 ) { my $out = { "anchor" => "goto960" }; return $out; }
  # 746  IF INT(F/3)=F/3 THEN 952
  if ( int($state->{"F"}/3) == $state->{"F"}/3 ) { my $out = { "anchor" => "goto952" }; return $out; }
  my $out = { "anchor" => "goto752" }; return $out;
}

sub goto752 {
  # 752  IF C<4 THEN 756
  if ( $state->{"C"} < 4 ) { my $out = { "anchor" => "goto756" }; return $out; }
  # 754  GOTO 760
  my $out = { "anchor" => "goto760" }; return $out;
}

sub goto756 {
  # 756  IF L(0)=2 THEN 862
  if ( $state->{"La"}[0] == 2 ) { my $out = { "anchor" => "goto862" }; return $out; }
  my $out = { "anchor" => "goto760" }; return $out;
}

sub goto760 {
  # 760  IF S1>7 THEN 867
  if ( $state->{"S1"} > 7 ) { my $out = { "anchor" => "goto867" }; return $out; }
  my $out = { "anchor" => "goto770" }; return $out;
}

sub goto770 {
  # 770  D1=INT(((30-H)*2.5+187-((30-H)*.25+15)*C/2)+25*RND(1))
  $state->{"D1"} = int(((30-$state->{"H"})*2.5+187-((30-$state->{"H"})*0.25+15)*$state->{"C"}/2)+25*rand());
  # 780  D1=INT(D1*W)
  $state->{"D1"} = int($state->{"D1"}*$state->{"W"});
  # 800  IF T=2 THEN 1170
  if ( $state->{"T"} == 2 ) { my $out = { "anchor" => "goto1170" }; return $out; }
  my $out = { "anchor" => "goto830" }; return $out;
}

sub goto830 {
  # 830  O=(RND(1)/.8)*(2*H+16)*ABS(TAN(D1*.0035))
  $state->{"O"} = (rand()/0.8)*(2*$state->{"H"}+16)*abs(tan($state->{"D1"}*0.0035));
  # 840  D2=INT(SQR(O^2+ABS(D-D1)^2))
  $state->{"D2"} = int(sqrt($state->{"O"}**2+abs($state->{"D"}-$state->{"D1"})**2));
  # 850  IF D-D1<0 THEN 870
  if ( $state->{"D"}-$state->{"D1"} < 0 ) { my $out = { "anchor" => "goto870" }; return $out; }
  # 860  GOTO 890
  my $out = { "anchor" => "goto890" }; return $out;
}

sub goto862 {
  # 862  PRINT "YOU DUBBED IT."
  gameMessage( "YOU DUBBED IT.");
  # 864  D1=35
  $state->{"D1"} = 35;
  # 866  GOTO 830
  my $out = { "anchor" => "goto830" }; return $out;
}

sub goto867 {
  # 867  IF D<200 THEN 1300
  if ( $state->{"D"} < 200 ) { my $out = { "anchor" => "goto1300" }; return $out; }
  # 868  GOTO 770
  my $out = { "anchor" => "goto770" }; return $out;
}

sub goto870 {
  # 870  IF D2<20 THEN 890
  if ( $state->{"D2"} < 20 ) { my $out = { "anchor" => "goto890" }; return $out; }
  # 880  PRINT "TOO MUCH CLUB. YOU'RE PAST THE HOLE."
  gameMessage( "TOO MUCH CLUB. YOU'RE PAST THE HOLE.");
  my $out = { "anchor" => "goto890" }; return $out;
}

sub goto890 {
  # 890  B=D
  $state->{"B"} = $state->{"D"};
  # 900  D=D2
  $state->{"D"} = $state->{"D2"};
  # 910  IF D2>27 THEN 1020
  if ( $state->{"D2"} > 27 ) { my $out = { "anchor" => "goto1020" }; return $out; }
  # 920  IF D2>20 THEN 1100
  if ( $state->{"D2"} > 20 ) { my $out = { "anchor" => "goto1100" }; return $out; }
  # 930  IF D2>.5 THEN 1120
  if ( $state->{"D2"} > 0.5 ) { my $out = { "anchor" => "goto1120" }; return $out; }
  # 940  L(0)=9
  $state->{"La"}[0] = 9;
  # 950  GOTO 1470
  my $out = { "anchor" => "goto1470" }; return $out;
}

sub goto952 {
  # 952  IF S2+Q+(10*(F-1)/18)<(F-1)*(72+((H+1)/.85))/18 THEN 956
  if ( $state->{"S2"}+$state->{"Q"}+(10*($state->{"F1"}-1)/18) < ($state->{"F"}-1)*(72+(($state->{"H"}+1)/0.85))/18 ) { my $out = { "anchor" => "goto956" }; return $out; }
  # 954  GOTO 752
  my $out = { "anchor" => "goto752" }; return $out;
}

sub goto956 {
  # 956  Q=Q+1
  $state->{"Q"} = $state->{"Q"}+1;
  # 957  IF S1/2<>INT(S1/2) THEN 1011
  if ( $state->{"S1"}/2 != int($state->{"S1"}/2) ) { my $out = { "anchor" => "goto1011" }; return $out; }
  # 958  GOTO 862
  my $out = { "anchor" => "goto862" }; return $out;
}

sub goto960 {
  # 960  PRINT "NOW GAUGE YOUR DISTANCE BY A PERCENTAGE (1 TO 100)"
  gameMessage( "NOW GAUGE YOUR DISTANCE BY A PERCENTAGE (1 TO 100)");
  # 961  PRINT "OF A FULL SWING";
  gameMessage( "OF A FULL SWING");
  # 970  INPUT W
  my @argv = ("number");
  my $out = {
    "anchor" => "goto970postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto970postinput {
  $state->{"W"} = $input->{"action"}[0];
  #(970)  W=W/100
  $state->{"W"} = $state->{"W"}/100;
  # 972  PRINT
  blank();
  # 980  IF W>1 THEN 680
  if ( $state->{"W"} > 1 ) { my $out = { "anchor" => "goto680" }; return $out; }
  # 985  IF L(0)=5 THEN 1280
  if ( $state->{"L"}[0] == 5 ) { my $out = { "anchor" => "goto1280" }; return $out; }
  # 990  IF C=14 THEN 760
  if ( $state->{"C"} == 14 ) { my $out = { "anchor" => "goto760" }; return $out; }
  # 1000  C=C-10
  $state->{"C"} = $state->{"C"}-10;
  # 1010  GOTO 760
  my $out = { "anchor" => "goto760" }; return $out;
}

sub goto1011 {
  # 1011  IF D<95 THEN 862
  if ( $state->{"D"} < 95 ) { my $out = { "anchor" => "goto862" }; return $out; }
  # 1012  PRINT "BALL HIT TREE - BOUNCED INTO ROUGH";D-75;"YARDS FROM HOLE."
  my $d = $state->{"D"}-75;
  gameMessage( "BALL HIT TREE - BOUNCED INTO ROUGH ".$d." YARDS FROM HOLE.");
  # 1014  D=D-75
  $state->{"D"} = $state->{"D"}-75;
  # 1018  GOTO 620
  my $out = { "anchor" => "goto620" }; return $out;
}

sub goto1020 {
  # 1020  IF O<30 THEN 1150
  if ( $state->{"O"} < 30 ) { my $out = { "anchor" => "goto1150" }; return $out; }
  # 1022  IF J>0 THEN 1150
  if ( $state->{"J"} > 0 ) { my $out = { "anchor" => "goto1150" }; return $out; }
  # 1030  IF T>0 THEN 1070
  if ( $state->{"J"} > 0 ) { my $out = { "anchor" => "goto1070" }; return $out; }
  # 1035  S9=(S2+1)/15
  $state->{"S9"} = ($state->{"S2"}+1)/15;
  # 1036  IF INT(S9)=S9 THEN 1075
  if ( int($state->{"S9"}) == $state->{"S9"} ) { my $out = { "anchor" => "goto1075" }; return $out; }
  my $out = { "anchor" => "goto1040" }; return $out;
}

sub goto1040 {
  # 1040  PRINT "YOU HOOKED- ";
  gameMessage( "YOU HOOKED- ");
  # 1050  L(0)=L(2)
  $state->{"La"}[0] = $state->{"La"}[2];
  my $out = { "anchor" => "goto1055" }; return $out;
}

sub goto1055 {
  # 1055  IF O>45 THEN 1092
  if ( $state->{"O"} > 45 ) { my $out = { "anchor" => "goto1092" }; return $out; }
  # 1060  GOTO 320
  my $out = { "anchor" => "goto320" }; return $out;
}

sub goto1070 {
  # 1070  S9=(S2+1)/15
  $state->{"S9"} = ($state->{"S2"}+1)/15;
  # 1071  IF INT(S9)=S9 THEN 1040
  if ( int($state->{"S9"}) == $state->{"S9"} ) { my $out = { "anchor" => "goto1040" }; return $out; }
}

sub goto1075 {
  # 1075  PRINT "YOU SLICED- ";
  gameMessage( "YOU SLICED- ");
  # 1080  L(0)=L(1)
  $state->{"La"}[0] = $state->{"La"}[1];
  # 1090  GOTO 1055
  my $out = { "anchor" => "goto1055" }; return $out;
}

sub goto1092 {
  # 1092  PRINT "BADLY."
  gameMessage( "BADLY.");
  # 1094  GOTO 320
  my $out = { "anchor" => "goto320" }; return $out;
}

sub goto1100 {
  # 1100  L(0)=5
  $state->{"La"}[0] = 5;
  # 1110  GOTO 320
  my $out = { "anchor" => "goto320" }; return $out;
}

sub goto1120 {
  # 1120  L(0)=8
  $state->{"La"}[0] = 8;
  # 1130  D2=INT(D2*3)
  $state->{"D2"} = int($state->{"D2"}*3);
  # 1140  GOTO 1380
  my $out = { "anchor" => "goto1380" }; return $out;
}

sub goto1150 {
  # 1150  L(0)=1
  $state->{"La"}[0] = 1;
  # 1160  GOTO 320
  my $out = { "anchor" => "goto320" }; return $out;
}

sub goto1170 {
  # 1170  D1=INT(.85*D1)
  $state->{"D1"} = int($state->{"D1"}*0.85);
  # 1180  GOTO 830
  my $out = { "anchor" => "goto830" }; return $out;
}

sub goto1190 {
  # 1190  IF L(0)>6 THEN 1260
  if ( $state->{"La"}[0] > 6 ) { my $out = { "anchor" => "goto1260" }; return $out; }
  # 1200  PRINT "YOUR SHOT WENT INTO THE WATER."
  gameMessage( "YOUR SHOT WENT INTO THE WATER.");
  my $out = { "anchor" => "goto1210" }; return $out;
}

sub goto1210 {
  # 1210  S1=S1+1
  $state->{"S1"} = $state->{"S1"}+1;
  # 1220  PRINT "PENALTY STROKE ASSESSED.  HIT FROM PREVIOUS LOCATION."
  gameMessage( "PENALTY STROKE ASSESSED.  HIT FROM PREVIOUS LOCATION.");
  # 1230  J=J+1
  $state->{"J"} = $state->{"J"}+1;
  # 1240  L(0)=1
  $state->{"La"}[0] = 1;
  # 1242  D=B
  $state->{"D"} = $state->{"B"};
  # 1250  GOTO 620
  my $out = { "anchor" => "goto620" }; return $out;
}

sub goto1260 {
  # 1260  PRINT "YOUR SHOT WENT OUT OF BOUNDS."
  gameMessage( "YOUR SHOT WENT OUT OF BOUNDS.");
  # 1270  GOTO 1210
  my $out = { "anchor" => "goto1210" }; return $out;
}

sub goto1280 {
  # 1280  IF T=3 THEN 1320
  if ( $state->{"T"} == 3 ) { my $out = { "anchor" => "goto1320" }; return $out; }
  my $out = { "anchor" => "goto1300" }; return $out;
}

sub goto1300 {
  # 1300  D2=1+(3*INT((80/(40-H))*RND(1)))
  $state->{"D2"} = 1+(3*int((80/(40-$state->{"H"}))*rand()));
  # 1310  GOTO 1380
  my $out = { "anchor" => "goto1380" }; return $out;
}

sub goto1320 {
  # 1320  IF RND(1)>N THEN 1360
  if ( rand() > $state->{"N"} ) { my $out = { "anchor" => "goto1360" }; return $out; }
  # 1330  N=N*.2
  $state->{"N"} = $state->{"N"}*0.2;
  # 1340  PRINT "SHOT DUBBED, STILL IN TRAP."
  gameMessage( "SHOT DUBBED, STILL IN TRAP.");
  # 1350  GOTO 620
  my $out = { "anchor" => "goto620" }; return $out;
}

sub goto1360 {
  # 1360  N=.8
  $state->{"N"} = 0.8;
  # 1370  GOTO 1300
  my $out = { "anchor" => "goto1300" }; return $out;
}

sub goto1380 {
  # 1380  PRINT "ON GREEN,";D2;"FEET FROM THE PIN."
  gameMessage( "ON GREEN, ".$state->{"D2"}." FEET FROM THE PIN.");
  # 1381  PRINT "CHOOSE YOUR PUTT POTENCY (1 TO 13)
  gameMessage( "CHOOSE YOUR PUTT POTENCY (1 TO 13):");
  # 1400  INPUT I
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto1400postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto1400postinput {
  $state->{"I"} = $input->{"action"}[0];
  # 1410  S1=S1+1
  $state->{"S1"} = $state->{"S1"}+1;
  # 1420  IF S1+1-P>(H*.072)+2 THEN 1470
  if ( $state->{"S1"}+1-$state->{"P"} > ($state->{"H"}*0.072)+2 ) { my $out = { "anchor" => "goto1470" }; return $out; }
  # 1425  IF K>2 THEN 1470
  if ( $state->{"K"} > 2 ) { my $out = { "anchor" => "goto1470" }; return $out; }
  # 1428  K=K+1
  $state->{"K"} = $state->{"K"}+1;
  # 1430  IF T=4 THEN 1530
  if ( $state->{"T"} == 4 ) { my $out = { "anchor" => "goto1530" }; return $out; }
  # 1440  D2=D2-I*(4+2*RND(1))+1.5
  $state->{"D2"} = $state->{"D2"}-$state->{"I"}*(4+2*rand())+1.5;
  my $out = { "anchor" => "goto1450" }; return $out;
}

sub goto1450 {
  # 1450  IF D2<-2 THEN 1560
  if ( $state->{"D2"} < -2 ) { my $out = { "anchor" => "goto1560" }; return $out; }
  # 1460  IF D2>2 THEN 1500
  if ( $state->{"D2"} > 2 ) { my $out = { "anchor" => "goto1500" }; return $out; }
  my $out = { "anchor" => "goto1470" }; return $out;
}

sub goto1470 {
  # 1470  PRINT "YOU HOLED IT."
  gameMessage( "YOU HOLED IT.");
  # 1472  PRINT
  blank();
  # 1480  F=F+1
  $state->{"F"} = $state->{"F"}+1;
  # 1490  GOTO 230
  my $out = { "anchor" => "goto230" }; return $out;
}

sub goto1500 {
  # 1500  PRINT "PUTT SHORT."
  gameMessage( "PUTT SHORT.");
  my $out = { "anchor" => "goto1505" }; return $out;
}

sub goto1505 {
  # 1505  D2=INT(D2)
  $state->{"D2"} = int($state->{"D2"});
  # 1510  GOTO 1380
  my $out = { "anchor" => "goto1380" }; return $out;
}

sub goto1530 {
  # 1530  D2=D2-I*(4+1*RND(1))+1
  $state->{"D2"} = $state->{"D2"}-$state->{"I"}*(4+rand())+1;
  # 1550  GOTO 1450
  my $out = { "anchor" => "goto1450" }; return $out;
}

sub goto1560 {
  # 1560  PRINT "PASSED BY CUP."
  gameMessage( "PASSED BY CUP.");
  # 1570  D2=-D2
  $state->{"D2"} = -$state->{"D2"};
  # 1580  GOTO 1505
  my $out = { "anchor" => "goto1505" }; return $out;
}

sub goto1590 {
  # 1590  READ D,P,L(1),L(2)
  $state->{"D"} = $state->{"DATA"}[$state->{"pointer"}]; advance_pointer();
  $state->{"P"} = $state->{"DATA"}[$state->{"pointer"}]; advance_pointer();
  $state->{"La"}[1] = $state->{"DATA"}[$state->{"pointer"}]; advance_pointer();
  $state->{"La"}[2] = $state->{"DATA"}[$state->{"pointer"}]; advance_pointer();
  # 1595  PRINT
  blank();
  # 1600  PRINT "YOU ARE AT THE TEE OFF HOLE";F;"DISTANCE";D;"YARDS, PAR";P
  gameMessage( "YOU ARE AT THE TEE OFF HOLE ".$state->{"F"}." DISTANCE ".$state->{"D"}." YARDS, PAR ".$state->{"P"});
  # 1605  G3=G3+P
  $state->{"G3"} = $state->{"G3"}+$state->{"P"};
  # 1620  PRINT "ON YOUR RIGHT IS ";
  $__golfmsg__ = "ON YOUR RIGHT IS ";
  # 1630  X=1
  $state->{"X"} = 1;
  # 1640  GOSUB 400
  gosub400();
  # 1650  PRINT "ON YOUR LEFT IS ";
  $__golfmsg__ = "ON YOUR LEFT IS ";
  # 1660  X=2
  $state->{"X"} = 2;
  # 1670  GOSUB 400
  gosub400();
  # 1680  GOTO 620
  my $out = { "anchor" => "goto620" }; return $out;
}

sub goto1690 {
  # 1690  RETURN
  #return;
  # 1700  DATA 361,4,4,2,389,4,3,3,206,3,4,2,500,5,7,2
  # 1702  DATA 408,4,2,4,359,4,6,4,424,4,4,2,388,4,4,4
  # 1704  DATA 196,3,7,2,400,4,7,2,560,5,7,2,132,3,2,2
  # 1706  DATA 357,4,4,4,294,4,2,4,475,5,2,3,375,4,4,2
  # 1708  DATA 180,3,6,2,550,5,6,6
  my $out = { "anchor" => "goto1710" }; return $out;
}

sub goto1710 {
  # 1710  PRINT
  blank();
  my $out = { "anchor" => "goto1750" }; return $out;
}

sub goto1750 {
  # 1750  G2=G2+S1
  $state->{"G2"} = $state->{"G2"}+$state->{"S1"};
  # 1760  PRINT "TOTAL PAR FOR";F-1;"HOLES IS";G3;"  YOUR TOTAL IS";G2
  my $f = $state->{"F"}-1;
  gameMessage( "TOTAL PAR FOR ".$f." HOLES IS ".$state->{"G3"}."  YOUR TOTAL IS ".$state->{"G2"});
  # 1761  IF G1=F-1 THEN 1770
  if ( $state->{"G1"} == $state->{"F"}-1 ) { my $out = { "anchor" => "goto1770" }; return $out; }
  # 1765  GOTO 292
  my $out = { "anchor" => "goto292" }; return $out;
}

sub goto1770 {
  # 1770  END
  my $out = { "anchor" => "quit_game" }; return $out;
}

1;

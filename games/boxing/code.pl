#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

  #(<<<GOTO>> ) 90, 1040, 1060, 600, 225, 230, 340, 450, 520, 290, 310, 950, 410, 300, 980, 480, 500, 570, 605, 610, 720, 810, 860, 700, 690, 770, 1010, 890, 920, 955, 960, 1080, 1000
  #(<<<GOSUB>> ) 

sub initialize {
  my $out = { "anchor" => "goto1" }; return $out;
}

sub goto1 {
  # 1  PRINT TAB(33);"BOXING"
  gameMessageTab(33, "BOXING");
  # 2  PRINT TAB(15);"CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY"
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  # 3  PRINT
  blank();
  #(3) PRINT
  blank();
  #(3) PRINT
  blank();
  # 4  PRINT "BOXING OLYMPIC STYLE (3 ROUNDS -- 2 OUT OF 3 WINS)"
  gameMessage( "BOXING OLYMPIC STYLE (3 ROUNDS -- 2 OUT OF 3 WINS)");
  # 5  J=0
  $state->{"J"} = 0;
  # 6  L=0
  $state->{"L"} = 0;
  # 8  PRINT
  blank();
  # 10  PRINT "WHAT IS YOUR OPPONENT'S NAME";
  gameMessage( "WHAT IS YOUR OPPONENT'S NAME");
  # 20  INPUT J$
  my $out = {
    "anchor" => "goto20postinput",
    "echo" => " > ",
    "hook" => "reply"
  };
  return $out;
}

sub goto20postinput {
  $state->{"Js"} = join(' ', @{$input->{"action"}});
  # 30  PRINT "INPUT YOUR MAN'S NAME";
  gameMessage( "INPUT YOUR MAN'S NAME");
  # 40  INPUT L$
  my $out = {
    "anchor" => "goto40postinput",
    "echo" => " > ",
    "hook" => "reply"
  };
  return $out;
}

sub goto40postinput {
  $state->{"Ls"} = join(' ', @{$input->{"action"}});
  # 50  PRINT "DIFFERENT PUNCHES ARE
  gameMessage( "DIFFERENT PUNCHES ARE: (1) FULL SWING; (2) HOOK; (3) UPPERCUT; (4) JAB.");
  # 60  PRINT "WHAT IS YOUR MANS BEST";
  gameMessage( "WHAT IS YOUR MANS BEST");
  # 64  INPUT B
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto64postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto64postinput {
  $state->{"B"} = $input->{"action"}[0];
  # 70  PRINT "WHAT IS HIS VULNERABILITY";
  gameMessage( "WHAT IS HIS VULNERABILITY");
  # 80  INPUT D
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto80postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto80postinput {
  $state->{"D"} = $input->{"action"}[0];
  my $out = { "anchor" => "goto90" }; return $out;
}

sub goto90 {
  # 90  B1=INT(4*RND(1)+1)
  $state->{"B1"} = int(4*rand()+1);
  # 100  D1=INT(4*RND(1)+1)
  $state->{"D1"} = int(4*rand(1)+1);
  # 110  IF B1=D1 THEN 90
  if ( $state->{"B1"} eq $state->{"D1"} ) { my $out = { "anchor" => "goto90" }; return $out; }
  # 120  PRINT J$;"'S ADVANTAGE IS";B1;"AND VULNERABILITY IS SECRET."
  gameMessage( $state->{"Js"}."'S ADVANTAGE IS ".$state->{"B1"}." AND VULNERABILITY IS SECRET.");
  #(120) PRINT
  blank();
  # TODO: Solve these nested for-next loops
  # 130  FOR R=1 TO 3
  $state->{"R"} = 0;
  my $out = { "anchor" => "nextR" }; return $out;
}

sub nextR {
  $state->{"R"} = $state->{"R"} + 1;
  if ($state->{"R"} > 3) { my $out = { "anchor" => "goto961" }; return $out; }
  # 140  IF J>= 2 THEN 1040
  if ( $state->{"J"} >= 2 ) { my $out = { "anchor" => "goto1040" }; return $out; }
  # 150  IF L>=2 THEN 1060
  if ( $state->{"L"} >= 2 ) { my $out = { "anchor" => "goto1060" }; return $out; }
  # 160  X=0
  $state->{"X"} = 0;
  # 170  Y=0
  $state->{"Y"} = 0;
  # 180  PRINT "ROUND";R;"BEGINS..."
  gameMessage( "ROUND ".$state->{"R"}." BEGINS...");
  $state->{"R1"} = 0;
  my $out = { "anchor" => "nextR1" }; return $out;
}

sub nextR1 {
  # 185  FOR R1= 1 TO 7
  $state->{"R1"} = $state->{"R1"} + 1;
  if ($state->{"R1"} > 7) { my $out = { "anchor" => "goto951" }; return $out; }
  # 190  I=INT(10*RND(1)+1)
  $state->{"I"} = int(10*rand()+1);
  # 200  IF I>5 THEN 600
  if ( $state->{"I"} > 5 ) { my $out = { "anchor" => "goto600" }; return $out; }
  # 210  PRINT L$;"'S PUNCH";
  gameMessage( $state->{"Ls"}."'S PUNCH");
  # 220  INPUT P
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto220postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto220postinput {
  $state->{"P"} = $input->{"action"}[0];
  # 221  IF P=B THEN 225
  if ( $state->{"P"} == $state->{"B"} ) { my $out = { "anchor" => "goto225" }; return $out; }
  # 222  GOTO 230
  my $out = { "anchor" => "goto230" }; return $out;
}

sub goto225 {
  # 225  X=X+2
  $state->{"X"} = $state->{"X"}+2;
  my $out = { "anchor" => "goto230" }; return $out;
}

sub goto230 {
  # 230  IF P=1 THEN 340
  if ( $state->{"P"} == 1 ) { my $out = { "anchor" => "goto340" }; return $out; }
  # 240  IF P=2 THEN 450
  if ( $state->{"P"} ==  2 ) { my $out = { "anchor" => "goto450" }; return $out; }
  # 250  IF P=3 THEN 520
  if ( $state->{"P"} == 3 ) { my $out = { "anchor" => "goto520" }; return $out; }
  # 270  PRINT L$;" JABS AT ";J$"'S HEAD ";
  gameMessage( $state->{"Ls"}." JABS AT ".$state->{"Js"}."'S HEAD ");
  # 271  IF D1=4 THEN 290
  if ( $state->{"D1"} == 4 ) { my $out = { "anchor" => "goto290" }; return $out; }
  # 275  C=INT(8*RND(1)+1)
  $state->{"C"} = int(8*rand()+1);
  # 280  IF C<4 THEN 310
  if ( $state->{"C"} < 4 ) { my $out = { "anchor" => "goto310" }; return $out; }
  my $out = { "anchor" => "goto290" }; return $out;
}

sub goto290 {
  # 290  X=X+3
  $state->{"X"} = $state->{"X"}+3;
  my $out = { "anchor" => "goto300" }; return $out;
}

sub goto300 {
  # 300  GOTO 950
  my $out = { "anchor" => "goto950" }; return $out;
}

sub goto310 {
  # 310  PRINT "IT'S BLOCKED."
  gameMessage( "IT'S BLOCKED.");
  # 330  GOTO 950
  my $out = { "anchor" => "goto950" }; return $out;
}

sub goto340 {
  # 340  PRINT L$ " SWINGS AND ";
  gameMessage( $state->{"Ls"}." SWINGS AND ");
  # 341  IF D1=4 THEN 410
  if ( $state->{"D1"} == 4 ) { my $out = { "anchor" => "goto410" }; return $out; }
  # 345  X3=INT(30*RND(1)+1)
  $state->{"X3"} = int(30*rand()+1);
  # 350  IF X3<10 THEN 410
  if ( $state->{"X3"} < 10 ) { my $out = { "anchor" => "goto410" }; return $out; }
  # 360  PRINT "HE MISSES ";
  gameMessage( "HE MISSES ");
  # 370  PRINT
  blank();
  # 375  IF X=1 THEN 950
  if ( $state->{"X"} == 1 ) { my $out = { "anchor" => "goto950" }; return $out; }
  # 380  PRINT
  blank();
  # 390  PRINT
  blank();
  # 400  GOTO 300
  my $out = { "anchor" => "goto300" }; return $out;
}

sub goto410 {
  # 410  PRINT "HE CONNECTS!"
  gameMessage( "HE CONNECTS!");
  # 420  IF X>35 THEN 980
  if ( $state->{"X"} > 35 ) { my $out = { "anchor" => "goto980" }; return $out; }
  # 425  X=X+15
  $state->{"X"} = $state->{"X"}+15;
  # 440  GOTO 300
  my $out = { "anchor" => "goto300" }; return $out;
}

sub goto450 {
  # 450  PRINT L$;" GIVES THE HOOK... ";
  gameMessage( $state->{"Ls"}." GIVES THE HOOK... ");
  # 455  IF D1=2 THEN 480
  if ( $state->{"D1"} == 2 ) { my $out = { "anchor" => "goto480" }; return $out; }
  # 460  H1=INT(2*RND(1)+1)
  $state->{"H1"} = int(2*rand()+1);
  # 470  IF H1=1 THEN 500
  if ( $state->{"H1"} == 1 ) { my $out = { "anchor" => "goto500" }; return $out; }
  # 475  PRINT "CONNECTS..."
  gameMessage( "CONNECTS...");
  my $out = { "anchor" => "goto480" }; return $out;
}

sub goto480 {
  # 480  X=X+7
  $state->{"X"} = $state->{"X"}+7;
  # 490  GOTO 300
  my $out = { "anchor" => "goto300" }; return $out;
}

sub goto500 {
  # 500  PRINT "BUT IT'S BLOCKED!!!!!!!!!!!!!"
  gameMessage( "BUT IT'S BLOCKED!!!!!!!!!!!!!");
  # 510  GOTO 300
  my $out = { "anchor" => "goto300" }; return $out;
}

sub goto520 {
  # 520  PRINT L$ " TRIES AN UPPERCUT ";
  gameMessage( $state->{"Ls"}." TRIES AN UPPERCUT ");
  # 530  IF D1=3 THEN 570
  if ( $state->{"D1"} == 3 ) { my $out = { "anchor" => "goto570" }; return $out; }
  # 540  D5=INT(100*RND(1)+1)
  $state->{"D5"} = int(100*rand()+1);
  # 550  IF D5<51 THEN 570
  if ( $state->{"D5"} < 51 ) { my $out = { "anchor" => "goto570" }; return $out; }
  # 560  PRINT "AND IT'S BLOCKED (LUCKY BLOCK!)"
  gameMessage( "AND IT'S BLOCKED (LUCKY BLOCK!)");
  # 565  GOTO 300
  my $out = { "anchor" => "goto300" }; return $out;
}

sub goto570 {
  # 570  PRINT "AND HE CONNECTS!"
  gameMessage( "AND HE CONNECTS!");
  # 580  X=X+4
  $state->{"X"} = $state->{"X"}+4;
  # 590  GOTO 300
  my $out = { "anchor" => "goto300" }; return $out;
}

sub goto600 {
  # 600  J7=INT(4*RND(1)+1)
  $state->{"J7"} = int(4*rand()+1);
  # 601  IF J7 =B1 THEN 605
  if ( $state->{"J7"} == $state->{"B1"} ) { my $out = { "anchor" => "goto605" }; return $out; }
  # 602  GOTO 610
  my $out = { "anchor" => "goto610" }; return $out;
}

sub goto605 {
  # 605  Y=Y+2
  $state->{"Y"} = $state->{"Y"}+2;
  my $out = { "anchor" => "goto610" }; return $out;
}

sub goto610 {
  # 610  IF J7=1 THEN 720
  if ( $state->{"J7"} == 1 ) { my $out = { "anchor" => "goto720" }; return $out; }
  # 620  IF J7=2 THEN 810
  if ( $state->{"J7"} == 2 ) { my $out = { "anchor" => "goto810" }; return $out; }
  # 630  IF J7 =3 THEN 860
  if ( $state->{"J7"} == 3 ) { my $out = { "anchor" => "goto860" }; return $out; }
  # 640  PRINT J$;" JABS AND ";
  gameMessage( $state->{"Js"}." JABS AND ");
  # 645  IF D=4 THEN 700
  if ( $state->{"D"} == 4 ) { my $out = { "anchor" => "goto700" }; return $out; }
  # 650  Z4=INT(7*RND(1)+1)
  $state->{"Z4"} = int(7*rand()+1);
  # 655  IF Z4>4 THEN 690
  if ( $state->{"Z4"} > 4 ) { my $out = { "anchor" => "goto690" }; return $out; }
  # 660  PRINT "IT'S BLOCKED!"
  gameMessage( "IT'S BLOCKED!");
  # 670  GOTO 300
  my $out = { "anchor" => "goto300" }; return $out;
}

sub goto690 {
  # 690  PRINT " BLOOD SPILLS !!!"
  gameMessage( " BLOOD SPILLS !!!");
  my $out = { "anchor" => "goto700" }; return $out;
}

sub goto700 {
  # 700  Y=Y+5
  $state->{"Y"} = $state->{"Y"}+5;
  # 710  GOTO 300
  my $out = { "anchor" => "goto300" }; return $out;
}

sub goto720 {
  # 720  PRINT J$" TAKES A FULL SWING AND";
  gameMessage( $state->{"Js"}." TAKES A FULL SWING AND");
  # 730  IF D=1 THEN 770
  if ( $state->{"D"} == 1 ) { my $out = { "anchor" => "goto770" }; return $out; }
  # 740  R6=INT(60*RND(1)+1)
  $state->{"R6"} = int(60*rand()+1);
  # 745  IF R6 <30 THEN 770
  if ( $state->{"R6"} < 30 ) { my $out = { "anchor" => "goto770" }; return $out; }
  # 750  PRINT " IT'S BLOCKED!"
  gameMessage( " IT'S BLOCKED!");
  # 760  GOTO 300
  my $out = { "anchor" => "goto300" }; return $out;
}

sub goto770 {
  # 770  PRINT " POW!!!!! HE HITS HIM RIGHT IN THE FACE!"
  gameMessage( " POW!!!!! HE HITS HIM RIGHT IN THE FACE!");
  # 780  IF Y>35 THEN 1010
  if ( $state->{"Y"} > 35 ) { my $out = { "anchor" => "goto1010" }; return $out; }
  # 790  Y=Y+15
  $state->{"Y"} = $state->{"Y"}+15;
  # 800  GOTO 300
  my $out = { "anchor" => "goto300" }; return $out;
}

sub goto810 {
  # 810  PRINT J$;" GETS ";L$;" IN THE JAW (OUCH!)"
  gameMessage( $state->{"Js"}." GETS ".$state->{"Ls"}." IN THE JAW (OUCH!)");
  # 820  Y=Y+7
  $state->{"Y"} = $state->{"Y"}+7;
  # 830  PRINT "....AND AGAIN!"
  gameMessage( "....AND AGAIN!");
  # 835  Y=Y+5
  $state->{"Y"} = $state->{"Y"}+5;
  # 840  IF Y>35 THEN 1010
  if ( $state->{"Y"} > 35 ) { my $out = { "anchor" => "goto1010" }; return $out; }
  # 850  PRINT
  blank();
  my $out = { "anchor" => "goto860" }; return $out;
}

sub goto860 {
  # 860  PRINT L$;" IS ATTACKED BY AN UPPERCUT (OH,OH)..."
  gameMessage( $state->{"Ls"}." IS ATTACKED BY AN UPPERCUT (OH,OH)...");
  # 865  IF D=3 THEN 890
  if ( $state->{"D"} = 3 ) { my $out = { "anchor" => "goto890" }; return $out; }
  # 870  Q4=INT(200*RND(1)+1)
  $state->{"Q4"} = int(200*rand()+1);
  # 880  IF Q4>75 THEN 920
  if ( $state->{"Q4"} > 75 ) { my $out = { "anchor" => "goto920" }; return $out; }
}

sub goto890 {
  # 890  PRINT "AND ";J$;" CONNECTS..."
  gameMessage( "AND ".$state->{"Js"}." CONNECTS...");
  # 900  Y=Y+8
  $state->{"Y"} = $state->{"Y"}+8;
  # 910  GOTO 300
  my $out = { "anchor" => "goto300" }; return $out;
}

sub goto920 {
  # 920  PRINT " BLOCKS AND HITS ";J$;" WITH A HOOK."
  gameMessage( " BLOCKS AND HITS ".$state->{"Js"}." WITH A HOOK.");
  # 930  X=X+5
  $state->{"X"} = $state->{"X"}+5;
  # 940  GOTO 300
  my $out = { "anchor" => "goto300" }; return $out;
}

sub goto950 {
  # 950  NEXT R1
  my $out = { "anchor" => "nextR1" }; return $out;
}
sub goto951 {
  # 951  IF X>Y THEN 955
  if ( $state->{"X"} > $state->{"Y"} ) { my $out = { "anchor" => "goto955" }; return $out; }
  # 952  PRINT
  blank();
  #(952) PRINT J$" WINS ROUND" R
  gameMessage( $state->{"Js"}." WINS ROUND".$state->{"R"});
  # 953  J=J+1
  $state->{"J"} = $state->{"J"}+1;
  # 954  GOTO 960
  my $out = { "anchor" => "goto960" }; return $out;
}

sub goto955 {
  # 955  PRINT
  blank();
  #(955) PRINT L$" WINS ROUND"R
  gameMessage( $state->{"Ls"}." WINS ROUND".$state->{"R"});
  # 956  L=L+1
  $state->{"L"} = $state->{"L"}+1;
  my $out = { "anchor" => "goto960" }; return $out;
}

sub goto960 {
  # 960  NEXT R
  my $out = { "anchor" => "nextR" }; return $out;
}

sub goto961 {
  # 961  IF J>= 2 THEN 1040
  if ( $state->{"J"} >= 2 ) { my $out = { "anchor" => "goto1040" }; return $out; }
  # 962  IF L>=2 THEN 1060
  if ( $state->{"L"} >= 2 ) { my $out = { "anchor" => "goto1060" }; return $out; }
  my $out = { "anchor" => "goto980" }; return $out;
}

sub goto980 {
  # 980  PRINT J$ " IS KNOCKED COLD AND " L$" IS THE WINNER AND CHAMP!";
  gameMessage( $state->{"Js"}." IS KNOCKED COLD AND ".$state->{"Ls"}." IS THE WINNER AND CHAMP!");
  my $out = { "anchor" => "goto1000" }; return $out;
}

sub goto1000 {
  # 1000  GOTO 1080
  my $out = { "anchor" => "goto1080" }; return $out;
}

sub goto1010 {
  # 1010  PRINT L$ " IS KNOCKED COLD AND " J$" IS THE WINNER AND CHAMP!";
  gameMessage( $state->{"Ls"}." IS KNOCKED COLD AND ".$state->{"Js"}." IS THE WINNER AND CHAMP!");
  # 1030  GOTO 1000
  my $out = { "anchor" => "goto1000" }; return $out;
}

sub goto1040 {
  # 1040  PRINT J$ " WINS (NICE GOING," J$;")."
  gameMessage( $state->{"Js"}." WINS (NICE GOING,".$state->{"Js"}.").");
  # 1050  GOTO 1000
  my $out = { "anchor" => "goto1000" }; return $out;
}

sub goto1060 {
  # 1060  PRINT L$ " AMAZINGLY WINS!!"
  gameMessage( $state->{"Ls"}." AMAZINGLY WINS!!");
  # 1070  GOTO 1000
  my $out = { "anchor" => "goto1000" }; return $out;
}

sub goto1080 {
  # 1080  PRINT
  blank();
  # 1085  PRINT
  blank();
  # 1090  PRINT "AND NOW GOODBYE FROM THE OLYMPIC ARENA."
  gameMessage( "AND NOW GOODBYE FROM THE OLYMPIC ARENA.");
  # 1100  PRINT
  blank();
  # 1110  END
  my $out = { "anchor" => "quit_game" }; return $out;
}

1;

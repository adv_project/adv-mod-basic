#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

  #(<<<GOTO>> ) 200, 100, 260
  #(<<<GOSUB>> ) 

sub initialize {
  my $out = { "anchor" => "goto3" }; return $out;
}

sub goto3 {
  # 3  PRINT TAB(33);"CHEMIST"
  gameMessageTab(33, "CHEMIST");
  # 6  PRINT TAB(15);"CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY"
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  # 8  PRINT
  blank();
  #(8) PRINT
  blank();
  #(8) PRINT
  blank();
  # 10  PRINT "THE FICTITIOUS CHECMICAL KRYPTOCYANIC ACID CAN ONLY BE"
  gameMessage( "THE FICTITIOUS CHECMICAL KRYPTOCYANIC ACID CAN ONLY BE");
  # 20  PRINT "DILUTED BY THE RATIO OF 7 PARTS WATER TO 3 PARTS ACID."
  gameMessage( "DILUTED BY THE RATIO OF 7 PARTS WATER TO 3 PARTS ACID.");
  # 30  PRINT "IF ANY OTHER RATIO IS ATTEMPTED, THE ACID BECOMES UNSTABLE"
  gameMessage( "IF ANY OTHER RATIO IS ATTEMPTED, THE ACID BECOMES UNSTABLE");
  # 40  PRINT "AND SOON EXPLODES.  GIVEN THE AMOUNT OF ACID, YOU MUST"
  gameMessage( "AND SOON EXPLODES.  GIVEN THE AMOUNT OF ACID, YOU MUST");
  # 50  PRINT "DECIDE WHO MUCH WATER TO ADD FOR DILUTION.  IF YOU MISS"
  gameMessage( "DECIDE WHO MUCH WATER TO ADD FOR DILUTION.  IF YOU MISS");
  # 60  PRINT "YOU FACE THE CONSEQUENCES."
  gameMessage( "YOU FACE THE CONSEQUENCES.");
  my $out = { "anchor" => "goto100" }; return $out;
}

sub goto100 {
  # 100  A=INT(RND(1)*50)
  $state->{"A"} = int(rand()*50);
  # 110  W=7*A/3
  $state->{"W"} = 7*$state->{"A"}/3;
  # 120  PRINT A;"LITERS OF KRYPTOCYANIC ACID.  HOW MUCH WATER";
  gameMessage( $state->{"A"}." LITERS OF KRYPTOCYANIC ACID.  HOW MUCH WATER");
  # 130  INPUT R
  my @argv = ("number");
  my $out = {
    "anchor" => "goto130postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto130postinput {
  $state->{"R"} = $input->{"action"}[0];
  # 140  D=ABS(W-R)
  $state->{"D"} = abs($state->{"W"}-$state->{"R"});
  # 150  IF D>W/20 THEN 200
  if ( $state->{"D"} > $state->{"W"}/20 ) { my $out = { "anchor" => "goto200" }; return $out; }
  # 160  PRINT " GOOD JOB! YOU MAY BREATHE NOW, BUT DON'T INHALE THE FUMES!"
  gameMessage( " GOOD JOB! YOU MAY BREATHE NOW, BUT DON'T INHALE THE FUMES!");
  # 170  PRINT
  blank();
  # 180  GOTO 100
  my $out = { "anchor" => "goto100" }; return $out;
}

sub goto200 {
  # 200  PRINT " SIZZLE!  YOU HAVE JUST BEEN DESALINATED INTO A BLOB"
  gameMessage( " SIZZLE!  YOU HAVE JUST BEEN DESALINATED INTO A BLOB");
  # 210  PRINT " OF QUIVERING PROTOPLASM!"
  gameMessage( " OF QUIVERING PROTOPLASM!");
  # 220  T=T+1
  $state->{"T"} = $state->{"T"}+1;
  # 230  IF T=9 THEN 260
  if ( $state->{"T"} == 9 ) { my $out = { "anchor" => "goto260" }; return $out; }
  # 240  PRINT " HOWEVER, YOU MAY TRY AGAIN WITH ANOTHER LIFE."
  gameMessage( " HOWEVER, YOU MAY TRY AGAIN WITH ANOTHER LIFE.");
  # 250  GOTO 100
  my $out = { "anchor" => "goto100" }; return $out;
}

sub goto260 {
  # 260  PRINT " YOUR 9 LIVES ARE USED, BUT YOU WILL BE LONG REMEMBERED FOR"
  gameMessage( " YOUR 9 LIVES ARE USED, BUT YOU WILL BE LONG REMEMBERED FOR");
  # 270  PRINT " YOUR CONTRIBUTIONS TO THE FIELD OF COMIC BOOK CHEMISTRY."
  gameMessage( " YOUR CONTRIBUTIONS TO THE FIELD OF COMIC BOOK CHEMISTRY.");
  # 280  END
  my $out = { "anchor" => "quit_game" }; return $out;
}

1;

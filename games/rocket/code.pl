#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

  #(<<<GOTO>> ) 390, 650, 0, 670, 490, 640, 540, 720, 730, 810, 840
  #(<<<GOSUB>> ) 

sub initialize {
  my $out = { "anchor" => "goto10" }; return $out;
}

sub goto10 {
  # 10  PRINT TAB(30); "ROCKET"
  gameMessageTab(30,  "ROCKET");
  # 20  PRINT TAB(15);"CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY"
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  # 30  PRINT
  blank();
  #(30) PRINT
  blank();
  #(30) PRINT
  blank();
  # 70  PRINT "LUNAR LANDING SIMULATION"
  gameMessage( "LUNAR LANDING SIMULATION");
  # 80  PRINT "----- ------- ----------"
  gameMessage( "----- ------- ----------");
  #(80)  PRINT
  blank();
  # 100  INPUT "DO YOU WANT INSTRUCTIONS (YES OR NO)";A$
  gameMessage("DO YOU WANT INSTRUCTIONS (YES OR NO)");
  my $out = {
    "anchor" => "goto100postinput",
    "echo" => " > ",
    "hook" => "reply"
  };
  return $out;
}

sub goto100postinput {
  $state->{"As"} = $input->{"action"}[0];
  # 110  IF A$="NO" THEN 390
  if ( $state->{"As"} =~ /^NO$/ ) { my $out = { "anchor" => "goto390" }; return $out; }
  # 160  PRINT
  blank();
  # 200  PRINT"YOU ARE LANDING ON THE MOON AND AND HAVE TAKEN OVER MANUAL"
  gameMessage("YOU ARE LANDING ON THE MOON AND AND HAVE TAKEN OVER MANUAL");
  # 210  PRINT"CONTROL 1000 FEET ABOVE A GOOD LANDING SPOT. YOU HAVE A DOWN-"
  gameMessage("CONTROL 1000 FEET ABOVE A GOOD LANDING SPOT. YOU HAVE A DOWN-");
  # 220  PRINT"WARD VELOCITY OF 50 FEET/SEC. 150 UNITS OF FUEL REMAIN."
  gameMessage("WARD VELOCITY OF 50 FEET/SEC. 150 UNITS OF FUEL REMAIN.");
  # 225  PRINT
  blank();
  # 230  PRINT"HERE ARE THE RULES THAT GOVERN YOUR APOLLO SPACE-CRAFT
  gameMessage("HERE ARE THE RULES THAT GOVERN YOUR APOLLO SPACE-CRAFT");
  #(230) "
  #(230)  PRINT
  blank();
  # 240  PRINT"(1) AFTER EACH SECOND THE HEIGHT, VELOCITY, AND REMAINING FUEL"
  gameMessage("(1) AFTER EACH SECOND THE HEIGHT, VELOCITY, AND REMAINING FUEL");
  # 250  PRINT"    WILL BE REPORTED VIA DIGBY YOUR ON-BOARD COMPUTER."
  gameMessage("    WILL BE REPORTED VIA DIGBY YOUR ON-BOARD COMPUTER.");
  # 260  PRINT"(2) AFTER THE REPORT A '?' WILL APPEAR. ENTER THE NUMBER"
  gameMessage("(2) AFTER THE REPORT A '?' WILL APPEAR. ENTER THE NUMBER");
  # 270  PRINT"    OF UNITS OF FUEL YOU WISH TO BURN DURING THE NEXT"
  gameMessage("    OF UNITS OF FUEL YOU WISH TO BURN DURING THE NEXT");
  # 280  PRINT"    SECOND. EACH UNIT OF FUEL WILL SLOW YOUR DESCENT BY"
  gameMessage("    SECOND. EACH UNIT OF FUEL WILL SLOW YOUR DESCENT BY");
  # 290  PRINT"    1 FOOT/SEC."
  gameMessage("    1 FOOT/SEC.");
  # 310  PRINT"(3) THE MAXIMUM THRUST OF YOUR ENGINE IS 30 FEET/SEC/SEC"
  gameMessage("(3) THE MAXIMUM THRUST OF YOUR ENGINE IS 30 FEET/SEC/SEC");
  # 320  PRINT"    OR 30 UNITS OF FUEL PER SECOND."
  gameMessage("    OR 30 UNITS OF FUEL PER SECOND.");
  # 330  PRINT"(4) WHEN YOU CONTACT THE LUNAR SURFACE. YOUR DESCENT ENGINE"
  gameMessage("(4) WHEN YOU CONTACT THE LUNAR SURFACE. YOUR DESCENT ENGINE");
  # 340  PRINT"    WILL AUTOMATICALLY SHUT DOWN AND YOU WILL BE GIVEN A"
  gameMessage("    WILL AUTOMATICALLY SHUT DOWN AND YOU WILL BE GIVEN A");
  # 350  PRINT"    REPORT OF YOUR LANDING SPEED AND REMAINING FUEL."
  gameMessage("    REPORT OF YOUR LANDING SPEED AND REMAINING FUEL.");
  # 360  PRINT"(5) IF YOU RUN OUT OF FUEL THE '?' WILL NO LONGER APPEAR"
  gameMessage("(5) IF YOU RUN OUT OF FUEL THE '?' WILL NO LONGER APPEAR");
  # 370  PRINT"    BUT YOUR SECOND BY SECOND REPORT WILL CONTINUE UNTIL"
  gameMessage("    BUT YOUR SECOND BY SECOND REPORT WILL CONTINUE UNTIL");
  # 380  PRINT"    YOU CONTACT THE LUNAR SURFACE."
  gameMessage("    YOU CONTACT THE LUNAR SURFACE.");
  #(380) PRINT
  blank();
  my $out = { "anchor" => "goto390" }; return $out;
}

sub goto390 {
  # 390  PRINT"BEGINNING LANDING PROCEDURE.........."
  gameMessage("BEGINNING LANDING PROCEDURE..........");
  #(390) PRINT
  blank();
  # 400  PRINT"G O O D  L U C K ! ! !"
  gameMessage("G O O D  L U C K ! ! !");
  # 420  PRINT
  blank();
  #(420) PRINT
  blank();
  # 430  PRINT"SEC  FEET      SPEED     FUEL     PLOT OF DISTANCE"
  gameMessage("SEC  FEET      SPEED     FUEL     PLOT OF DISTANCE");
  # 450  PRINT
  blank();
  # 455  T=0
  $state->{"T"} = 0;
  #(455) H=1000
  $state->{"H"} = 1000;
  #(455) V=50
  $state->{"V"} = 50;
  #(455) F=150
  $state->{"F"} = 150;
  my $out = { "anchor" => "goto490" }; return $out;
}

sub goto490 {
  # 490  PRINT T;TAB(6);H;TAB(16);V;TAB(26);F;TAB(35);"I";TAB(H/15);"*"
  gameMessage(
    sprintf('%-6s', $state->{"T"}).
    sprintf('%-10s', $state->{"H"}).
    sprintf('%-10s', $state->{"V"}).
    sprintf('%-9s', $state->{"F"}).
    sprintf('%-'.($state->{"H"}/15).'s', "I").'*');
  # 500  INPUT B
  my @argv = ("integer");
  my $out = {
    "anchor" => "goto500postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto500postinput {
  $state->{"B"} = $input->{"action"}[0];
  # 510  IF B<0 THEN 650
  if ( $state->{"B"} < 0 ) { my $out = { "anchor" => "goto650" }; return $out; }
  # 520  IF B>30 THEN B=30
  if ( $state->{"B"} > 30 ) { $state->{"B"} = 30; }
  # 530  IF B>F THEN B=F
  if ( $state->{"B"} > $state->{"F"} ) { $state->{"B"} = $state->{"F"}; }
  my $out = { "anchor" => "goto540" }; return $out;
}

sub goto540 {
  # 540  V1=V-B+5
  $state->{"V1"} = $state->{"V"}-$state->{"B"}+5;
  # 560  F=F-B
  $state->{"F"} = $state->{"F"}-$state->{"B"};
  # 570  H=H- .5*(V+V1)
  $state->{"H"} = $state->{"H"}- .5*($state->{"V"}+$state->{"V1"});
  # 580  IF H<=0 THEN 670
  if ( $state->{"H"} <= 0 ) { my $out = { "anchor" => "goto670" }; return $out; }
  # 590  T=T+1
  $state->{"T"} = $state->{"T"}+1;
  # 600  V=V1
  $state->{"V"} = $state->{"V1"};
  # 610  IF F>0 THEN 490
  if ( $state->{"F"} > 0 ) { my $out = { "anchor" => "goto490" }; return $out; }
  # 615  IF B=0 THEN 640
  if ( $state->{"B"} == 0 ) { my $out = { "anchor" => "goto640" }; return $out; }
  # 620  PRINT"**** OUT OF FUEL ****"
  gameMessage("**** OUT OF FUEL ****");
  my $out = { "anchor" => "goto640" }; return $out;
}

sub goto640 {
  # 640  PRINT T;TAB(4);H;TAB(12);V;TAB(20);F;TAB(29);"I";TAB(H/12+29);"*"
  gameMessage(
    sprintf('%4s', $state->{"B"}).
    sprintf('%8s', $state->{"H"}).
    sprintf('%8s', $state->{"V"}).
    sprintf('%9s', $state->{"F"}).
    sprintf('%-'.($state->{"H"}/12+29).'s', "I").'*');
  my $out = { "anchor" => "goto650" }; return $out;
}

sub goto650 {
  # 650  B=0
  $state->{"B"} = 0;
  # 660  GOTO 540
  my $out = { "anchor" => "goto540" }; return $out;
}

sub goto670 {
  # 670  PRINT"***** CONTACT *****"
  gameMessage("***** CONTACT *****");
  # 680  H=H+ .5*(V1+V)
  $state->{"H"} = $state->{"H"}+ .5*($state->{"V1"}+$state->{"V"});
  # 690  IF B=5 THEN 720
  if ( $state->{"B"} == 5 ) { my $out = { "anchor" => "goto720" }; return $out; }
  # 700  D=(-V+SQR(V*V+H*(10-2*B)))/(5-B)
  $state->{"D"} = (-$state->{"V"}+sqrt($state->{"V"}*$state->{"V"}+$state->{"H"}*(10-2*$state->{"B"})))/(5-$state->{"B"});
  # 710  GOTO 730
  my $out = { "anchor" => "goto730" }; return $out;
}

sub goto720 {
  # 720  D=H/V
  $state->{"D"} = $state->{"H"}/$state->{"V"};
  my $out = { "anchor" => "goto730" }; return $out;
}

sub goto730 {
  # 730  V1=V+(5-B)*D
  $state->{"V1"} = $state->{"V"}+(5-$state->{"B"})*$state->{"D"};
  # 760  PRINT"TOUCHDOWN AT";T+D;"SECONDS."
  gameMessage("TOUCHDOWN AT ".($state->{"T"}+$state->{"D"})." SECONDS.");
  # 770  PRINT"LANDING VELOCITY=";V1;"FEET/SEC."
  gameMessage("LANDING VELOCITY = ".$state->{"V1"}." FEET/SEC.");
  # 780  PRINT F;"UNITS OF FUEL REMAINING."
  gameMessage( $state->{"F"}."UNITS OF FUEL REMAINING.");
  # 790  IF V1<>0 THEN 810
  if ( $state->{"V1"} != 0 ) { my $out = { "anchor" => "goto810" }; return $out; }
  # 800  PRINT"CONGRATULATIONS! A PERFECT LANDING!!"
  gameMessage("CONGRATULATIONS! A PERFECT LANDING!!");
  # 805  PRINT"YOUR LICENSE WILL BE RENEWED.......LATER."
  gameMessage("YOUR LICENSE WILL BE RENEWED.......LATER.");
  my $out = { "anchor" => "goto810" }; return $out;
}

sub goto810 {
  # 810  IF ABS(V1)<2 THEN 840
  if ( abs($state->{"V1"}) < 2 ) { my $out = { "anchor" => "goto840" }; return $out; }
  # 820  PRINT"***** SORRY, BUT YOU BLEW IT!!!!"
  gameMessage("***** SORRY, BUT YOU BLEW IT!!!!");
  # 830  PRINT"APPROPRIATE CONDOLENCES WILL BE SENT TO YOUR NEXT OF KIN."
  gameMessage("APPROPRIATE CONDOLENCES WILL BE SENT TO YOUR NEXT OF KIN.");
  my $out = { "anchor" => "goto840" }; return $out;
}

sub goto840 {
  # 840  PRINT
  blank();
  #(840) PRINT
  blank();
  #(840) PRINT
  blank();
  # 850  INPUT "ANOTHER MISSION";A$
  gameMessage("ANOTHER MISSION");
  my $out = {
    "anchor" => "goto850postinput",
    "echo" => " > ",
    "hook" => "reply"
  };
  return $out;
}

sub goto850postinput {
  $state->{"As"} = $input->{"action"}[0];
  # 860  IF A$="YES" THEN 390
  if ( $state->{"As"} =~ /^YES$/ ) { my $out = { "anchor" => "goto390" }; return $out; }
  # 870  PRINT
  blank();
  #(870)  PRINT "CONTROL OUT."
   gameMessage( "CONTROL OUT.");
  #(870)  PRINT
  blank();
  # 999  END
  my $out = { "anchor" => "quit_game" }; return $out;
}

1;

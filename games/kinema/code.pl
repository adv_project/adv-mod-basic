#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

  #(<<<GOTO>> ) 100, 510, 512
  #(<<<GOSUB>> ) 500

sub initialize {
  my $out = { "anchor" => "goto10" }; return $out;
}

sub goto10 {
  # 10  PRINT TAB(33);"KINEMA"
  gameMessageTab(33, "KINEMA");
  # 20  PRINT TAB(15);"CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY"
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  # 30  PRINT
  blank();
  #(30)  PRINT
  blank();
  #(30)  PRINT
  blank();
  my $out = { "anchor" => "goto100" }; return $out;
}

sub goto100 {
  # 100  PRINT
  blank();
  # 105  PRINT
  blank();
  # 106  Q=0
  $state->{"Q"} = 0;
  # 110  V=5+INT(35*RND(1))
  $state->{"V"} = 5+int(35*rand());
  # 111  PRINT "A BALL IS THROWN UPWARDS AT";V;"METERS PER SECOND."
  gameMessage( "A BALL IS THROWN UPWARDS AT ".$state->{"V"}." METERS PER SECOND.");
  # 112  PRINT
  blank();
  # 115  A=.05*V^2
  $state->{"A"} = .05*$state->{"V"}**2;
  # 116  PRINT "HOW HIGH WILL IT GO (IN METERS)";
  gameMessage( "HOW HIGH WILL IT GO (IN METERS)");
  # 117  GOSUB 500
  $state->{"return"} = "goto120";
  gosub500();
}

sub goto120 {
  # 120  A=V/5
  $state->{"A"} = $state->{"V"}/5;
  # 122  PRINT "HOW LONG UNTIL IT RETURNS (IN SECONDS)";
  gameMessage( "HOW LONG UNTIL IT RETURNS (IN SECONDS)");
  # 124  GOSUB 500
  $state->{"return"} = "goto130";
  gosub500();
}

sub goto130 {
  # 130  T=1+INT(2*V*RND(1))/10
  $state->{"T"} = 1+int(2*$state->{"V"}*rand())/10;
  # 132  A=V-10*T
  $state->{"A"} = $state->{"V"}-10*$state->{"T"};
  # 134  PRINT "WHAT WILL ITS VELOCITY BE AFTER";T;"SECONDS";
  gameMessage( "WHAT WILL ITS VELOCITY BE AFTER ".$state->{"T"}." SECONDS");
  # 136  GOSUB 500
  $state->{"return"} = "goto140";
  gosub500();
}

sub goto140 {
  # 140  PRINT
  blank();
  # 150  PRINT Q;"RIGHT OUT OF 3.";
  # 160  IF Q<2 THEN 100
  if ( $state->{"Q"} < 2 ) {
    gameMessage( $state->{"Q"}." RIGHT OUT OF 3.");
    my $out = { "anchor" => "goto100" }; return $out;
  }
  # 170  PRINT "  NOT BAD."
  gameMessage( $state->{"Q"}." RIGHT OUT OF 3. NOT BAD.");
  # 180  GOTO 100
  my $out = { "anchor" => "goto100" }; return $out;
}

sub gosub500 {
  # 500  INPUT G
  my @argv = ("number");
  my $out = {
    "anchor" => "goto500postinput",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub goto500postinput {
  $state->{"G"} = $input->{"action"}[0];
  # 502  IF ABS((G-A)/A)<.15 THEN 510
  if ( abs(($state->{"G"}-$state->{"A"})/$state->{"A"}) < .15 ) { my $out = { "anchor" => "goto510" }; return $out; }
  # 504  PRINT "NOT EVEN CLOSE...."
  gameMessage( "NOT EVEN CLOSE....");
  # 506  GOTO 512
  my $out = { "anchor" => "goto512" }; return $out;
}

sub goto510 {
  # 510  PRINT "CLOSE ENOUGH."
  gameMessage( "CLOSE ENOUGH.");
  # 511  Q=Q+1
  $state->{"Q"} = $state->{"Q"}+1;
  my $out = { "anchor" => "goto512" }; return $out;
}

sub goto512 {
  # 512  PRINT "CORRECT ANSWER IS ";A
  gameMessage( "CORRECT ANSWER IS ".$state->{"A"});
  # 520  PRINT
  blank();
  # 530  RETURN
  #return;
  my $out = { "anchor" => $state->{"return"} }; return $out;
  # 999  END
  my $out = { "anchor" => "quit_game" }; return $out;
}

1;

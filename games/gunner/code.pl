#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

use Storable qw(dclone);

sub initialize {
  my $out = { "anchor" => "goto10" };
  return $out;
}

sub goto10 {
  gameMessageTab(30, "GUNNER");
  gameMessageTab(15, "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY");
  blank(3);
  gameMessage("YOU ARE THE OFFICER-IN-CHARGE, GIVING ORDERS TO A GUN");
  gameMessage("CREW, TELLING THEM THE DEGREES OF ELEVATION YOU ESTIMATE");
  gameMessage("WILL PLACE A PROJECTILE ON TARGET.  A HIT WITHIN 100 YARDS");
  gameMessage("OF THE TARGET WILL DESTROY IT.");
  blank();
  my $out = { "anchor" => "goto170" };
  return $out;
}

sub goto170 {
  $state->{"R"} = int( 40000 * rand() + 20000 );
  gameMessage("MAXIMUM RANGE OF YOUR GUN IS ".$state->{"R"}." YARDS.");
  $state->{"Z"} = 0;
  blank();
  $state->{"S1"} = 0;
  my $out = { "anchor" => "goto200" };
  return $out;
}

sub goto200 {
  $state->{"T"} = int( $state->{"R"} * ( 0.1 + 0.8 * rand() ) );
  $state->{"S"} = 0;
  my $out = { "anchor" => "goto370" };
  return $out;
}

sub goto230 {
  gameMessage("MINIMUM ELEVATION IS ONE DEGREE.");
  my $out = { "anchor" => "goto390" };
  return $out;
}

sub goto250 {
  gameMessage("MAXIMUM ELEVATION IS 89 DEGREES.");
  my $out = { "anchor" => "goto390" };
  return $out;
}

sub goto270 {
  my $v = abs($state->{"E"});
  gameMessage("OVER TARGET BY ".$v." YARDS.");
  my $out = { "anchor" => "goto390" };
  return $out;
}

sub goto290 {
  my $v = abs($state->{"E"});
  gameMessage("SHORT OF TARGET BY ".$v." YARDS.");
  my $out = { "anchor" => "goto390" };
  return $out;
}

sub goto320 {
  gameMessage("*** TARGET DESTROYED ***   ".$state->{"S"}." ROUNDS OF AMMUNITION EXPENDED.");
  $state->{"S1"} = $state->{"S1"} + $state->{"S"};
  if ( $state->{"Z"} == 4 ) {
    my $out = { "anchor" => "goto490" };
    return $out;
  }
  $state->{"Z"} = $state->{"Z"} + 1;
  blank();
  gameMessage("THE FORWARD OBSERVER HAS SIGHTED MORE ENEMY ACTIVITY...");
  my $out = { "anchor" => "goto200" };
  return $out;
}

sub goto370 {
  gameMessage("DISTANCE TO THE TARGET IS ".$state->{"T"}." YARDS.");
  blank();
  my $out = { "anchor" => "goto390" };
  return $out;
}

sub goto390 {
  blank();
  gameMessage("ELEVATION");
  my @argv = ("number");
  my $out = {
    "anchor" => "elevation",
    "echo" => " > ",
    "argc" => 1,
    "argv" => dclone(\@argv),
    "hook" => "reply"
  };
  return $out;
}

sub elevation {
  $state->{"B"} = $input->{"action"}[0];
  blank(1);
  if ( $state->{"B"} > 89 ) {
    my $out = { "anchor" => "goto250" };
    return $out;
  }
  if ( $state->{"B"} < 1 ) {
    my $out = { "anchor" => "goto230" };
    return $out;
  }
  $state->{"S"} = $state->{"S"} + 1;
  if ( $state->{"S"} < 6 ) {
    my $out = { "anchor" => "goto450" };
    return $out;
  }
  gameMessage("BOOM !!!!   YOU HAVE JUST BEEN DESTROYED BY THE ENEMY.");
  blank(3);
  my $out = { "anchor" => "goto495" };
  return $out;
}

sub goto450 {
  $state->{"B2"} = 2 * $state->{"B"} / 57.3;
  $state->{"I"} = $state->{"R"} * sin($state->{"B2"});
  $state->{"X"} = $state->{"T"} - $state->{"I"};
  $state->{"E"} = int($state->{"X"});
  if ( abs($state->{"E"}) < 100 ) {
    my $out = { "anchor" => "goto320" };
    return $out;
  }
  if ( $state->{"E"} > 100 ) {
    my $out = { "anchor" => "goto290" };
    return $out;
  }
  my $out = { "anchor" => "goto270" };
  return $out;
}

sub goto490 {
  blank(2);
  gameMessage("TOTAL ROUNDS EXPENDED WERE: ".$state->{"S1"});
  if ( $state->{"S1"} > 18 ) {
    my $out = { "anchor" => "goto495" };
    return $out;
  }
  gameMessage("NICE SHOOTING !!");
  my $out = { "anchor" => "goto500" };
  return $out;
}

sub goto495 {
  gameMessage("BETTER GO BACK TO FORT SILL FOR REFRESHER TRAINING!");
  my $out = { "anchor" => "goto500" };
  return $out;
}

sub goto500 {
  blank();
  gameMessage("TRY AGAIN (Y OR N)");
  my $out = {
    "anchor" => "goto510",
    "echo" => " > ",
    "hook" => "reply"
  };
  return $out;
}

sub goto510 {
  $state->{"Z"} = $input->{"action"}[0];
  if ( $state->{"Z"} eq "Y" ) {
    my $out = { "anchor" => "goto170" };
    return $out;
  }
  blank();
  gameMessage("OK.  RETURN TO BASE CAMP.");
  my $out = { "anchor" => "quit_game" };
  return $out;
}

1;

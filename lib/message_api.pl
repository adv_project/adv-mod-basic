#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

# Color constants
$RESET = "\0110";
$BOLD = "\0112";
$ITALIC = "\0113";
$UNDERLINE = "\0114";
$NOTBOLD = "\0115";
$NOTITALIC = "\0116";
$NOTUNDERLINE = "\0117";
$RED = "\011r";
$GREEN = "\011g";
$YELLOW = "\011y";
$BLUE = "\011b";
$MAGENTA = "\011m";
$CYAN = "\011c";
$WHITE = "\011w";
$POSITIVE = "\011p";
$NEGATIVE = "\011n";

sub callSend {
  my $player = shift;
  my $message_type = shift;
  my $message = shift;
  my $object = {
    "class"   => "send",
    "player"  => $player,
    "type"    => $message_type,
    "message" => $message
  };
  return $object;
}

sub callSendTagged {
  my $object = callSend(shift, shift, shift);
  $object->{'tag'} = shift;
  return $object;
}

sub callBroadcast {
  my $message_type = shift;
  my $message = shift;
  my $object = {
    "class"   => "broadcast",
    "type"    => $message_type,
    "message" => $message
  };
  return $object;
}

sub callBroadcastSkipping {
  my $skip = shift;
  my $object = callBroadcast(shift, shift);
  $object->{'skip'} = $skip;
  return $object;
}

sub callWrite {
  my $player = shift;
  my $action = shift;
  my $text = shift;
  my $object = {
    "class"  => "write",
    "player" => $player,
    "action" => $action,
    "text"   => $text
  };
  return $object;
}

sub callLog {
  my $loglevel = int(shift);
  my $message = shift;
  my $object = {
    "class"     => "log",
    "log-level" => $loglevel,
    "message"   => $message
  };
  return $object;
}

sub callSendPlain   { return callSend(shift, "plain",   shift); }
sub callSendInfo    { return callSend(shift, "info",    shift); }
sub callSendNotice  { return callSend(shift, "notice",  shift); }
sub callSendWarning { return callSend(shift, "warning", shift); }
sub callSendError   { return callSend(shift, "error",   shift); }

sub callSendMePlain   { return callSend($input->{'player'}, "plain",   shift); }
sub callSendMeInfo    { return callSend($input->{'player'}, "info",    shift); }
sub callSendMeNotice  { return callSend($input->{'player'}, "notice",  shift); }
sub callSendMeWarning { return callSend($input->{'player'}, "warning", shift); }
sub callSendMeError   { return callSend($input->{'player'}, "error",   shift); }

sub callLogDebug  { return callLog(7, shift); }
sub callLogInfo   { return callLog(6, shift); }
sub callLogNotice { return callLog(5, shift); }
sub callLogWarn   { return callLog(4, shift); }
sub callLogError  { return callLog(3, shift); }
sub callLogCrit   { return callLog(2, shift); }
sub callLogAlert  { return callLog(1, shift); }
sub callLogEmerg  { return callLog(0, shift); }

1;

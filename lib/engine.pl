#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

# TODO: @messages should be defined in every hook. Also, messages should be
# flushed by default on every hook execution.

sub quit_game {
  my $infofile = $wdir."/gameinfo.json";
  return unless (-f $infofile);
  local $/;
  open my $fh, '<', $infofile;
  my $json = <$fh>;
  close $fh;
  my $info = JSON::decode_json($json);
  unlink $info->{"infofile"};
  my $lock = $wdir."/../../hook.lock";
  my $hook = $wdir."/../../hook.json";
  my @offlinecalls = ();
  my @action = ('reset');
  my $run_input = {
    "player" => $info->{'owner'},
    "action" => dclone(\@action)
  };
  my $action_hook = $info->{'parent_wdir'}."/mods/adv-basic/hooks/action";
  push(@offlinecalls, callDestroy($info->{'created_id'}));
  push(@offlinecalls, callRun($action_hook, $run_input));
  my $offlinejson = JSON->new->pretty(1)->encode(dclone(\@offlinecalls));
  while (1) {
    last unless (-f $lock);
    sleep(.5);
  }
  local $/;
  open my $lh, '>', $lock;
  print $lh '';
  close $lh;
  open my $jh, '>', $hook;
  print $jh $offlinejson;
  close $jh;
  unlink $lock;
  my $o = {
    "anchor" => "dummy_anchor",
    "hook" => "already_dead"
  };
  return $o;
}

sub gameMessage {
  my $string = $usercolors->{"games_color"}.shift;
  push(@messages, $string);
}

sub gameMessageTab {
  my $ts = shift;
  my $string = $usercolors->{"games_color"}.shift;
  my $indentation;
  for my $i (1 .. $ts) { $indentation = $indentation . " "; }
  push(@messages, $indentation . $string);
}

sub gameMessageBlank {
  my $bs = shift;
  my $string = $usercolors->{"games_color"}.shift;
  for my $i (1 .. $bs) { message(''); }
  push(@messages, $string);
}

sub message {
  my $string = shift;
  push(@messages, $string);
}

sub messageTab {
  my $ts = shift;
  my $string = shift;
  my $indentation;
  for my $i (1 .. $ts) { $indentation = $indentation . " "; }
  push(@messages, $indentation . $string);
}

sub messageBlank {
  my $bs = shift;
  my $string = shift;
  for my $i (1 .. $bs) { message(''); }
  push(@messages, $string);
}

sub blank {
  my $lines = shift;
  $lines = 1 unless ($lines);
  for my $i (1 .. $lines) {
    push(@messages, "");
  }
}

sub generate_random_string {
  my $length_of_randomstring=shift;# the length of 
  my @letters=('a'..'z');
  my @chars=('a'..'z','1'..'9');
  my $random_string;
  $random_string.=$chars[rand @letters];
  foreach (2 .. $length_of_randomstring) {
    # rand @chars will generate a random 
    # number between 0 and scalar @chars
    $random_string.=$chars[rand @chars];
  }
  return $random_string;
}

sub flushMessages {
  push(@calls, callSendMePlain(dclone(\@messages)));
  @messages = ();
}

sub flush {
  flushMessages();
  writeState();
  writeError();
  writeOutput();
  @calls = ();
}

sub back_to_menu {
  blank();
  message($CYAN."  Back to menu (enter any value to continue)");
  my $o = {
    "anchor" => "show_short_help",
    "hook" => "press_any_key"
  };
  return $o;
}

sub errmsg_wrong_argc {
  my $a = shift; my $i = shift;
  if ($a == 1) {
    message($BOLD.$MAGENTA."Expected a single value, got ".$i);
  } else {
    message($BOLD.$MAGENTA."Expected ".$a." values, got ".$i);
  }
}

sub errmsg_wrong_argv {
  my $a = shift;
  if (@{$a} == 1) {
    message($BOLD.$MAGENTA."Expected type: ".join(', ', @{$a}));
  } else {
    message($BOLD.$MAGENTA."Expected types: ".join(', ', @{$a}));
  }
}

sub initialize_player {
  initialize_menu() unless (-f $wdir."/menu.json");
  my $player = $input->{"player"};
  $state->{$player} = {
    "current" => { "anchor" => "show_banner" },
    "page" => 1
  };
}

sub set_config {
  my $param = $input->{"action"}[1];
  my $value = $input->{"action"}[2];
  if ($param eq "help") { $userconfig->{"help_color"} = $value; }
  elsif ($param eq "bracket") { $userconfig->{"menu_color_bracket"} = $value; }
  elsif ($param eq "item") { $userconfig->{"menu_color_item"} = $value; }
  elsif ($param eq "title") { $userconfig->{"menu_color_title"} = $value; }
  elsif ($param eq "ni") { $userconfig->{"menu_color_not_implemented"} = $value; }
  elsif ($param eq "game") { $userconfig->{"games_color"} = $value; }
  elsif ($param eq "banner") { $userconfig->{"banner_theme"} = $value; }
  elsif ($param eq "defaults") {
    writeDefaultUserConfig(
      $wdir."/users/".$input->{"player"}."/adv-basic/config.json"
    );
  } else {
    message( $BOLD.$MAGENTA.
      "Parameter '".$param."' not recognized; use ".$CYAN."i|instructions".
      $MAGENTA." for details."
    );
    my $o = { "anchor" => "show_menu" }; return $o;
  }
  writeUserConfig();
  parseUserColors();
  my $o = { "anchor" => "show_config" }; return $o;
}

sub show_config {
  blank();
  message("  ".$BOLD.$CYAN.sprintf('%36s', "Banner theme : ").$BOLD.$WHITE.$userconfig->{"banner_theme"});
  message("  ".$BOLD.$CYAN.sprintf('%36s', "Color for menu items : ").$RESET.$usercolors->{"menu_color_item"}.$userconfig->{"menu_color_item"});
  message("  ".$BOLD.$CYAN.sprintf('%36s', "Color for menu brackets : ").$RESET.$usercolors->{"menu_color_bracket"}.$userconfig->{"menu_color_bracket"});
  message("  ".$BOLD.$CYAN.sprintf('%36s', "Color for menu titles : ").$RESET.$usercolors->{"menu_color_title"}.$userconfig->{"menu_color_title"});
  message("  ".$BOLD.$CYAN.sprintf('%36s', "Color for not implemented titles : ").$RESET.$usercolors->{"menu_color_not_implemented"}.$userconfig->{"menu_color_not_implemented"});
  message("  ".$BOLD.$CYAN.sprintf('%36s', "Color for games : ").$RESET.$usercolors->{"games_color"}.$userconfig->{"games_color"});
  message("  ".$BOLD.$CYAN.sprintf('%36s', "Color for help text : ").$RESET.$usercolors->{"help_color"}.$userconfig->{"help_color"});
  blank();
  my $o = { "anchor" => "show_menu" }; return $o;
}

sub main_loop {
  # Read the function left by the last run
  my @messages = ();
  my $player = $input->{"player"};
  if (! exists ($state->{$player})) { initialize_player($input->{"player"}); }
  my $anchor = $state->{$player}->{"current"}->{"anchor"};
  my $ret;

  # Implement safe exit (do not destroy the menu!!!)
  if (($input->{"action"}[0] eq "QUIT") and ($manifest->{"id"} ne "menu")) {
    $anchor = "quit_game";
  } elsif (($input->{"action"}[0] eq "set") and ($manifest->{"id"} eq "menu")) {
    $anchor = "set_config";
  } elsif (($input->{"action"}[0] eq "config") and ($manifest->{"id"} eq "menu")) {
    $anchor = "show_config";
  } elsif (($input->{"action"}[0] eq "menu") and ($manifest->{"id"} eq "menu")) {
    $anchor = "show_menu";
  } else {
    # Check arguments for type and number
    my $cur = $state->{$player}->{"current"};
    # Check argc
    if (
      ( exists $cur->{"argc"} ) and ( $cur->{"argc"} ne "any" )
    ) {
      my $argc = $cur->{"argc"};
      if ( @{ $input->{"action"} } != $argc ) {
        errmsg_wrong_argc($argc, @{ $input->{"action"} }+0);
        return $state->{$player}->{"current"};
      }
    }
  
    # Check argv
    if ( exists $cur->{"argv"} ) {
      my @argv = @{ $cur->{"argv"} };
      for my $i (0 .. (@argv - 1)) {
        # Accept values: 'integer', 'number', 'any' (optional)
        # int: numbers, without decimals
        # float: any number
        # any: anything else
        my $a = $input->{"action"}[$i]; my $e = $argv[$i];
        if ($e eq "integer") {
          my @aint = ($a);
          if ( ( ($a ne $a+0) and ($a ne "0") ) or ( grep /\./, @aint ) ) {
            errmsg_wrong_argv(dclone(\@argv));
            return $state->{$player}->{"current"};
          }
        }
  
        if ($e eq "number") {
          if ($a ne $a+0) {
            errmsg_wrong_argv(dclone(\@argv));
            return $state->{$player}->{"current"};
          }
        }
      }
    }
    if ( exists( $state->{$player}->{"current"}->{"echo"} ) ) {
      gameMessage(
        $state->{$player}->{"current"}->{"echo"}
        . join(' ', @{ $input->{"action"} })
      );
    }
  }

  my $last_ret;
  while (1) {
    $ret = eval $anchor;
    if ($ret ~~ undef) {
      print "anchor ".$anchor." FAILED!!!!!!!\n";
      print "Last return value: ".JSON->new->encode($last_ret)."\n";
      last;
    }
    if ( exists($ret->{"hook"}) ) {
      $state->{$player}->{"current"} = $ret;
      last;
    } else {
      $anchor = $ret->{"anchor"};
      $last_ret = $ret;
    }
  }
  if ($ret->{"hook"} eq "create_game") {
    my $player = $input->{'player'};
    my $created_game = $ret->{'game_id'}.generate_random_string(6);
    my @mods = ('adv-basic');
    #flushMessages();
    my $infofile = $wdir.'/'.$gameproperties->{'name'}."_".$created_game.".json";
    my $content = {
      "owner" => $player,
      "game" => $ret->{'game_id'},
      "parent_wdir" => $wdir,
      "created_id" => $created_game,
      "infofile" => $infofile
    };
    local $/;
    open my $fh, '>', $infofile;
    print $fh JSON->new->pretty(1)->encode($content);
    close $fh;
    push(@calls, callCreate($created_game, dclone(\@mods)));
    $state->{$player}->{"current"} = {
      "anchor" => "back_to_menu"
    };
  }
}

1;

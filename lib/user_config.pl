#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

use File::Path qw(make_path);
use JSON;

require $moddir . "/lib/message_api.pl";

no warnings;

sub initializeUserConfig {
  my $player = shift;
  my $userhomedir = $wdir."/users/".$player;
  my $userconfigdir = $userhomedir."/adv-basic";
  my $userconfigfile = $userconfigdir."/config.json";
  make_path($userconfigdir);
  writeDefaultUserConfig($userconfigfile);
}

sub writeDefaultUserConfig {
  my $userconfigfile = shift;
  my $o = {
    "banner_theme" => "metal",
    "help_color" => "cyan",
    "menu_color_bracket" => "red",
    "menu_color_item" => "bold,red",
    "menu_color_title" => "bold,white",
    "menu_color_not_implemented" => "green",
    "games_color" => "white"
  };
  my $json = JSON->new->pretty(1)->encode($o);
  local $/;
  open my $fh, '>', $userconfigfile;
  print $fh $json;
  close $fh;
}

sub readUserConfig {
  my $player = $input->{'player'};
  $player = $gameproperties->{'owner'} unless ($player);
  my $userhomedir = $wdir."/users/".$player;
  my $userconfigdir = $userhomedir."/adv-basic";
  my $userconfigfile= $userconfigdir."/config.json";
  initializeUserConfig($player) unless (-f $userconfigfile);
  local $/;
  open my $fh, '<', $userconfigfile;
  my $json = <$fh>;
  close $fh;
  $userconfig = JSON::decode_json($json);
  parseUserColors();
}

sub writeUserConfig {
  my $player = $input->{'player'};
  $player = $gameproperties->{'owner'} unless ($player);
  my $userhomedir = $wdir."/users/".$player;
  my $userconfigdir = $userhomedir."/adv-basic";
  my $userconfigfile= $userconfigdir."/config.json";
  initializeUserConfig($player) unless (-f $userconfigfile);
  my $json = JSON->new->pretty(1)->encode($userconfig);
  local $/;
  open my $fh, '>', $userconfigfile;
  print $fh $json;
  close $fh;
}

sub parse_colors {
  my $string = shift;
  my @colortags = split(',', $string);
  my $color;
  foreach my $i (0 .. (@colortags-1)) {
    my $t = $colortags[$i];
    if ($t eq 'reset') { $color = $color.$RESET; }
    if ($t eq 'bold') { $color = $color.$BOLD; }
    if ($t eq 'italic') { $color = $color.$ITALIC; }
    if ($t eq 'underline') { $color = $color.$UNDERLINE; }
    if ($t eq 'notbold') { $color = $color.$NOTBOLD; }
    if ($t eq 'notitalic') { $color = $color.$NOTITALIC; }
    if ($t eq 'notunderline') { $color = $color.$NOTUNDERLINE; }
    if ($t eq 'red') { $color = $color.$RED; }
    if ($t eq 'green') { $color = $color.$GREEN; }
    if ($t eq 'yellow') { $color = $color.$YELLOW; }
    if ($t eq 'blue') { $color = $color.$BLUE; }
    if ($t eq 'magenta') { $color = $color.$MAGENTA; }
    if ($t eq 'cyan') { $color = $color.$CYAN; }
    if ($t eq 'white') { $color = $color.$WHITE; }
    if ($t eq 'positive') { $color = $color.$POSITIVE; }
    if ($t eq 'negative') { $color = $color.$NEGATIVE; }
  }
  return $color;
}

sub parseUserColors {
  $usercolors = {
    "help_color" => parse_colors($userconfig->{"help_color"}),
    "menu_color_bracket" => parse_colors($userconfig->{"menu_color_bracket"}),
    "menu_color_item" => parse_colors($userconfig->{"menu_color_item"}),
    "menu_color_title" => parse_colors($userconfig->{"menu_color_title"}),
    "menu_color_not_implemented" => parse_colors($userconfig->{"menu_color_not_implemented"}),
    "games_color" => parse_colors($userconfig->{"games_color"})
  };
}

1;

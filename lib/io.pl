#!/usr/bin/env perl

# This file is part of the ADV Project
# Text-based adventure game, highly modular and customizable
# Written by Marcelo López Minnucci <coloconazo@gmail.com>
# and Mariano López Minnucci <mlopezviedma@gmail.com>

use JSON;
use Storable qw(dclone);

# Read $gamepropertiesfile and store its content inside the $gameproperties global variable.
sub readGameProperties {
  local $/;
  open my $fh, '<', $gamepropertiesfile;
  my $json = <$fh>;
  close $fh;
  $gameproperties = JSON::decode_json($json);
}

# Read $modpropertiesfile and store its content inside the $modproperties global variable.
sub readModProperties {
  local $/;
  open my $fh, '<', $modpropertiesfile;
  my $json = <$fh>;
  close $fh;
  $modproperties = JSON::decode_json($json);
}

# Read $inputfile and store its content inside the $input global variable.
sub readInput {
  local $/;
  open my $fh, '<', $inputfile;
  my $json = <$fh>;
  close $fh;
  $input = JSON::decode_json($json);
}

# Write @calls global array on $outputfile.
sub writeOutput {
  my $output = dclone(\@calls);
  my $json = JSON->new->pretty(1)->encode($output);
  open my $fh, '>', $outputfile;
  print $fh $json;
  close $fh;
}

# Read $errorfile and store its content inside the @errorcalls global array.
sub readError {
  local $/;
  open my $fh, '<', $errorfile;
  my $json = <$fh>;
  close $fh;
  my $object = JSON::decode_json($json);
  @errorcalls = @{dclone(\@{$object})};
}

# Write @errorcalls global array on $errorfile.
sub writeError {
  my $error = dclone(\@errorcalls);
  my $json = JSON->new->pretty(1)->encode($error);
  open my $fh, '>', $errorfile;
  print $fh $json;
  close $fh;
}

1;

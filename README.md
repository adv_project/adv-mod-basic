# adv-mod-basic

A collection of old games originally written in BASIC.

## Some ideas

The idea is to provide `playadv` with ports of these vintage
command-line games, originally for BASIC; perhaps in time
others will come, like those in bsdgames (mille, go-fish...).

Few of these games are multiplayer, and in principle the plan
is to port them without changes. Maybe later it could evolve
to something else...

Some ideas about the implementation:

- The mod should allow only admins to create games.
- Make extensive use of acl and subgames.
- The game itself would function as some kind of room. When
  a player enters the game, a menu will offer a list of
  games. Selecting a game triggers the creation of a subgame,
  with one and only one player. When the game ends, or the
  player leaves, the subgame can be reset (replay) or destroyed.
- Multiplayer games are possible, in principle. When such a
  game is created, the owner may choose to invite others,
  or broadcast a message that will be sent to all players
  that enter the room, and to those that are already there.
- The games should be grouped in broad categories, to facilitate
  the process of selecting the game. Also, the game owner should
  be able to configure the room in many ways: freezing it,
  filtering games, allowing/restricting players to enter,
  configuring colors...

### Design issues

Having one mod for each game seems overkill -- we would have almost 100 mods installed.

One possibility is to use the same mod for the room and the subgames; the mod would
initialize the subgames appropriately.

### Proposed auxiliary mods

- **ioctl** -- to manage aliases, message tags, configurable colors, etc.

## The games

These are the games I have collected so far
(found in [Vintage Basic Games](http://www.vintage-basic.net/games.html)):

- 23matches
- 3dplot
- aceyducey
- amazing
- animal
- **awari**
- bagels
- basketball
- batnum
- battle
- **blackjack**
- bombardment
- **bombsaway**
- bounce
- bowling
- boxing
- bug
- bullfight
- bullseye
- buzzword
- change
- checkers
- chemist
- chief
- chomp
- civilwar
- **combat**
- **craps**
- cube
- **depthcharge**
- digits
- evenwins
- flipflop
- football
- ftball
- **furtrader**
- gameofevenwins
- **golf**
- gomoko
- guess
- gunner
- **hammurabi**
- hangman
- hello
- hexapawn
- highiq
- hi-lo
- hockey
- horserace
- hurkle
- kinema
- king
- lem
- letter
- life
- lifefortwo
- litquiz
- love
- **lunar**
- mastermind
- mathdice
- mugwump
- name
- nicomachus
- nim
- number
- onecheck
- orbit
- pizza
- poetry
- poker
- qubit
- queen
- reverse
- rocket
- rockscissors
- roulette
- russianroulette
- salvo
- sinewave
- slalom
- slots
- splat
- stars
- stockmarket
- **superstartrek**
- **superstartrekins**
- synonym
- target
- tictactoe1
- tictactoe2
- tower
- train
- trap
- war
- weekday
- word

## Other multiplayer games

- Onitama
- Naval Battle

## Other single player games

- Football Manager (1982) (maybe this should be a standalone project)

## Discarded (irrelevant)

- banner
- bunny
- calendar
- diamond
- dice

